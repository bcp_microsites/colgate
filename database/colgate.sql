CREATE TABLE public.auth_rule
(
  name character varying(64) NOT NULL,
  data bytea,
  created_at integer,
  updated_at integer,
  CONSTRAINT auth_rule_pkey PRIMARY KEY (name)
);

CREATE TABLE public.auth_item
(
  name character varying(64) NOT NULL,
  type smallint NOT NULL,
  description text,
  rule_name character varying(64),
  data bytea,
  created_at integer,
  updated_at integer,
  CONSTRAINT auth_item_pkey PRIMARY KEY (name),
  CONSTRAINT auth_item_rule_name_fkey FOREIGN KEY (rule_name)
      REFERENCES public.auth_rule (name) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE SET NULL
);

CREATE TABLE public.auth_code
(
  id serial NOT NULL,
  code character varying(25),
  status smallint,
  usedon timestamp without time zone,
  regtill date,
  booktill date,
  campaign_id integer,
  CONSTRAINT auth_code_pkey PRIMARY KEY (id)
);

CREATE TABLE public.auth_assignment
(
  item_name character varying(64) NOT NULL,
  user_id character varying(64) NOT NULL,
  created_at integer,
  CONSTRAINT auth_assignment_pkey PRIMARY KEY (item_name, user_id),
  CONSTRAINT auth_assignment_item_name_fkey FOREIGN KEY (item_name)
      REFERENCES public.auth_item (name) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE public.booking
(
  id serial NOT NULL,
  voucher_code character varying(100) NOT NULL DEFAULT ''::character varying,
  city_id integer NOT NULL DEFAULT 0,
  theatre_id integer NOT NULL DEFAULT 0,
  movie character varying(200) NOT NULL DEFAULT '0'::character varying,
  prefered_date date NOT NULL,
  prefered_time character varying(30) NOT NULL DEFAULT '0'::character varying,
  seats smallint NOT NULL DEFAULT 0,
  cost_per_ticket character varying(10) NOT NULL DEFAULT '0'::character varying,
  grand_total character varying(20) NOT NULL DEFAULT '0'::character varying,
  discount character varying(20) NOT NULL DEFAULT '0'::character varying,
  total character varying(20) NOT NULL DEFAULT '0'::character varying,
  status smallint NOT NULL DEFAULT 1,
  createdon timestamp without time zone NOT NULL,
  updatedon timestamp without time zone NOT NULL,
  payment_status smallint NOT NULL DEFAULT 1, -- 0:Not Paid, 1:Paid, 2:Pending
  transaction_details text DEFAULT '1'::text,
  registration_id integer NOT NULL,
  movie_id integer NOT NULL DEFAULT 0,
  CONSTRAINT booking_pkey PRIMARY KEY (id)
);

CREATE TABLE public.campaigns
(
  id serial NOT NULL,
  analyticscode character varying(5000),
  title character varying(100) NOT NULL,
  regtext text,
  contactno character varying(15) NOT NULL,
  txtcontact text,
  txtterms text,
  postbookmsgvalid_uber character varying(1000) DEFAULT NULL::character varying,
  postbookmsgvalid_magazine character varying(1000) DEFAULT NULL::character varying,
  startdate date,
  enddate date,
  disabledweekdays character varying(15) DEFAULT NULL::character varying,
  sitebanner text,
  status integer,
  created_date timestamp without time zone,
  updated_date timestamp without time zone,
  max_num_offercodes integer,
  booking_reject_msg text,
  booking_excess_msg text,
  ocr_enable boolean,
  reg_allowed_mobile integer,
  emailtext text,
  postregmsgvalid text,
  postregmsginvalid text,
  CONSTRAINT campaigns_pkey PRIMARY KEY (id)
);

CREATE TABLE public.cities
(
  id serial NOT NULL,
  state_id integer,
  cityname character varying(150),
  status smallint DEFAULT 1,
  CONSTRAINT cities_pkey PRIMARY KEY (id)
);

CREATE TABLE public.customers
(
  id serial NOT NULL,
  createdon timestamp without time zone,
  updated_date timestamp without time zone,
  access_token character varying(1000),
  access_token_expiry timestamp without time zone,
  access_token_updated timestamp without time zone,
  voucher_code character varying(100),
  customer_name character varying,
  email character varying,
  mobile character varying(50),
  CONSTRAINT customers_pkey PRIMARY KEY (id)
);

CREATE TABLE public.logs
(
  id serial NOT NULL,
  campaign_id integer,
  description character varying(1000),
  created_date timestamp without time zone,
  category character varying(100),
  CONSTRAINT logs_pkey PRIMARY KEY (id)
);

CREATE TABLE public.messages
(
  id serial NOT NULL,
  createdon timestamp(0) without time zone NOT NULL,
  fromid character varying(50) DEFAULT NULL::character varying,
  toid character varying(50) NOT NULL,
  sub character varying(250) DEFAULT NULL::character varying,
  body character varying(1500) NOT NULL,
  msgtype smallint NOT NULL,
  senton timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  status smallint NOT NULL,
  campaign_id integer,
  CONSTRAINT messages_pkey PRIMARY KEY (id)
);

CREATE TABLE public.movies
(
  id serial NOT NULL,
  name character varying(200) DEFAULT NULL::character varying,
  status smallint DEFAULT '1'::smallint, -- 1:Active, 2:Deleted
  CONSTRAINT movies_pkey PRIMARY KEY (id)
);

CREATE TABLE public.registration
(
  id serial NOT NULL,
  voucher_code character varying(100),
  customername character varying(50),
  email character varying(50),
  mobile character varying(50),
  created_date timestamp without time zone,
  ipaddress character varying(50),
  otp character varying(25),
  lat character varying(50),
  lng character varying(50),
  updated_date timestamp without time zone,
  city character varying,
  booking_status integer,
  otp_verified smallint DEFAULT 0,
  campaign_id integer,
  customer_id integer,
  CONSTRAINT registration_pkey PRIMARY KEY (id)
);

CREATE TABLE public.states
(
  id serial NOT NULL,
  statename character varying(100),
  status smallint DEFAULT 1,
  CONSTRAINT states_pkey PRIMARY KEY (id)
);

CREATE TABLE public.theatres
(
  id serial NOT NULL,
  theatre_name character varying(200),
  area_id integer,
  city_id integer,
  CONSTRAINT id PRIMARY KEY (id)
);

CREATE TABLE public.transaction
(
  id serial NOT NULL,
  transaction_id integer,
  booking_id integer,
  payu_payment_id integer,
  amount character varying(30) DEFAULT NULL::character varying,
  card_mode character varying(30) DEFAULT NULL::character varying,
  status character varying(30) DEFAULT NULL::character varying,
  createdon timestamp without time zone,
  txn_id character varying(50),
  CONSTRAINT transaction_pkey PRIMARY KEY (id)
);

CREATE TABLE public.user
(
  id serial NOT NULL,
  username character varying(200),
  auth_key character varying(255),
  access_token_expired_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  password_hash character varying(255),
  password_reset_token character varying(255),
  email character varying(255),
  unconfirmed_email character varying(255),
  confirmed_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  registration_ip character varying(20),
  last_login_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  last_login_ip character varying(20),
  blocked_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
  status integer DEFAULT 10,
  role integer,
  created_at timestamp(0) without time zone DEFAULT now(),
  updated_at timestamp(0) without time zone DEFAULT now(),
  campaign_id character varying(150),
  CONSTRAINT user_pkey PRIMARY KEY (id)
);

CREATE TABLE public.vouchers
(
  id serial NOT NULL,
  code character varying(100) NOT NULL,
  auth_code character varying(100) NOT NULL,
  status smallint DEFAULT 0,
  regtill date,
  booktill date,
  auth_regtill date,
  auth_booktill date,
  campaign_id integer,
  usedon timestamp without time zone,
  active integer DEFAULT 1,
  last_date date,
  expiry_date date,
  CONSTRAINT vouchers_pkey PRIMARY KEY (id),
  CONSTRAINT code UNIQUE (code)
);



INSERT INTO "auth_item" ("name", "type", "description", "rule_name", "data", "created_at", "updated_at") VALUES (E'admin', 1, E'Administrator', NULL, NULL, 1512630620, 1512630620);


INSERT INTO "auth_assignment" ("item_name", "user_id", "created_at") VALUES (E'admin', E'1', 1518518315);


INSERT INTO "campaigns" ("id", "analyticscode", "title", "regtext", "contactno", "txtcontact", "txtterms", "postbookmsgvalid_uber", "postbookmsgvalid_magazine", "startdate", "enddate", "disabledweekdays", "sitebanner", "status", "created_date", "updated_date", "max_num_offercodes", "booking_reject_msg", "booking_excess_msg", "ocr_enable", "reg_allowed_mobile", "emailtext", "postregmsgvalid", "postregmsginvalid") VALUES (1, NULL, E'Colgate', E'{{otp}} is your OTP for Colgate Movie Offer. For detailed T&C visit : www.colgate.bigcityexperience.com', E'9952417915', E'test', E'test', NULL, NULL, E'2019-07-01', E'2019-07-31', NULL, E'', 1, NULL, E'2019-07-03 17:12:37', NULL, NULL, NULL, E'true', 2, E'<p>Dear Customer,</p><p>{{otp}} is your OTP for Colgate Movie Offer. For detailed T&C visit : <a href="http://www.colgate.bigcityexperience.com">www.colgate.bigcityexperience.com</a></p><p>&nbsp;</p><p>Regards,</p><p>Team Bigcity</p>', E'Product Matched', E'Product Doesnot Match');


INSERT INTO "user" ("id", "username", "auth_key", "access_token_expired_at", "password_hash", "password_reset_token", "email", "unconfirmed_email", "confirmed_at", "registration_ip", "last_login_at", "last_login_ip", "blocked_at", "status", "role", "created_at", "updated_at", "campaign_id") VALUES (1, E'admin', E'dVN8fzR_KzJ_lBrymfXI6qyH2QzyXYUU', E'2019-07-25 10:37:30', E'$2y$13$9Gouh1ZbewVEh4bQIGsifOs8/RWW/7RIs0CAGNd7tapXFm9.WxiXS', NULL, E'admin@demo.com', E'admin@demo.com', E'2017-05-05 16:38:48', E'127.0.0.1', E'2019-07-24 10:37:31', E'::1', NULL, 10, 99, E'2019-02-13 10:38:22', E'2019-07-24 10:37:29', E'1');
