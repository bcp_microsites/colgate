import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {Offercode} from "../model/Offercode";
import {StaffService} from "../model/staff.service";
import { OffercodeService } from "../model/offercode.service";
import { PagerservicesService } from "../model/pagerservices.service";
import { Angular2Csv } from 'angular2-csv/Angular2-csv';


@Component({
  selector: 'app-offercode',
  templateUrl: './offercode.component.html',
  styleUrls: ['./offercode.component.scss']
})
export class OffercodeComponent implements OnInit {
  private _offercodes :Offercode[];
  private _errorMessage:string;
  private _role:any;
  private _csvdata = [];
  private _downloadSubmitted:boolean = false;
  // pager object
  pager: any = {};
 
  // paged items
  pagedItems: any[];

  
  // Construct form group
  constructor(private _router:Router,
    private _staffService:StaffService,private OffercodeService:OffercodeService,private pagerService: PagerservicesService) { }

  ngOnInit() {
    this._role = this._staffService.getRole();
    console.log(this._role);
    this.getOfferCodes();
  }

  public getOfferCodes() {
    this._offercodes = null;
    this.OffercodeService.getAllOfferCodes()
      .subscribe(
        offercodes => {
              this._offercodes = offercodes;
              // initialize to page 1
              this.setPage(1);
          },
          error =>  {
              // unauthorized access
              if(error.status == 401 || error.status == 403) {
                  this._staffService.unauthorizedAccess(error);
                  this._errorMessage = error.data.message;
              } else {
                  this._errorMessage = error.data.message;
              }
          }
      );
      console.log(this);
  }

  setPage(page: number) {
    // get pager object from service
    this.pager = this.pagerService.getPager(this._offercodes.length, page);

    // get current page of items
    this.pagedItems = this._offercodes.slice(this.pager.startIndex, this.pager.endIndex + 1);
}

public downloadOffercode() {
    this._csvdata = [];
    this._downloadSubmitted = true;
    this.OffercodeService.getAllOfferCodes()
      .subscribe(
        offercodes => {
              this._offercodes = offercodes;
              let filename:string;
              filename = 'offercodes';
              let options = {
                  showLabels: true,
                  headers: ['Offer Code', 'Reward', 'Status']
              };

              for(let i=0; i<offercodes.length; i++) {
                  let item = offercodes[i];
                  if(item.status==0)
                  {
                    status = 'Unused';
                  }
                  if(item.status==1)
                  {
                    status = 'Used';
                  }
                  this._csvdata.push({regid: item.code, voucher: item.product,status: status});
              }

              new Angular2Csv(this._csvdata, filename, options);

              this._downloadSubmitted = false;
          },
          error =>  {
              // unauthorized access
              if(error.status == 401 || error.status == 403) {
                  this._staffService.unauthorizedAccess(error);
                  this._errorMessage = error.data.message;
              } else {
                  this._errorMessage = error.data.message;
              }
          }
      );
}
}
