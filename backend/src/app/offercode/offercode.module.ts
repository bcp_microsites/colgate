import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OffercodeRoutingModule } from './offercode-routing.module';
import { OffercodeComponent } from './offercode.component';
import { OffercodeFormComponent } from './offercode-form.component';
import { ReactiveFormsModule,FormsModule  } from '@angular/forms';
@NgModule({
  imports: [
    CommonModule,
    OffercodeRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [OffercodeComponent,OffercodeFormComponent]
})
export class OffercodeModule { 

  
}
