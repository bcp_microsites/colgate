import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OffercodeComponent } from './offercode.component';
import { OffercodeFormComponent } from './offercode-form.component';

const routes: Routes = [
  {
      path: '',
      data: {
          title: 'Offer Codes'
      },
      children: [
          {
              path: 'list',
              component: OffercodeComponent,
              data: {
                  title: 'List',
              }
          },
          {
              path: 'upload',
              component: OffercodeFormComponent,
              data: {
                  title: 'upload'
              }
          },
          {
              path: '',
              pathMatch: 'full',
              redirectTo: 'list'
          }
      ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OffercodeRoutingModule {}
