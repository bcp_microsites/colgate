import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OffercodeComponent } from './offercode.component';

describe('OffercodeComponent', () => {
  let component: OffercodeComponent;
  let fixture: ComponentFixture<OffercodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OffercodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OffercodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
