import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {CampaignListComponent} from './campaign-list.component';
import {CampaignFormComponent} from './campaign-form.component';

const routes: Routes = [
    {
        path: '',
        data: {
            title: 'Campaign'
        },
        children: [
            {
                path: 'list',
                component: CampaignListComponent,
                data: {
                    title: 'List',
                }
            },
            {
                path: 'create',
                component: CampaignFormComponent,
                data: {
                    title: 'Create'
                }
            },
            {
                path: ':id',
                component: CampaignFormComponent,
                data: {
                    title: 'Update'
                }
            },
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'list'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CampaignRoutingModule {}
