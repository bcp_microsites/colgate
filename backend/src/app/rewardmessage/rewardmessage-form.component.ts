import {Component, OnInit, OnDestroy} from '@angular/core';
import {Router, ActivatedRoute} from "@angular/router";
import {CustomValidators} from 'ng2-validation';
import {ContainsValidators} from "../shared/contains-validator.directive";
import {FormGroup, FormBuilder, Validators} from "@angular/forms";

import {CampaignDataService} from "../model/campaign-data.service";
import {Campaign} from "../model/campaign";
import {RewardMessageDataService} from "../model/rewardmessage-data.service";
import {RewardMessage} from "../model/rewardmessage";
import {StaffService} from "../model/staff.service";



import * as moment from "moment";

@Component({
    templateUrl: './rewardmessage-form.component.html',
})
export class RewardMessageFormComponent implements OnInit, OnDestroy{
    private _mode:string = '';

    private _id:number;
    private _parameters:any;
    private _rewardmessage:RewardMessage;
    private _campaigns:Campaign[];
    private _rewards:any;
    private _campaignId:number;

    private _errorMessage:string;

    private _form:FormGroup;
    private _formErrors:any;
    private _submitted:boolean = false;

    // Status Types
    private _statusTypes:any = {};

    constructor(private _rewardMessageDataService:RewardMessageDataService,
                private _campaignDataService:CampaignDataService,
                private _router:Router,
                private _activatedRoute:ActivatedRoute,
                private _formBuilder:FormBuilder,
				private _staffService:StaffService) {

        // Construct form group
        this._form = _formBuilder.group({
            campaign_id: ['', Validators.compose([
                Validators.required
            ])],
			reward_id: ['', Validators.compose([
                Validators.required
            ])],
            messages_template: ['', Validators.compose([
                Validators.required
            ])],
            email_template: ['', Validators.compose([
                Validators.required
            ])],
            status: ['', Validators.compose([
                Validators.required,
                // Custom validator for checking value against list of values
                ContainsValidators.contains('value', RewardMessageDataService.getStatusTypes())
            ])],
        });

        this._statusTypes = RewardMessageDataService.getStatusTypes();


        this._form.valueChanges
            .subscribe(data => this.onValueChanged(data));

    }

    public getCampaigns() {
        this._campaigns = null;
        this._campaignDataService.getAllCampaigns()
            .subscribe(
                campaigns => {
                    this._campaigns = campaigns;
                },
                error =>  {
                    // unauthorized access
                    if(error.status == 401 || error.status == 403) {
                        this._staffService.unauthorizedAccess(error);
                    } else {
                        this._errorMessage = error.data.message;
                    }
                }
            );
    }

    public getRewards(){
        this._rewards = null;
        this._rewardMessageDataService.getAllRewards()
            .subscribe(
                rewards => {
                    this._rewards = rewards;
                },
                error =>  {
                    // unauthorized access
                    if(error.status == 401 || error.status == 403) {
                        this._staffService.unauthorizedAccess(error);
                    } else {
                        this._errorMessage = error.data.message;
                    }
                }
            );
    }

    private _setFormErrors(errorFields:any):void{
        for (let key in errorFields) {
            let errorField = errorFields[key];
            // skip loop if the property is from prototype
            if (!this._formErrors.hasOwnProperty(key)) continue;

            // let message = errorFields[error.field];
            this._formErrors[key].valid = false;
            this._formErrors[key].message = errorField;
        }
    }

    private _resetFormErrors():void{
        this._formErrors = {
            campaign_id: {valid: true, message: ''},
		    reward_id: {valid: true, message: ''},
            messages_template: {valid: true, message: ''},
            email_template: {valid: true, message: ''},
			status: {valid: true, message: ''},
        };
    }

    private _isValid(field):boolean {
        let isValid:boolean = false;

        // If the field is not touched and invalid, it is considered as initial loaded form. Thus set as true
        if(this._form.controls[field].touched == false) {
            isValid = true;
        }
        // If the field is touched and valid value, then it is considered as valid.
        else if(this._form.controls[field].touched == true && this._form.controls[field].valid == true) {
            isValid = true;
        }

        return isValid;
    }


    public onValueChanged(data?: any) {
        if (!this._form) { return; }
        const form = this._form;
        for (let field in this._formErrors) {
            // clear previous error message (if any)
            let control = form.get(field);
            if (control && control.dirty) {
                this._formErrors[field].valid = true;
                this._formErrors[field].message = '';
            }
        }
    }

    private _resetRewardMessage(){
        this._rewardmessage = new RewardMessage();
        this._rewardmessage.campaign_id = 0;
        this._rewardmessage.messages_template = '';
        this._rewardmessage.email_template = '';
        this._rewardmessage.reward_id = 0;
        this._rewardmessage.status = 1;
    }

    public ngOnInit() {
        this.getCampaigns();
        this.getRewards();
        this._resetFormErrors();
        this._resetRewardMessage();
        // _route is activated route service. this._route.params is observable.
        // subscribe is method that takes function to retrieve parameters when it is changed.
        this._parameters = this._activatedRoute.params.subscribe(params => {
            // plus(+) is to convert 'id' to number
            if(typeof params['id'] !== "undefined") {
                this._id = Number.parseInt(params['id']);
                this._errorMessage = "";
                this._rewardMessageDataService.getRewardMessageById(this._id)
                    .subscribe(
                        rewardmessage => {
                            this._rewardmessage = rewardmessage;
                            this._mode = 'update';
                        },
                        error => {
                            // unauthorized access
                            if(error.status == 401 || error.status == 403) {
                                this._staffService.unauthorizedAccess(error);
                            } else {
                                this._errorMessage = error.data.message;
                            }
                        }
                    );
            } else {
                this._mode = 'create';

            }
        });
    }

    public ngOnDestroy() {
        this._parameters.unsubscribe();
        this._rewardmessage = new RewardMessage();
    }

    public onSubmit() {
        this._submitted = true;
        this._resetFormErrors();
        if(this._mode == 'create') {
            this._rewardMessageDataService.addRewardMessage(this._rewardmessage)
                .subscribe(
                    result => {
                        if(result.success) {
                            this._router.navigate(['/rewardmessage']);
                        } else {
                            this._submitted = false;
                        }
                    },
                    error => {
                        this._submitted = false;
                        // Validation errors
                        if(error.status == 422) {
                            let errorFields = JSON.parse(error.data.message);
                            this._setFormErrors(errorFields);
                        }
                        // Unauthorized Access
                        else if(error.status == 401 || error.status == 403) {
                            this._staffService.unauthorizedAccess(error);
                        }
                        // All other errors
                        else {
                            this._errorMessage = error.data.message;
                        }
                    }
                );
        } else if(this._mode == 'update') {
            this._rewardMessageDataService.updateRewardMessageById(this._rewardmessage)
                .subscribe(
                    result => {
                        if(result.success) {
                            this._router.navigate(['/rewardmessage']);
                        } else {
                            this._submitted = false;
                        }
                    },
                    error => {
                        this._submitted = false;
                        // Validation errors
                        if(error.status == 422) {
                            let errorFields = JSON.parse(error.data.message);
                            this._setFormErrors(errorFields);
                            //this._setFormErrors(error.data);
                        }
                        // Unauthorized Access
                        else if(error.status == 401 || error.status == 403) {
                            this._staffService.unauthorizedAccess(error);
                        }
                        // All other errors
                        else {
                            this._errorMessage = error.data.message;
                        }
                    }
                );
        }
    }

    
}