import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import swal, {SweetAlertOptions} from 'sweetalert2';

import {RewardMessageDataService} from "../model/rewardmessage-data.service";
import {RewardMessage} from "../model/rewardmessage";
import {StaffService} from "../model/staff.service";

@Component({
    templateUrl: './rewardmessage-list.component.html',
})
export class RewardMessageListComponent implements OnInit{
    private _rewardmessages:RewardMessage[];
    private _errorMessage:string;

    constructor(private _rewardmessageDataService:RewardMessageDataService,
                private _staffService:StaffService,
                private _router:Router) {}

    ngOnInit() {
         this.getRewardMessages();
    }
    public getRewardMessages() {
        this._rewardmessages = null;
        this._rewardmessageDataService.getAllRewardMessages()
            .subscribe(
                rewardmessages => {
                    this._rewardmessages = rewardmessages
                },
                error =>  {
                    // unauthorized access
                    if(error.status == 401 || error.status == 403) {
                        this._staffService.unauthorizedAccess(error);
                    } else {
                        this._errorMessage = error.data.message;
                    }
                }
            );
    }

    public viewRewardMessage(rewardmessage:RewardMessage):void {
        this._router.navigate(['/rewardmessage', rewardmessage.id]);
    }

}