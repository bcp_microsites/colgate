import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {RewardMessageListComponent} from './rewardmessage-list.component';
import {RewardMessageFormComponent} from './rewardmessage-form.component';

const routes: Routes = [
    {
        path: '',
        data: {
            title: 'Reward Message'
        },
        children: [
            {
                path: 'list',
                component: RewardMessageListComponent,
                data: {
                    title: 'List',
                }
            },
            {
                path: 'create',
                component: RewardMessageFormComponent,
                data: {
                    title: 'Create'
                }
            },
            {
                path: ':id',
                component: RewardMessageFormComponent,
                data: {
                    title: 'Update'
                }
            },
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'list'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RewardMessageRoutingModule {}
