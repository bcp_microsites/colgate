import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';

import {RewardMessageListComponent} from './rewardmessage-list.component';
import {RewardMessageFormComponent} from './rewardmessage-form.component';
import {RewardMessageRoutingModule} from './rewardmessage-routing.module';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        RewardMessageRoutingModule,
    ],
    declarations: [
        RewardMessageListComponent,
        RewardMessageFormComponent,
    ]
})
export class RewardMessageModule { }
