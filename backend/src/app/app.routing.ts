import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

// Layouts
import {FullLayoutComponent} from './layouts/full-layout.component';
import {SimpleLayoutComponent}  from './layouts/simple-layout.component';
import {P404Component} from './pages/404.component';

import {AuthGuard} from './model/auth.guard';

export const routes: Routes = [
    {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
    },
    {
        path: '',
        component: FullLayoutComponent,
        data: {
            title: 'Home'
        },
        canActivate: [AuthGuard],
        children: [
            {
                path: 'dashboard',
                loadChildren: 'app/dashboard/dashboard.module#DashboardModule'
            },
            {
                path: 'campaign',
                loadChildren: 'app/campaign/campaign.module#CampaignModule'
            },
            {
                path: 'rewardmessage',
                loadChildren: 'app/rewardmessage/rewardmessage.module#RewardMessageModule'
            },

            {
                path: 'report',
                loadChildren: 'app/report/report.module#ReportModule'
            },
            {
                path: 'user',
                loadChildren: 'app/user/user.module#UserModule'
            },

            /*{
                path: 'auditor',
                loadChildren: 'app/auditor/auditor.module#AuditorModule'
            },
            {
                path: 'client',
                loadChildren: 'app/client/client.module#ClientModule'
            },*/
            {
                path: 'teamleader',
                loadChildren: 'app/teamleader/teamleader.module#TeamleaderModule'
            },
            {
                path: 'agent',
                loadChildren: 'app/agent/agent.module#AgentModule'
            },
            {
                path: 'offercode',
                loadChildren: 'app/offercode/offercode.module#OffercodeModule'
            },
            {
                path: 'vouchercode',
                loadChildren: 'app/vouchercode/vouchercode.module#VouchercodeModule'
            },
            {
                path: 'offercodesummary',
                loadChildren: 'app/offercodesummary/offercodesummary.module#OffercodesummaryModule'
            },
            {
                path: 'theatres',
                loadChildren: 'app/theatres/theatres.module#TheatresModule'
            },
            {
                path: 'movies',
                loadChildren: 'app/movies/movies.module#MoviesModule'
            },
        ]
    },
    {
        path: '',
        component:SimpleLayoutComponent,
        children: [
            {
                path: 'login',
                loadChildren: 'app/login/login.module#LoginModule'
            },
            {
                path: 'logout',
                loadChildren: 'app/logout/logout.module#LogoutModule'
            }
        ],
    },
    // otherwise redirect to home
    { path: '**', component: P404Component }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
