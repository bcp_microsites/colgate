import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import { MoviesComponent } from './movies.component';

import { MoviesFormComponent } from './movies-form.component';

const routes: Routes = [
    {
        path: '',
        data: {
            title: 'Movies'
        },
        children: [
            {
                path: 'list',
                component: MoviesComponent,
                data: {
                    title: 'List',
                }
            },
            {
                path: 'upload',
                component: MoviesFormComponent,
                data: {
                    title: 'upload'
                }
            },
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'list'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MoviesRoutingModule {}
