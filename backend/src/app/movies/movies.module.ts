import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { MoviesRoutingModule } from './movies-routing.module';
import { MoviesComponent } from './movies.component';
import { MoviesFormComponent } from './movies-form.component';
import { ReactiveFormsModule,FormsModule  } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    MoviesRoutingModule,
    ReactiveFormsModule
    
  ],
  declarations: [MoviesComponent, MoviesFormComponent]
})
export class MoviesModule { }
