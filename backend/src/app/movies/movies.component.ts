import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { Movies } from '../model/movies';
import { StaffService } from '../model/staff.service';
import { MoviesService } from '../model/movies.service';
import { FormGroup, FormBuilder, FormControl, FormArray, Validators } from "@angular/forms";
import { PagerservicesService } from "../model/pagerservices.service";

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.scss']
})
export class MoviesComponent implements OnInit {
  private _movies :Movies[];
  private _moviedetails :Movies[];
  private _errorMessage:string;
  private _role:any;
  private _form:FormGroup;
  private searchtxt:string;
    // pager object
    pager: any = {};
 
    // paged items
    pagedItems: any[];

  // Construct form group
  private _formErrors:any;

  constructor(private _router:Router,
    private _staffService:StaffService,private MoviesService:MoviesService,private _formBuilder:FormBuilder,private pagerService: PagerservicesService) {
      this._form = this._formBuilder.group({
        searchtxt: ['', Validators.compose([Validators.required,])],
      });
     }

  ngOnInit() {
    this._role = this._staffService.getRole();
    // console.log(this._role);
    this.getMovies();
  }

  public getMovies() {
    this._movies = null;
    this._moviedetails = null;
    this.searchtxt = null;
    this.MoviesService.getAllMovies()
        .subscribe(
            movies => {
                this._movies = movies
                 // initialize to page 1
              this.setPage(1);
            },
            error =>  {
                // unauthorized access
                if(error.status == 401 || error.status == 403) {
                    this._staffService.unauthorizedAccess(error);
                } else {
                    this._errorMessage = error.data.message;
                    console.log(this._errorMessage);
                }
            }
        );
}

private _setFormErrors(errorFields:any):void{
  for (let key in errorFields) {
      let errorField = errorFields[key];
      // skip loop if the property is from prototype
      if (!this._formErrors.hasOwnProperty(key)) continue;
      // let message = errorFields[error.field];
      this._formErrors[key].valid = false;
      this._formErrors[key].message = errorField;
  }
}

public onSearch(movie:any)
{
 
  this.MoviesService.searchMovies(movie)
    .subscribe(
      result => {
            this._moviedetails = result;   
            this.setPage(1);         
    },
        error => {
            
            // Validation errors
            if(error.status == 422) {
                let errorFields = JSON.parse(error.data.message);
                console.log(error.data.message);
                this._setFormErrors(errorFields);
                this._errorMessage = error.data.message;
            }
            // Unauthorized Access
            else if(error.status == 401 || error.status == 403) {
             }
            // All other errors
            else {
                this._errorMessage = error.data.message;
            }
        }
    );
  }

  setPage(page: number) {
    // get pager object from service
    this.pager = this.pagerService.getPager(this._movies.length, page);

    // get current page of items
    this.pagedItems = this._movies.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  // Delete movie
  deleteMovie(id: number) {
    this.MoviesService.deleteMovie(id)
    .subscribe(
      result => {
        this.getMovies();
      },
      error =>  {
        // unauthorized access
        if(error.status == 401 || error.status == 403) {
            this._staffService.unauthorizedAccess(error);
        } else {
            this._errorMessage = error.data.message;
            console.log(this._errorMessage);
        }
      }
    )
  }
}
