import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TheatresRoutingModule } from './theatres-routing.module';
import { TheatresComponent } from './theatres.component';
import { TheatresFormComponent } from './theatres-form.component';
import { ReactiveFormsModule,FormsModule  } from '@angular/forms';
@NgModule({
  imports: [
    CommonModule,
    TheatresRoutingModule,
    ReactiveFormsModule
  ],
  declarations:[TheatresComponent,TheatresFormComponent]
})
export class TheatresModule { 

  
}
