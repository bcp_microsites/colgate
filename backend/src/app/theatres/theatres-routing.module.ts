import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TheatresComponent } from './theatres.component';
import { TheatresFormComponent } from './theatres-form.component';

const routes: Routes = [
  {
      path: '',
      data: {
          title: 'Offer Codes'
      },
      children: [
          {
              path: 'list',
              component: TheatresComponent,
              data: {
                  title: 'List',
              }
          },
          {
              path: 'upload',
              component: TheatresFormComponent,
              data: {
                  title: 'upload'
              }
          },
          {
            path: 'create',
            component: TheatresFormComponent,
            data: {
                title: 'create'
            }
          },
          {
              path: '',
              pathMatch: 'full',
              redirectTo: 'list'
          }
      ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TheatresRoutingModule {}
