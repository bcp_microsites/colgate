import {Component, OnInit, OnDestroy, ViewChild} from '@angular/core';
import {Router, ActivatedRoute} from "@angular/router";
import {FormGroup, FormBuilder, FormControl, FormArray, Validators} from "@angular/forms";
import { Offercodeupload} from "../model/offercode";
import { OffercodeService } from "../model/offercode.service";



@Component({
  selector: 'app-theatres',
  templateUrl: './theatres-form.component.html',

})
export class TheatresFormComponent implements OnInit {

  
  private _offercodeupload:Offercodeupload;

  // Error and success messages
  private _errorMessage:string;

  // Forms
  private _form : FormGroup;
  private _formErrors:any;

  // file object
  @ViewChild('upload') upload;

  constructor(private _router:Router,
    private _activatedRoute:ActivatedRoute,private _formBuilder:FormBuilder,private OffercodeService: OffercodeService ) {
       // Construct form group
       this._form = _formBuilder.group({  upload: ['', Validators.compose([])] });
  }

  private _resetFormErrors():void{
    this._formErrors = { upload: {valid: true, message: ''} };
  }

  ngOnInit() {

  }

  handleFileChange(e) {
    const file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
        let reader = new FileReader();
        reader.onload = this._setFileUpload.bind(this);
        reader.readAsDataURL(file); 
  }

  private _setFileUpload(e) {
    this._offercodeupload = new Offercodeupload();
    let  reader = e.target;
    console.log(reader.result);
    this._offercodeupload.upload = reader.result;
}  

private _setFormErrors(errorFields:any):void{
  for (let key in errorFields) {
      let errorField = errorFields[key];
      // skip loop if the property is from prototype
      if (!this._formErrors.hasOwnProperty(key)) continue;
      // let message = errorFields[error.field];
      this._formErrors[key].valid = false;
      this._formErrors[key].message = errorField;
  }
}

public onSubmit() {
  this._resetFormErrors();
  console.log(this);
  const fileBrowser = this.upload.nativeElement;
  if(fileBrowser.files.length == 0) {
    this._offercodeupload.upload = '';
  }
  console.log(this._offercodeupload);
  this.OffercodeService.uploadOffercodes(this._offercodeupload)
    .subscribe(
      result => {
        if (result) {
            this._router.navigate(['/offer']);
        } 
    },
        error => {
            
            // Validation errors
            if(error.status == 422) {
                let errorFields = JSON.parse(error.data.message);               
                this._setFormErrors(errorFields);
                this._errorMessage = error.data.message;
            }
            // Unauthorized Access
            else if(error.status == 401 || error.status == 403) {
             }
            // All other errors
            else {
                this._errorMessage = error.data.message;
            }
        }
    );
}

}
