import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {StaffService} from "../model/staff.service";
import { OffercodeService } from "../model/offercode.service";
import {Offercodesummary} from "../model/offercodesummary";


@Component({
  selector: 'app-offercodesummary',
  templateUrl: './offercodesummary.component.html',
  styleUrls: ['./offercodesummary.component.scss']
})
export class OffercodesummaryComponent implements OnInit {

  constructor(private _router:Router,private _staffService:StaffService,private OffercodeService:OffercodeService) { }
  private _errorMessage:string;
  private _role:any;
  private _csvdata = [];
  private _offercodesummary:Offercodesummary[];

  ngOnInit() {
    this._role = this._staffService.getRole();
    console.log(this._role);
    this.getOfferCodesummary();
  }

  public getOfferCodesummary()
  {
    this._offercodesummary = null;
    this.OffercodeService.getOfferCodesummarydetails()
      .subscribe(
        offercodesummary => {
              this._offercodesummary = offercodesummary;
          },
          error =>  {
              // unauthorized access
              if(error.status == 401 || error.status == 403) {
                  this._staffService.unauthorizedAccess(error);
                  this._errorMessage = error.data.message;
              } else {
                  this._errorMessage = error.data.message;
              }
          }
      );
  }

}
