import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OffercodesummaryComponent } from './offercodesummary.component';

describe('OffercodesummaryComponent', () => {
  let component: OffercodesummaryComponent;
  let fixture: ComponentFixture<OffercodesummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OffercodesummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OffercodesummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
