import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OffercodesummaryRoutingModule } from './offercodesummary-routing.module';
import { OffercodesummaryComponent } from './offercodesummary.component';
import { ReactiveFormsModule,FormsModule  } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    OffercodesummaryRoutingModule,
    ReactiveFormsModule,
  ],
  declarations: [OffercodesummaryComponent]
})
export class OffercodesummaryModule{ 

  
}


