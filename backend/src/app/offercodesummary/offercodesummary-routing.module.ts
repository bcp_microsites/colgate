import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OffercodesummaryComponent } from './offercodesummary.component';

const routes: Routes = [
  {
      path: '',
      data: {
          title: 'Offercode Summary'
      },
      children: [
          {
              path: 'list',
              component: OffercodesummaryComponent,
              data: {
                  title: 'List',
              }
          },
          {
              path: '',
              pathMatch: 'full',
              redirectTo: 'list'
          }
      ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OffercodesummaryRoutingModule{}