import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';
// import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import {SharedModule} from '../shared/shared.module';

import { ReportComponent } from './report.component';
import { BookingComponent } from './booking.component';
import { ReportRoutingModule } from './report-routing.module';


@NgModule({
  imports: [
      CommonModule,
      // InfiniteScrollModule,
      SharedModule,
      ReportRoutingModule,
  ],
  declarations: [
      ReportComponent,
      BookingComponent
  ]
})
export class ReportModule { }
