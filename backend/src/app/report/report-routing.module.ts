import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReportComponent } from './report.component';
import { BookingComponent } from './booking.component';


const routes: Routes = [
  {
      path: '',
      data: {
        title: 'Reports'
      },
      children: [
          {
              path: 'register',
              component: ReportComponent,
              data: {
                  title: 'Register Report',
              }
          },
          {
              path: 'booking',
              component: BookingComponent,
              data: {
                  title: 'Booking report'
              }
          },
          {
              path: '',
              pathMatch: 'full',
              redirectTo: 'register'
          }
      ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportRoutingModule {}
