import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';

import {ReportDataService} from "../model/report-data.service";
import {StaffService} from "../model/staff.service";


import * as moment from 'moment';

@Component({
    selector: 'app-register',
    templateUrl: 'report.component.html'
})
export class ReportComponent implements OnInit {

    private _form:FormGroup;
    private _reportData:any = {};
    private _errorMessage:string;
    private _submitted:boolean = false;
    private _downloadSubmitted:boolean = false;
    private _csvdata = [];
    private _startdate:string = '';
    private _enddate:string = '';
    private _minDate:any;
    private _maxDate:any;


    constructor(
        private _reportDataService:ReportDataService,
        private _staffService:StaffService,
        private _router:Router,
        private _formBuilder:FormBuilder) {

        // Construct form group
        this._form = this._formBuilder.group({
            filterDate: ['', Validators.compose([Validators.required,])],
        });
    }

    ngOnInit() {
        this._maxDate = new Date();
        this._errorMessage = '';
    }


    public registerReport(daterange:any) {
        if(daterange) {
            this._reportData = null;
            this._startdate = moment(daterange[0]).format("YYYY-MM-DD");
            this._enddate = moment(daterange[1]).format("YYYY-MM-DD");

            this._reportDataService.getRegisterData( this._startdate, this._enddate)
                .subscribe(
                    result => {
                        this._reportData = result;
                    },
                    error => {
                        // unauthorized access
                        if(error.status == 401 || error.status == 403) {
                            this._staffService.unauthorizedAccess(error);
                        } else {
                            this._errorMessage = error.data.message;
                        }
                    }
                );
        }
    }

    public downloadReport() {
        this._csvdata = [];
        this._downloadSubmitted = true;
        this._reportDataService.getRegisterData(this._startdate, this._enddate)
            .subscribe(
                result => {
                    let filename:string;

                    if(this._startdate) {
                        filename = 'Register_Report_' + this._startdate + '_' + this._enddate;
                    } else {
                        filename = 'Register Report';
                    }

                    let options = {
                        showLabels: true,
                        headers: ['Register ID', 'Voucher#', 'Customer Name', 'Mobile', 'Email', 'Register Date']
                    };

                    for(let i=0; i<result.length; i++) {
                        let item = result[i];
                        this._csvdata.push({regid: item.reg_id, voucher: item.voucher_code, cnmae: item.customername, mobile: item.mobile, email: item.email, regdate: item.created_date});
                    }

                    new Angular2Csv(this._csvdata, filename, options);

                    this._downloadSubmitted = false;
                },
                error => {
                    this._downloadSubmitted = false;
                    // unauthorized access
                    if(error.status == 401 || error.status == 403) {
                        this._staffService.unauthorizedAccess(error);
                    } else {
                        this._errorMessage = error.data.message;
                    }
                }
            );
    }
}
