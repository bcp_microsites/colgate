export class Agent{
    id:number;
    username:string;
    email:string;
    role:number;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}