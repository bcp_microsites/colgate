<?php
namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "vouchers".
 *
 * @property integer $id
 * @property integer $campaign_id
 * @property integer $reward_id
 * @property string $code
 * @property integer $status
 * @property timestamp $usedon
 * @property date $regtill
 * @property timestamp $booktill
 */

class Voucher extends \yii\db\ActiveRecord
{
	const STATUS_UNUSED = 0;
	const STATUS_USED = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vouchers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code'], 'required'],
            [['status','reward_id'], 'integer'],
            [['code'], 'string', 'max'=>100],
            ['status', 'default', 'value' => self::STATUS_UNUSED],
            ['status', 'in', 'range' => [self::STATUS_UNUSED, self::STATUS_USED]],
            [['usedon'], 'safe'],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Voucher Code',
            'status' => 'Status'
        ];
    }


    public function getVoucherStatus($vcode){
        $sql   = "SELECT reward_id,
                         status
                  FROM vouchers
                  WHERE code = '".$vcode."'";
        $data= Yii::$app->db->createCommand($sql)->queryAll();
        return $data;
    }

    public function getReward()
    {
        return $this->hasOne(Reward::className(), ['id' => 'reward_id']);
    }

        public function fields()
    {
        return [
            'code',
            'product' => function($model) {
                return $model->reward->prod_name;
            },
            'status',
        ];
    }
}
