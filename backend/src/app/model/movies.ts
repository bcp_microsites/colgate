export class Movies{
    id : number;
    name : string;
    file:string;
    
    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}

export class Moviesupload {
    upload: string;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}

export class Moviessearch {
    name: string;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}