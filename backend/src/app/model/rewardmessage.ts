export class RewardMessage{
    id:number;
    campaign_id:number;
    reward_id:number;
    messages_template:string;
    email_template:string;
    status:number;
    created_date:string;
    updated_date:string;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}