export class Offercodesummary{
    id : number;
    code : string;
    reward:string;
    used_count:number;
    unused_count:number;
    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}
