import { Injectable } from '@angular/core';
import {Http, Headers, Response} from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs/Rx';

import {GlobalService} from './global.service';
import {StaffService} from './staff.service';

import {AuthHttp} from 'angular2-jwt';
import { Movies, Moviesupload, Moviessearch} from './movies';

@Injectable()
export class MoviesService {

  constructor(
    private _globalService: GlobalService,
    private _staffService: StaffService,
    private _authHttp: AuthHttp
  ) { }
  

    // GET
  public getAllMovies(): Observable<Movies[]> {
      let headers = this.getHeaders();
  
      return this._authHttp.get(
          this._globalService.apiHost+'/staff/movies',
          {
              headers: headers
          }
      )
          .map(response => response.json())
          .map((response) => {
              return <Movies[]>response.data.movies;
          })
          .catch(this.handleError);
  }
  
  public uploadMovies(movies:Moviesupload): Observable<Movies[]>{
    let headers = this.getHeaders();
  
    return this._authHttp.post(
      this._globalService.apiHost+'/staff/moviesupload',
      JSON.stringify(movies),
      {
          headers: headers
      }
  )
      .map(response => response.json())
      .map((response) => {
          return response;
      })
      .catch(this.handleError);
  }
  
  private getHeaders():Headers {
    return new Headers({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer '+this._staffService.getToken(),
    });
  }
  
  private handleError (error: Response | any) {
  
    let errorMessage:any = {};
    // Connection error
    if(error.status == 0) {
        errorMessage = {
            success: false,
            status: 0,
            data: "Sorry, there was a connection error occurred. Please try again.",
        };
    }
    else {
        errorMessage = error.json();
    }
    return Observable.throw(errorMessage);
  }
  
  public searchMovies(movie:Moviessearch):Observable<Movies[]>
  {
    let headers = this.getHeaders();
    return this._authHttp.get(
        this._globalService.apiHost+'/staff/moviesearch?movie=' + movie,
        {
            headers: headers
        }
    )
      .map(response => response.json())
      .map((response) => {
          return response.data;
      })
      .catch(this.handleError);
  }

    public deleteMovie(id: number): Observable<Movies[]> {
        let headers = this.getHeaders();
        return this._authHttp.get(
            this._globalService.apiHost + '/staff/moviedelete?id=' + id,
            {
                headers: headers
            }
        )
        .map(response => response.json())
        .map((response) => {
            return response.data;
        })
        .catch(this.handleError);
    }
}
