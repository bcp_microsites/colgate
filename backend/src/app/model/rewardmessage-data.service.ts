import {Injectable} from '@angular/core';
import {Http, Headers, Response} from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs/Rx';

import {GlobalService} from './global.service';
import {StaffService} from './staff.service';
import {RewardMessage} from './rewardmessage';
import {AuthHttp} from 'angular2-jwt';


@Injectable()
export class RewardMessageDataService {

    constructor(private _globalService:GlobalService,
                private _staffService:StaffService,
                private _authHttp: AuthHttp){
    }

    // POST /v1/category
    addRewardMessage(rewardmessage:RewardMessage):Observable<any>{
        let headers = this.getHeaders();

        return this._authHttp.post(
            this._globalService.apiHost+'/staff/createrewardmessage',
            JSON.stringify(rewardmessage),
            {
                headers: headers
            }
        )
            .map(response => response.json())
            .map((response) => {
                return response;
            })
            .catch(this.handleError);
    }



    // PUT /v1/category/1
    updateRewardMessageById(rewardmessage:RewardMessage):Observable<any>{
        let headers = this.getHeaders();

        return this._authHttp.put(
            this._globalService.apiHost+'/staff/updaterewardmessages?id='+rewardmessage.id,
            JSON.stringify(rewardmessage),
            {
                headers: headers
            }
        )
            .map(response => response.json())
            .map((response) => {
                return response;
            })
            .catch(this.handleError);
    }

    private getHeaders():Headers {
        return new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+this._staffService.getToken(),
        });
    }
    // GET /v1/category
    getAllRewardMessages(): Observable<RewardMessage[]> {
        let headers = this.getHeaders();

        return this._authHttp.get(
            this._globalService.apiHost+'/staff/rewardmessages',
            {
                headers: headers
            }
        )
            .map(response => response.json())
            .map((response) => {
                return <RewardMessage[]>response.data.reward_message;
            })
            .catch(this.handleError);
    }

    public getAllRewards(): Observable<RewardMessage[]> {
        let headers = this.getHeaders();

        return this._authHttp.get(
            this._globalService.apiHost+'/staff/rewards',
            {
                headers: headers
            }
        )
            .map(response => response.json())
            .map((response) => {
                return <RewardMessage[]>response.data;
            })
            .catch(this.handleError);
    }

    // GET /v1/category/1
    public getRewardMessageById(id:number):Observable<RewardMessage> {
        let headers = this.getHeaders();

        return this._authHttp.get(
            this._globalService.apiHost+'/staff/rewardmessage?id='+id,
            {
                headers: headers
            }
        )
            .map(response => response.json())
            .map((response) => {
                return <RewardMessage>response.data.reward_message;
            })
            .catch(this.handleError);
    }


    private handleError (error: Response | any) {

        let errorMessage:any = {};
        // Connection error
        if(error.status == 0) {
            errorMessage = {
                success: false,
                status: 0,
                data: "Sorry, there was a connection error occurred. Please try again.",
            };
        }
        else {
            errorMessage = error.json();
        }
        return Observable.throw(errorMessage);
    }

    public static getStatusTypes():Array<any>{
        return [
            {
                label: 'Active',
                value: 1
            },
            {
                label: 'In Active',
                value: 0
            }
        ];
    }
}
