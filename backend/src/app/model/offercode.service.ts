import {Injectable} from '@angular/core';
import {Http, Headers, Response} from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs/Rx';

import {GlobalService} from './global.service';
import {StaffService} from './staff.service';

import {AuthHttp} from 'angular2-jwt';
import {Offercode, Offercodeupload} from './offercode';
import {Offercodesummary} from './offercodesummary';

@Injectable()
export class OffercodeService {

  constructor(private _globalService:GlobalService,
    private _staffService:StaffService,
    private _authHttp: AuthHttp){
  }

  // GET
  public getAllOfferCodes(): Observable<Offercode[]> {
    let headers = this.getHeaders();

    return this._authHttp.get(
        this._globalService.apiHost+'/staff/offercodes',
        {
            headers: headers
        }
    )
        .map(response => response.json())
        .map((response) => {
            return <Offercode[]>response.data.offercodes;
        })
        .catch(this.handleError);
}

public uploadOffercodes(offercode:Offercodeupload): Observable<Offercode[]>{
  let headers = this.getHeaders();

  return this._authHttp.post(
    this._globalService.apiHost+'/staff/offercodeupload',
    JSON.stringify(offercode),
    {
        headers: headers
    }
)
    .map(response => response.json())
    .map((response) => {
        return response;
    })
    .catch(this.handleError);
}

private getHeaders():Headers {
  return new Headers({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer '+this._staffService.getToken(),
  });
}

private handleError (error: Response | any) {

  let errorMessage:any = {};
  // Connection error
  if(error.status == 0) {
      errorMessage = {
          success: false,
          status: 0,
          data: "Sorry, there was a connection error occurred. Please try again.",
      };
  }
  else {
      errorMessage = error.json();
  }
  return Observable.throw(errorMessage);
}

public getOfferCodesummarydetails(): Observable<Offercodesummary[]> 
{
    let headers = this.getHeaders();
    return this._authHttp.get(
        this._globalService.apiHost+'/staff/getoffercodesummary',
        {
            headers: headers
        }
    )
    .map(response => response.json())
    .map((response) => {
        return <Offercodesummary[]>response.data.offercodes;
    })
    .catch(this.handleError);
}
}
