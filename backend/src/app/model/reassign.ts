export class Reassign {
    booking_id:number;
    user_id:number;
    username:string;
    assigned_on:string;
    customername:string;
    mobile:string;
    email:string;
    bill_image:string;
    billsnap_comments:string;
    statename:string;
    state:boolean;
    status:any;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}