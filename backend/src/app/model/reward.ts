export class Reward{
    id:number;
    prod_name: string;
    status:number;
    value:number;
    created_at:string;
    updated_at:string;
    imgurl:string;
    
    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}