export class Bulkassign {
    user_id:number;
    username:string;
    assigned_count:string;
    pending_count:string;
    closed_count:any;
    rejected_count:any;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}