export class Offercode{
    id : number;
    code : string;
    reward_id : number;
    status : number;
    regtill: Date;
    booktill:Date;
    campaign_id: number;
    usedon : string;
    file:string;
    product:string;
    senton: any;
    
    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}

export class Offercodeupload {
    upload: string;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}