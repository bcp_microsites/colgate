export class Vouchercode{
    id : number;
    code : string;
    reward_id : number;
    status : number;
    regtill: Date;
    booktill:Date;
    campaign_id: number;
    usedon : string;
    file:string;
    
    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}

export class Vouchercodeupload {
    upload: string;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}

export class Vouchercodesearch {
    code: string;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}