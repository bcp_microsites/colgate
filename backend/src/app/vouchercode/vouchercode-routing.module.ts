import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {VouchercodeComponent} from './vouchercode.component';

import { VouchercodeFormComponent } from "./vouchercode-form.component";

const routes: Routes = [
    {
        path: '',
        data: {
            title: 'Voucher Codes'
        },
        children: [
            {
                path: 'list',
                component: VouchercodeComponent,
                data: {
                    title: 'List',
                }
            },
            {
                path: 'upload',
                component: VouchercodeFormComponent,
                data: {
                    title: 'upload'
                }
            },
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'list'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class VouchercodeRoutingModule {}
