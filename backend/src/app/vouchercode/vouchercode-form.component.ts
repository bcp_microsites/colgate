import {Component, OnInit, OnDestroy, ViewChild} from '@angular/core';
import {Router, ActivatedRoute} from "@angular/router";
import {FormGroup, FormBuilder, FormControl, FormArray, Validators} from "@angular/forms";
import { Vouchercodeupload} from "../model/vouchercode";
import { VouchercodeService } from "../model/vouchercode.service";
import {NgForm} from '@angular/forms';
@Component({
  selector: 'app-vouchercode',
  templateUrl: './vouchercode-form.component.html',

})
export class VouchercodeFormComponent implements OnInit {

  
  private _vouchercodeupload:Vouchercodeupload;

  // Error and success messages
  private _errorMessage:string;

  // Forms
  private _form : FormGroup;

  private _formErrors:any;

  // file object
  @ViewChild('upload') upload;

  constructor(private _router:Router,
    private _activatedRoute:ActivatedRoute,private _formBuilder:FormBuilder,private VouchercodeService: VouchercodeService ) {

       // Construct form group
       this._form = _formBuilder.group({  upload: ['', Validators.compose([])] });

  }

  private _resetFormErrors():void{
    this._formErrors = { upload: {valid: true, message: ''} };
  }

  ngOnInit() {
    this._resetFormErrors();
  }

  handleFileChange(e) {

    const file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
        let reader = new FileReader();
        reader.onload = this._setFileUpload.bind(this);
        reader.readAsDataURL(file); 
  }

  private _setFileUpload(e) {
    this._vouchercodeupload = new Vouchercodeupload();
    let  reader = e.target;
    console.log(reader.result);
    this._vouchercodeupload.upload = reader.result;
}  

private _setFormErrors(errorFields:any):void{
  for (let key in errorFields) {
      let errorField = errorFields[key];
      // skip loop if the property is from prototype
      if (!this._formErrors.hasOwnProperty(key)) continue;
      // let message = errorFields[error.field];
      this._formErrors[key].valid = false;
      this._formErrors[key].message = errorField;
  }
}

public onSubmit() {
  this._resetFormErrors();
  const fileBrowser = this.upload.nativeElement;
  if(fileBrowser.files.length == 0) {
    this._vouchercodeupload.upload = '';
  }
  console.log(this._vouchercodeupload);
  this.VouchercodeService.uploadVoucherCodes(this._vouchercodeupload)
    .subscribe(
      result => {
        if (result) {
            this._router.navigate(['/voucher']);
        } 
    },
        error => {
            
            // Validation errors
            if(error.status == 422) {
                let errorFields = JSON.parse(error.data.message);
                console.log(error.data.message);
                this._setFormErrors(errorFields);
                this._errorMessage = error.data.message;
            }
            // Unauthorized Access
            else if(error.status == 401 || error.status == 403) {
             }
            // All other errors
            else {
                this._errorMessage = error.data.message;
            }
        }
    );
}



}
