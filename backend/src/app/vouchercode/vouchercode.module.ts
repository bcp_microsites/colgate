import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {VouchercodeRoutingModule} from './vouchercode-routing.module';
import {VouchercodeComponent} from './vouchercode.component';
import { VouchercodeFormComponent } from './vouchercode-form.component';
import { ReactiveFormsModule,FormsModule  } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    VouchercodeRoutingModule,
    ReactiveFormsModule
    
  ],
  declarations: [VouchercodeComponent, VouchercodeFormComponent]
})
export class VouchercodeModule { }
