import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';
// import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import {SharedModule} from '../shared/shared.module';

import { TasklistComponent } from './tasklist.component';
import { TaskComponent } from './task.component';

import { AgentRoutingModule } from './agent-routing.module';

@NgModule({
  imports: [
      CommonModule,
      // InfiniteScrollModule,
      SharedModule,
      AgentRoutingModule,
  ],
  declarations: [
      TasklistComponent,
      TaskComponent,
  ]
})
export class AgentModule { }
