import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import {CustomValidators} from 'ng2-validation';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import {ContainsValidators} from "../shared/contains-validator.directive";
import { Router, ActivatedRoute } from '@angular/router';

import {ReportDataService} from "../model/report-data.service";
import {StaffService} from "../model/staff.service";
import {GlobalService} from "../model/global.service";
import {Reassign} from "../model/reassign";
import {Customerdata} from "../model/customerdata";

import * as moment from "moment";

@Component({
    selector: 'app-task',
    templateUrl: 'task.component.html'
})
export class TaskComponent implements OnInit, OnDestroy {

    private _id:number;
    private _parameters:any;
    private _form:FormGroup;
    private _formErrors:any;
    private _winnerdata:Reassign;
    private _customerdata:Customerdata;
    private _campaignId:number;
    private _userid:number;
    private _campaignName:string;
    private _errorMessage:string;
    private _successMessage:string;
    private _submitted:boolean = false;

  constructor(private _reportDataService:ReportDataService,
              private _staffService:StaffService,
              private _router:Router,
              private _activatedRoute:ActivatedRoute,
              private _formBuilder:FormBuilder,
              private _globalService:GlobalService) {

      this._form = this._formBuilder.group({
          qty: ['', Validators.compose([
              Validators.required,
              CustomValidators.number,
          ])],
          agent_comments: ['', Validators.compose([
              Validators.required,
          ])]
      });

  }

    private _resetFormErrors():void{
        this._formErrors = {
            status: {valid: true, message: ''},
            agent_comments: {valid: true, message: ''},
        };
    }

    private _resetCustomerdata(){
        this._customerdata = new Customerdata();
        this._customerdata.status = '';
        this._customerdata.agent_comments = '';
    }

  ngOnInit() {
      this._campaignId = Number.parseInt(sessionStorage.getItem('configCmpId'));
      this._userid = this._staffService.getUserId();

      this._resetFormErrors();
      this._resetCustomerdata();

      this._parameters = this._activatedRoute.params.subscribe(params => {
          // plus(+) is to convert 'id' to number
          if(typeof params['id'] !== "undefined") {
              this._id = Number.parseInt(params['id']);
              this._errorMessage = "";
              this._reportDataService.getCustomerData(this._campaignId, this._id)
                  .subscribe(
                      configs => {
                          this._campaignName = configs.campaign_name;
                          this._winnerdata = configs.customerdata;
                      },
                      error => {
                          // unauthorized access
                          if(error.status == 401 || error.status == 403) {
                              this._staffService.unauthorizedAccess(error);
                          } else {
                              this._errorMessage = error.data.message;
                          }
                      }
                  );
          }
      });
  }

    /*public campaignConfigs(campaignId:number, winnerlistId:number, userid:number) {
        this._configs = null;
        this._winnerdata = null;
        this._reportDataService.getCampaignConfigs(campaignId, winnerlistId, userid)
            .subscribe(
                configs => {
                    this._configs = configs.campaign_config;
                    this._istn = configs.istn;
                    this._campaignName = configs.campaign_name;
                },
                error =>  {
                    // unauthorized access
                    if(error.status == 401 || error.status == 403) {
                        this._staffService.unauthorizedAccess(error);
                    } else {
                        this._errorMessage = error.data.message;
                    }
                }
            );
    }*/

    public ngOnDestroy() {
        this._parameters.unsubscribe();
        this._winnerdata = new Reassign();
    }

    /*public onSubmit(elementVal:any) {
        this._submitted = true;
        this._successMessage = '';
        this._reportDataService.processCustomerData(this._campaignId, this._userid, this._id, elementVal.status, elementVal.agent_comments)
            .subscribe(
                configs => {
                    // this._campaignName = configs.campaign_name;
                    this._successMessage = 'Data Saved Successfully!';
                },
                error =>  {
                    this._successMessage = '';
                    this._submitted = false;
                    // unauthorized access
                    if(error.status == 401 || error.status == 403) {
                        this._staffService.unauthorizedAccess(error);
                    } else {
                        this._errorMessage = error.data.message;
                    }
                }
            );
    }*/

    public approveEntry(qty:number, agentComment:string) {
      this._submitted = true;
      this._reportDataService.approveCustomerData(this._campaignId, this._userid, this._id, qty, agentComment)
          .subscribe(
              resp => {
                this._router.navigate(['/agent/tasklist']);
              },
              error =>  {
                this._successMessage = '';
                this._submitted = false;
                // unauthorized access
                if(error.status == 401 || error.status == 403) {
                  this._staffService.unauthorizedAccess(error);
                } else {
                  this._errorMessage = error.data.message;
                }
              }
          );
    }

  public rejectEntry(qty:number, agentComment:string) {
    this._submitted = true;
    this._reportDataService.rejectCustomerData(this._campaignId, this._userid, this._id, qty, agentComment)
        .subscribe(
            resp => {
              this._router.navigate(['/agent/tasklist']);
            },
            error =>  {
              this._successMessage = '';
              this._submitted = false;
              // unauthorized access
              if(error.status == 401 || error.status == 403) {
                this._staffService.unauthorizedAccess(error);
              } else {
                this._errorMessage = error.data.message;
              }
            }
        );
  }
}
