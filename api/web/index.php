<?php

// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');
defined('PAYU_KEY') or define('PAYU_KEY', 'Zrwv4LjI');
defined('PAYU_SALT') or define('PAYU_SALT', '2Vyyt7HgmJ');

/* Localhost Constants */
// defined('PAYU_SUCCESSURL') or define('PAYU_SUCCESSURL', 'http://localhost/colgate/api/web/v1/user/successpage');
// defined('PAYU_FAILUREURL') or define('PAYU_FAILUREURL', 'http://localhost/colgate/api/web/v1/user/failurepage');
// defined('ANGULAR_SUCCESSURL') or define('ANGULAR_SUCCESSURL', 'http://localhost:4200/#/successpage');
// defined('ANGULAR_FAILUREURL') or define('ANGULAR_FAILUREURL', 'http://localhost:4200/#/failurepage');

/* Staging21 Server Constants */
// defined('PAYU_SUCCESSURL') or define('PAYU_SUCCESSURL', 'http://staging21api.bigcityexperiences.co.in/v1/user/successpage');
// defined('PAYU_FAILUREURL') or define('PAYU_FAILUREURL', 'http://staging21api.bigcityexperiences.co.in/v1/user/failurepage');
// defined('ANGULAR_SUCCESSURL') or define('ANGULAR_SUCCESSURL', 'http://staging21.bigcityexperiences.co.in/#/successpage');
// defined('ANGULAR_FAILUREURL') or define('ANGULAR_FAILUREURL', 'http://staging21.bigcityexperiences.co.in/#/failurepage');

/* Live Server Constants */
defined('PAYU_SUCCESSURL') or define('PAYU_SUCCESSURL', 'http://colgateapi.bigcityexperience.com/v1/user/successpage');
defined('PAYU_FAILUREURL') or define('PAYU_FAILUREURL', 'http://colgateapi.bigcityexperience.com/v1/user/failurepage');
defined('ANGULAR_SUCCESSURL') or define('ANGULAR_SUCCESSURL', 'http://colgate.bigcityexperience.com/#/successpage');
defined('ANGULAR_FAILUREURL') or define('ANGULAR_FAILUREURL', 'http://colgate.bigcityexperience.com/#/failurepage');


defined('PAYU_TEST_BASE_URL') or define('PAYU_TEST_BASE_URL', 'https://sandboxsecure.payu.in');
defined('PAYU_LIVE_BASE_URL') or define('PAYU_LIVE_BASE_URL', '"https://secure.payu.in');
defined('PRODUCTION_STATUS') or define('PRODUCTION_STATUS', true);

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');

$config = require(__DIR__ . '/../config/web.php');

(new yii\web\Application($config))->run();
