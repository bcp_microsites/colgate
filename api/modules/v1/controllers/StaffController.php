<?php
    namespace app\modules\v1\controllers;

    use app\filters\auth\HttpBearerAuth;
    use app\helpers\AppHelper;
    use app\helpers\MsgHelper;
    use app\models\Billsnap;
    use app\models\Booking;
    use app\models\Customer;
    use app\models\CustomerData;
    use app\models\CustomerTransaction;
    use app\models\Message;
    use app\models\Offercode;
    use app\models\PaymentMethod;
    use app\models\PhoneNumber;
    use app\models\Reward;
    use app\models\RewardMessage;
    use Yii;
    use yii\helpers\Json;
    use yii\data\ActiveDataProvider;
    use yii\filters\AccessControl;
    use yii\filters\auth\CompositeAuth;
    use yii\helpers\Url;
    use yii\rbac\Permission;
    use yii\rest\ActiveController;

    use yii\web\HttpException;
    use yii\web\NotFoundHttpException;
    use yii\web\ServerErrorHttpException;

    use app\models\User;
    use app\models\LoginForm;
    use app\models\Campaign;
    use app\models\Voucher;
    use app\models\Movie;

    class StaffController extends ActiveController
    {
        public $modelClass = 'app\models\User';

        public function __construct($id, $module, $config = [])
        {
            parent::__construct($id, $module, $config);

        }

        public function actions()
        {
            return [];
        }

        public function behaviors()
        {
            $behaviors = parent::behaviors();

            $behaviors['authenticator'] = [
                'class' => CompositeAuth::className(),
                'authMethods' => [
                    HttpBearerAuth::className(),
                ],

            ];

            $behaviors['verbs'] = [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'index'  => ['get'],
                    'view'   => ['get'],
                    'create' => ['post'],
                    'update' => ['put'],
                    'delete' => ['delete'],
                    'login'  => ['post'],
                    'getPermissions'    =>  ['get'],
                    'campaigns' => ['get'],
                    'campaign' =>['get'],
                    'createcampaign'=>['post'],
                    'updatecampaigns'=>['put'],
                    'users' => ['get', 'post'],
                    'user'=>['get'],
                    'createuser'=>['post'],
                    'updateUser' => ['put'],
                    'tldashboard' => ['get'],
                    'bulkassignlist' =>['get'],
                    'bulkassigntoagent' => ['post'],
                    'agentlist'         =>['get'],
                    'reassignlist' => ['get'],
                    'reassigntoagent' => ['post'],
                    'agentcampaignlist' => ['get'],
                    'agentdatalist' => ['get'],
                    'getbookingdata' => ['get'],
                    'getassignedlist' =>['get'],
                    'agentapprovedbooking'=>['post'],
                    'agentrejectbooking'=>['post'],
                    'rewardmessages' => ['get'],
                    'rewardmessage' => ['get'],
                    'rewards' => ['get'],
                    'createrewardmessage' =>['post'],
                    'updaterewardmessages' => ['put'],
                    'voucherrewardreport' => ['get'],
                    'registerreport' => ['get'],
                    'getuniquecustdata' => ['get'],
                    'bookingreport' => ['get'],
                    'getflaggednum' => ['get'],
                    'addphonenumber' =>['post'],
                    'getflaggedseriesdata' => ['get'],
                    'getpaymentmethods' => ['get'],
                    'cashbackdata' => ['get'],
                    'cashbackstatusupdate' => ['post'],
                    'cashbaackreport' => ['get'],
                    'vouchercodes' => ['get'],
                    'movies'       => ['get'],
                    'moviesupload' => ['post'],
                    'moviesearch'  => ['get'],
                    'moviedelete'  => ['get'],
                ],
            ];

            // remove authentication filter
            $auth = $behaviors['authenticator'];
            unset($behaviors['authenticator']);

            // add CORS filter
            $behaviors['corsFilter'] = [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
                    'Access-Control-Request-Headers' => ['*'],
                ],
            ];

            // re-add authentication filter
            $behaviors['authenticator'] = $auth;
            // avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)
            $behaviors['authenticator']['except'] = ['options', 'login'];


	        // setup access
	        $behaviors['access'] = [
		        'class' => AccessControl::className(),
		        'only' => ['getPermissions','campaigns' , 'campaign','createcampaign','updatecampaigns','users','user','createuser','updateUser','rewardmessages','rewardmessage','rewards','createrewardmessage','updaterewardmessages','voucherrewardreport','registerreport','bookingreport','getuniquecustdata','getflaggednum','addphonenumber','getflaggedseriesdata','getpaymentmethods','cashbackdata','cashbackstatusupdate','cashbaackreport', 'vouchercodes', 'movies', 'moviesupload', 'moviesearch', 'moviedelete'],
                //only be applied to
		        'rules' => [
			        [
				        'allow' => true,
				        'actions' => ['getPermissions','campaigns' , 'campaign','createcampaign','updatecampaigns','rewardmessages','rewardmessage','rewards','createrewardmessage','updaterewardmessages','voucherrewardreport','registerreport','bookingreport','getuniquecustdata','getflaggednum','addphonenumber','getflaggedseriesdata','getpaymentmethods','cashbackdata','cashbackstatusupdate','cashbaackreport', 'vouchercodes', 'movies', 'moviesupload', 'moviesearch', 'moviedelete'],
				        'roles' => ['admin'],
			        ],
                    [
                        'allow' => true,
                        'actions' => ['getPermissions','campaigns' , 'campaign','createcampaign','updatecampaigns','users','user','createuser','updateUser'],
                        'roles' => ['superadmin'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['campaigns','tldashboard','bulkassignlist','bulkassigntoagent','agentlist','reassignlist','reassigntoagent','getassignedlist'],
                        'roles' => ['staff'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['agentcampaignlist','agentdatalist','getcustomerdata','agentapprovedbooking','agentrejectbooking'],
                        'roles' => ['user'],
                    ],
		        ],
	        ];

            return $behaviors;
        }

	    /**
	     * Return list of staff members
	     *
	     * @return ActiveDataProvider
	     */
        public function actionIndex(){
            return new ActiveDataProvider([
                'query' =>  User::find()->where([
	                '!=', 'status', -1
                ])->andWhere([
	                'in', 'role', [User::ROLE_STAFF, User::ROLE_ADMIN]
                ])
            ]);
        }

	    /**
	     * Return requested staff member information
	     *
	     * Request: /v1/staff/2
	     *
	     * Sample Response:
	     * {
		 *   "success": true,
		 *   "status": 200,
		 *   "data": {
		 *	        "id": 2,
		 *		    "username": "staff",
		 *		    "email": "staff@staff.com",
		 *		    "unconfirmed_email": "lygagohur@hotmail.com",
		 *		    "role": 50,
		 *		    "role_label": "Staff",
		 *		    "last_login_at": "2017-05-20 18:58:40",
		 *		    "last_login_ip": "127.0.0.1",
		 *		    "confirmed_at": "2017-05-15 09:20:53",
		 *		    "blocked_at": null,
		 *		    "status": 10,
		 *		    "status_label": "Active",
		 *		    "created_at": "2017-05-15 09:19:02",
		 *		    "updated_at": "2017-05-21 23:31:32"
		 *	    }
		 *   }
	     *
	     * @param $id
	     *
	     * @return array|null|\yii\db\ActiveRecord
	     * @throws NotFoundHttpException
	     */
        public function actionView($id){
            $staff = User::find()->where([
	            'id'    =>  $id
            ])->andWhere([
	            '!=', 'status', -1
            ])->andWhere([
	            'in', 'role', [User::ROLE_STAFF, User::ROLE_ADMIN]
            ])->one();
            if($staff){
                return $staff;
            } else {
                throw new NotFoundHttpException("Object not found: $id");
            }
        }

	    /**
	     * Create new staff member from backend dashboard
	     *
	     * Request: POST /v1/staff/1
	     *
	     * @return User
	     * @throws HttpException
	     */
        public function actionCreate(){
            $model = new User();
            $model->load(\Yii::$app->getRequest()->getBodyParams(), '');

            if ($model->validate() && $model->save()) {
                $response = \Yii::$app->getResponse();
                $response->setStatusCode(201);
                $id = implode(',', array_values($model->getPrimaryKey(true)));
                $response->getHeaders()->set('Location', Url::toRoute([$id], true));
            } else {
                // Validation error
                throw new HttpException(422, json_encode($model->errors));
            }

            return $model;
        }

	    /**
	     * Update staff member information from backend dashboard
	     *
	     * Request: PUT /v1/staff/1
	     *  {
		 *  	"id": 20,
		 *  	"username": "testuser",
		 *  	"email": "test2@test.com",
		 *  	"unconfirmed_email": "test2@test.com",
	     *  	"password": "{password}",
		 *  	"role": 50,
		 *  	"role_label": "Staff",
		 *  	"last_login_at": null,
		 *  	"last_login_ip": null,
		 *  	"confirmed_at": null,
		 *  	"blocked_at": null,
		 *  	"status": 10,
		 *  	"status_label": "Active",
		 *  	"created_at": "2017-05-27 17:30:12",
		 *  	"updated_at": "2017-05-27 17:30:12",
		 *  	"permissions": [
		 *  		{
		 *  			"name": "manageSettings",
		 *  			"description": "Manage settings",
		 *  			"checked": false
		 *  		},
		 *  		{
		 *  			"name": "manageStaffs",
		 *  			"description": "Manage staffs",
		 *  			"checked": false
		 *  		},
		 *  		{
		 *  			"name": "manageUsers",
		 *  			"description": "Manage users",
		 *  			"checked": true
		 *  		}
		 *  	]
		 *  }
	     *
	     *
	     * @param $id
	     *
	     * @return array|null|\yii\db\ActiveRecord
	     * @throws HttpException
	     */
        public function actionUpdate($id) {
            $model = $this->actionView($id);

            $model->load(\Yii::$app->getRequest()->getBodyParams(), '');

            if ($model->validate() && $model->save()) {
                $response = \Yii::$app->getResponse();
                $response->setStatusCode(200);
            } else {
                // Validation error
                throw new HttpException(422, json_encode($model->errors));
            }

            return $model;
        }

	    /**
	     * Delete requested staff member from backend dashboard
	     *
	     * Request: DELETE /v1/staff/1
	     *
	     * @param $id
	     *
	     * @return string
	     * @throws ServerErrorHttpException
	     */
        public function actionDelete($id) {
            $model = $this->actionView($id);

            $model->status = User::STATUS_DELETED;

            if ($model->save(false) === false) {
                throw new ServerErrorHttpException('Failed to delete the object for unknown reason.');
            }

            $response = \Yii::$app->getResponse();
            $response->setStatusCode(204);
            return "ok";
        }

	    /**
	     * Handle the login process for staff members for backend dashboard
	     *
	     * Request: POST /v1/staff/login
	     *
	     *
	     * @return array
	     * @throws HttpException
	     */
        public function actionLogin(){
            $model = new LoginForm();

	        $model->roles = [
	            User::ROLE_ADMIN,
	            User::ROLE_STAFF,
                User::ROLE_SUPERADMIN,
                User::ROLE_USER,
	        ];
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                $user = $model->getUser();
                $user->generateAccessTokenAfterUpdatingClientInfo(true);

                $response = \Yii::$app->getResponse();
                $response->setStatusCode(200);
                $id = implode(',', array_values($user->getPrimaryKey(true)));
                $user_role = User::findIdentity($id);
                $responseData = [
                    'id'    =>  $id,
                    'access_token' => $user->access_token,
                    'role'=>$user_role->role
                ];

                return $responseData;
            } else {
                // Validation error
                throw new HttpException(422, json_encode($model->errors));
            }
        }


	    /**
	     * Return list of available permissions for the staff.  The function will be called when staff form is loaded in backend.
	     *
	     * Request: GET /v1/staff/get-permissions
	     *
	     * Sample Response:
	     * {
		 *		"success": true,
		 *		"status": 200,
		 *		"data": {
		 *			"manageSettings": {
		 *				"name": "manageSettings",
		 *				"description": "Manage settings",
		 *				"checked": false
		 *			},
		 *			"manageStaffs": {
		 *				"name": "manageStaffs",
		 *				"description": "Manage staffs",
	     *				"checked": false
		 *			}
		 *		}
		 *	}
	     */
	    public function actionGetPermissions(){
		    $authManager = Yii::$app->authManager;

		    /** @var Permission[] $permissions */
		    $permissions = $authManager->getPermissions();

		    /** @var array $tmpPermissions to store list of available permissions */
		    $tmpPermissions = [];

		    /**
		     * @var string $permissionKey
		     * @var Permission $permission
		     */
		    foreach($permissions as $permissionKey => $permission) {
			    $tmpPermissions[] = [
		            'name'          =>  $permission->name,
		            'description'   =>  $permission->description,
		            'checked'       =>  false,
		        ];
		    }

		    return $tmpPermissions;
	    }

        public function actionOptions($id = null) {
            return "ok";
        }

        public function actionCampaigns() {
            $user                  = User::findIdentity(\Yii::$app->user->getId());
            if(isset($user->campaign_id) && !empty($user->campaign_id))
            {
                $campaignid            = explode(',', $user->campaign_id);
                $campaigns             = Campaign::find()->where(['IN','id',$campaignid])->all();
            }
            else{
                $campaigns             = Campaign::find()->all();
            }
            $responseData = [
                'campaigns'  => $campaigns,
                'user_role'  => $user->role
            ];

            return $responseData;
        }

        public function actionCampaign($id) {
            $user         = User::findIdentity(\Yii::$app->user->getId());
            $campaigns    = Campaign::find()->where(['id'=>$id])->one();
            $responseData = [
                'campaigns'  => $campaigns,
                'user_role'  => $user->role,
            ];
            return $responseData;
        }

        public function actionCreatecampaign(){
            $model = new Campaign();
            $model->load(\Yii::$app->getRequest()->getBodyParams(), '');
            $model->status          = Campaign::STATUS_ACTIVE;
            $model->created_date    = date('Y-m-d H:i:s');
            $model->updated_date    = date('Y-m-d H:i:s');
            if (Yii::$app->request->post('sitebanner')) {
                $file = Yii::$app->request->post('sitebanner');
                if (isset($file)) {
                    // Decode base64 data
                    list($type, $data) = explode(';', $file);
                    list(, $data) = explode(',', $data);
                    $file_data = base64_decode($data);

                    // Get file mime type
                    $finfo = finfo_open();
                    $file_mime_type = finfo_buffer($finfo, $file_data, FILEINFO_MIME_TYPE);

                    // File extension from mime type
                    if ($file_mime_type == 'image/jpeg' || $file_mime_type == 'image/jpg')
                        $file_type = 'jpeg';
                    else if ($file_mime_type == 'image/png')
                        $file_type = 'png';
                    else if ($file_mime_type == 'image/gif')
                        $file_type = 'gif';
                    else
                        $file_type = 'other';

                    if (in_array($file_type, ['jpeg', 'png', 'gif'])) {
                        $file_name = uniqid() . '.' . $file_type;
                        file_put_contents('uploads/' . $file_name, $file_data);

                        if (isset($model->sitebanner)) {
                            if (file_exists('uploads/' . $model->sitebanner)) {
                                unlink('uploads/' . $model->sitebanner);
                            }
                        }

                        $uploaded_file_url = \yii\helpers\Url::home(true) . 'uploads/' . $file_name;
                        $model->sitebanner = $uploaded_file_url;
                    }
                }
            }
            if ($model->validate() && $model->save()) {
                $response = \Yii::$app->getResponse();
                $response->setStatusCode(200);
            } else {
                // Validation error
                throw new HttpException(422, json_encode($model->errors));
            }

            return $model;
        }

        public function actionUpdatecampaigns($id) {
            $model = Campaign::findOne($id);
            $model->load(\Yii::$app->getRequest()->getBodyParams(), '');
            $model->updated_date = date('Y-m-d H:i:s');
            if (Yii::$app->request->post('sitebanner')) {
                $file = Yii::$app->request->post('sitebanner');
                if (isset($file) && !empty($file)) {
                    // Decode base64 data
                    list($type, $data) = explode(';', $file);
                    list(, $data) = explode(',', $data);
                    $file_data = base64_decode($data);

                    // Get file mime type
                    $finfo = finfo_open();
                    $file_mime_type = finfo_buffer($finfo, $file_data, FILEINFO_MIME_TYPE);

                    // File extension from mime type
                    if ($file_mime_type == 'image/jpeg' || $file_mime_type == 'image/jpg')
                        $file_type = 'jpeg';
                    else if ($file_mime_type == 'image/png')
                        $file_type = 'png';
                    else if ($file_mime_type == 'image/gif')
                        $file_type = 'gif';
                    else
                        $file_type = 'other';

                    if (in_array($file_type, ['jpeg', 'png', 'gif'])) {
                        $file_name = uniqid() . '.' . $file_type;
                        file_put_contents('uploads/' . $file_name, $file_data);

                        if (isset($model->sitebanner)) {
                            if (file_exists('uploads/' . $model->sitebanner)) {
                                unlink('uploads/' . $model->sitebanner);
                            }
                        }

                        $uploaded_file_url = \yii\helpers\Url::home(true) . 'uploads/' . $file_name;
                        $model->sitebanner = $uploaded_file_url;
                    }
                }
                else{
                     $model->sitebanner = $model->sitebanner;
                }
            }

            if ($model->validate() && $model->save()) {
                $response = \Yii::$app->getResponse();
                $response->setStatusCode(200);
            } else {
                // Validation error
                throw new HttpException(422, json_encode($model->errors));
            }

            return $model;
        }

        public function actionUsers($action) {
            if($action=='index'){
                $users = User::find()->all();
                return $users;
            }
            else{
                $campaigns  = Campaign::find()->where(['status'=>Campaign::STATUS_ACTIVE])->orderBy(['id'=>SORT_ASC])->all();
                return $campaigns;
            }
        }

        public function actionUser($id){
            $user = User::findOne($id);
            $campaigns  = Campaign::find()->where(['status'=>Campaign::STATUS_ACTIVE])->orderBy(['id'=>SORT_ASC])->all();
            if($user){
                $responseData = [
                    'user'    => $user,
                    'campaigns'  => $campaigns
                ];
                return $responseData;
            } else {
                throw new NotFoundHttpException("Object not found: $id");
            }
        }

        public function actionCreateuser(){
            $model = new User();
            $model->load(\Yii::$app->getRequest()->getBodyParams(), '');
            //  $model->load(Yii::$app->request->post());
            $model->status          = User::STATUS_ACTIVE;
            $model->confirmed_at    = date('Y-m-d H:i:s');
            $model->campaign_id     = Yii::$app->request->post('campaign_id');

            if ($model->validate() && $model->save()) {
                $response = \Yii::$app->getResponse();
                $sql = "INSERT INTO auth_item(name, type, description) VALUES ('$model->username',1,'$model->username')";
                $query = Yii::$app->db->createCommand($sql)->execute();
                $response->setStatusCode(201);
            } else {
                // Validation error
                throw new HttpException(422, json_encode($model->errors));
            }

            return $model;
        }

        public function actionUpdateUser($id) {
            $model = User::findOne($id);
            $model->load(\Yii::$app->getRequest()->getBodyParams(), '');
            $model->campaign_id     = Yii::$app->request->post('campaign_id');
            if ($model->validate() && $model->save()) {
                $response = \Yii::$app->getResponse();
                $response->setStatusCode(200);
            } else {
                // Validation error
                throw new HttpException(422, json_encode($model->errors));
            }
            return $model;
        }

        public function actionRewardmessages() {
            $user                       = User::findIdentity(\Yii::$app->user->getId());
            $campaignid                 = explode(',', $user->campaign_id);
            $reward_messages = RewardMessage::find()->where(['IN','campaign_id',$campaignid])->all();
            $responseData    = [
                'reward_message'    =>  $reward_messages,
            ];
            return $responseData;

        }

        public function actionRewardmessage($id) {
            $reward_messages    = RewardMessage::find()->where(['id'=>$id])->one();
            $responseData = [
                'reward_message'  => $reward_messages,
            ];
            return $responseData;
        }

        public function actionRewards(){
            $reward = Reward::find()->where(['status'=>1])->all();
            return $reward;
        }

        public function actionCreaterewardmessage(){
            $model               = new RewardMessage();
            $model->load(\Yii::$app->getRequest()->getBodyParams(), '');
            $model->created_date = date('Y-m-d H:i:s');
            $model->updated_date = date('Y-m-d H:i:s');
            if ($model->validate() && $model->save()) {
                $response = \Yii::$app->getResponse();
                $response->setStatusCode(201);
            } else {
                // Validation error
                throw new HttpException(422, json_encode($model->errors));
            }

            return $model;
        }


        public function actionUpdaterewardmessages($id) {
            $model               = RewardMessage::findOne($id);
            $model->updated_date = date('Y-m-d H:i:s');
            $model->load(\Yii::$app->getRequest()->getBodyParams(), '');

            if ($model->validate() && $model->save()) {
                $response = \Yii::$app->getResponse();
                $response->setStatusCode(200);
            } else {
                // Validation error
                throw new HttpException(422, json_encode($model->errors));
            }

            return $model;
        }

        public function actionVoucherrewardreport(){
            $bookingmodel = new CustomerTransaction();
            $data   = $bookingmodel->getVoucherBasedRewardSummary();
            return $data;
        }

        public function actionRegisterreport($startdate,$enddate){
            $model  = new CustomerTransaction();
            if(isset($startdate) && !empty($startdate) && isset($enddate) && !empty($enddate)){
                $data   = $model->getRegisterData($startdate,$enddate);
                return $data;
            }
            else{
                throw new HttpException(422, json_encode('Start and End Date should not be empty!'));
            }
        }

        public function actionBookingreport($startdate,$enddate){
            $model  = new CustomerTransaction();
            if(isset($startdate) && !empty($startdate) && isset($enddate) && !empty($enddate)){
                $data   = $model->getBookingData($startdate,$enddate);
                return $data;
            }
            else{
                throw new HttpException(422, json_encode('Start and End Date should not be empty!'));
            }
        }

        public function actionCashbackdata($startdate,$enddate,$payment_method_id){
            $model  = new CustomerTransaction();
            if(isset($startdate) && !empty($startdate) && isset($enddate) && !empty($enddate)){
                $data   = $model->getCashbackData($startdate,$enddate,$payment_method_id);
                return $data;
            }
            else{
                throw new HttpException(422, json_encode('Start and End Date should not be empty!'));
            }
        }

        public function actionCashbaackreport($startdate,$enddate,$int_status){
            $model  = new CustomerTransaction();
            if(isset($startdate) && !empty($startdate) && isset($enddate) && !empty($enddate)){
                $data   = $model->getCashbackReport($startdate,$enddate,$int_status);
                return $data;
            }
            else{
                throw new HttpException(422, json_encode('Start and End Date should not be empty!'));
            }
        }



        public function actionGetuniquecustdata(){
            /*$model = new Campaign();
            $getdevicedata   = $model->getDeviceData();
            $getsimdata      = $model->getSimData();
            $responseData    = [
                'devicedata'    =>  $getdevicedata,
                'simdata'    =>  $getsimdata,
            ];*/
            $sql = "SELECT * FROM uniquecustomers ORDER by id";
            $data= Yii::$app->db->createCommand($sql)->queryAll();
            return $data;

        }

        public function actionAddphonenumber() {
            $model         = new PhoneNumber();
            if(Yii::$app->request->post()) {
                $phonenumber   = Yii::$app->request->post('phonenumber');
                $model->mobile              = $phonenumber;
                if($model->validate() && $model->save()){
                    $response = \Yii::$app->getResponse();
                    $response->setStatusCode(200);
                    $responseData = [
                        'message'       => 'Phone number added successfully',
                    ];
                    return $responseData;
                }
                else{
                    throw new HttpException(422, json_encode($model->errors));
                }
            }
            else {
                // Validation error
                throw new HttpException(422, json_encode("Enter phone number"));
            }
        }

        public function actionGetflaggednum(){
            $flaggednum   = PhoneNumber::find()->all();
            return $flaggednum;
        }

        public function actionGetflaggedseriesdata($mobile,$range){
            if(isset($mobile) && !empty($mobile) && isset($range) && !empty($range)){
                $model = new Campaign();
                $series_data = $model->getFlaggedSeriesdata($mobile,$range);
                $main_data = $model->getMaindata($mobile);
                $responseData = [
                    'main_data'         =>$main_data,
                    'series_data'       => $series_data,
                ];
                return $responseData;
            }
            else{
                throw new HttpException(422, json_encode("Please enter mandatory fields !!"));
            }
        }

        public function actionGetpaymentmethods(){
            $payment_method = PaymentMethod::find()->where(['status'=>PaymentMethod::STATUS_ACTIVE])->all();
            return $payment_method;
        }

        public function actionCashbackstatusupdate(){
            if(Yii::$app->request->post('status_id')){
                if(Yii::$app->request->post('customer_tran_id')){
                    $model             = new CustomerTransaction();
                    $status_id         = Yii::$app->request->post('status_id');
                    $customer_tran_id  = Yii::$app->request->post('customer_tran_id');
                    $cashbackDatas     = $model->getCashbackdataById($customer_tran_id);
                    foreach($cashbackDatas AS $cashbackData){
                        $mobile                 = $cashbackData['mobile'];
                        $campaign_id            = $cashbackData['campaign_id'];
                        $method_name            = $cashbackData['method_name'];
                        $cash                   = $cashbackData['cash'];
                        $payment_method_num     = $cashbackData['payment_method_num'];
                        $account_name           = $cashbackData['account_name'];
                        $ifsc_code              = $cashbackData['ifsc_code'];
                        $unique_id              = $cashbackData['customer_tran_id'];
                        $pay_method_id          = $cashbackData['pay_method_id'];
                        $wallet_type_id         = $cashbackData['wallet_type_id'];
                        $customername           = $cashbackData['customername'];
                        $email                  = $cashbackData['email'];
                        if($status_id == 2){
                            $this->rejectcashbackdatasentmessage($mobile,$campaign_id);
                        }
                        if($status_id == 1){
                            $this->approvedcashbackdatasentmessage($mobile,$campaign_id,$cash,$method_name);
                            $helper         = new AppHelper();
                            $authdata       = $helper->payoutAuthenticate();
                            $decode_data    = Json::decode($authdata);
                            $bearer_token   = $decode_data['data']['token'];
                            if($pay_method_id == 1){ // Bank Payment Method
                                $beneId             = 'B'.$unique_id;
                                $result             = $helper->addBeneficiary($bearer_token,$beneId,$customername,$email,$mobile,$payment_method_num,$ifsc_code,'','Test Address');
                                $decode_res_data    = Json::decode($result);
                                $status_code        = $decode_res_data['subCode'];
                                if($status_code == 200){
                                    $transferId          = 'TB'.$unique_id;
                                    $tran_res            = $helper->transferRequest($bearer_token,$beneId,$cash,$transferId,'banktransfer',$beneId.' '.$customername.' Bank transfer');
                                    $decode_tranres_data = Json::decode($tran_res);
                                    $tran_status_code    = $decode_tranres_data['subCode'];
                                    $tran_status_str     = $decode_tranres_data['message'];
                                    $referenceId         = $decode_data['data']['referenceId'];
                                    $utr                 = $decode_data['data']['utr'];
                                    $sql                 = " UPDATE customer_transactions SET status_code = ".$tran_status_code.", status_string = '".$tran_status_str."', reference_id = '".$referenceId."', utr = '".$utr."', bene_id = '".$beneId."', transfer_id = '".$transferId."', updated_date = now() WHERE id = ".$unique_id;
                                    $query               = Yii::$app->db->createCommand($sql)->execute();
                                }
                                else{
                                    $status_string       = $decode_res_data['message'];
                                    $sql                 = " UPDATE customer_transactions SET status_code = ".$status_code.", status_string = '".$status_string."', updated_date = now() WHERE id = ".$unique_id;
                                    $query               = Yii::$app->db->createCommand($sql)->execute();
                                }
                            }
                            elseif ($pay_method_id == 2){ // UPI Payment Method
                                $beneId             = 'U'.$unique_id;
                                $result             = $helper->addBeneficiary($bearer_token,$beneId,$customername,$email,$mobile,'',$ifsc_code,$payment_method_num,'Test Address');
                                $decode_res_data    = Json::decode($result);
                                $status_code        = $decode_res_data['subCode'];
                                if($status_code == 200){
                                    $transferId          = 'TU'.$unique_id;
                                    $tran_res            = $helper->transferRequest($bearer_token,$beneId,$cash,$transferId,'upi',$beneId.' '.$customername.' UPI transfer');
                                    $decode_tranres_data = Json::decode($tran_res);
                                    $tran_status_code    = $decode_tranres_data['subCode'];
                                    $tran_status_str     = $decode_tranres_data['message'];
                                    $referenceId         = $decode_data['data']['referenceId'];
                                    $sql                 = " UPDATE customer_transactions SET status_code = ".$tran_status_code.", status_string = '".$tran_status_str."', reference_id = '".$referenceId."', bene_id = '".$beneId."', transfer_id = '".$transferId."', updated_date = now() WHERE id = ".$unique_id;
                                    $query               = Yii::$app->db->createCommand($sql)->execute();
                                }
                                else{
                                    $status_string       = $decode_res_data['message'];
                                    $sql                 = " UPDATE customer_transactions SET status_code = ".$status_code.", status_string = '".$status_string."', updated_date = now() WHERE id = ".$unique_id;
                                    $query               = Yii::$app->db->createCommand($sql)->execute();
                                }
                            }
                            else{ // Wallet Type Payment Method (Paytm)
                                $beneId             = 'WP'.$unique_id;
                                $result             = $helper->addBeneficiary($bearer_token,$beneId,$customername,$email,$payment_method_num,'',$ifsc_code,'','Test Address');
                                $decode_res_data    = Json::decode($result);
                                $status_code        = $decode_res_data['subCode'];
                                if($status_code == 200){
                                    $transferId          = 'TWP'.$unique_id;
                                    $tran_res            = $helper->transferRequest($bearer_token,$beneId,$cash,$transferId,'paytm',$beneId.' '.$customername.' Wallet Type Paytm transfer');
                                    $decode_tranres_data = Json::decode($tran_res);
                                    $tran_status_code    = $decode_tranres_data['subCode'];
                                    $tran_status_str     = $decode_tranres_data['message'];
                                    $referenceId         = $decode_data['data']['referenceId'];
                                    $sql                 = " UPDATE customer_transactions SET status_code = ".$tran_status_code.", status_string = '".$tran_status_str."', reference_id = '".$referenceId."', bene_id = '".$beneId."', transfer_id = '".$transferId."', updated_date = now() WHERE id = ".$unique_id;
                                    $query               = Yii::$app->db->createCommand($sql)->execute();
                                }
                                else{
                                    $status_string       = $decode_res_data['message'];
                                    $sql                 = " UPDATE customer_transactions SET status_code = ".$status_code.", status_string = '".$status_string."', updated_date = now() WHERE id = ".$unique_id;
                                    $query               = Yii::$app->db->createCommand($sql)->execute();
                                }
                            }
                        }
                    }
                    $sql            = " UPDATE customer_transactions SET internal_status = ".$status_id." WHERE id in(".$customer_tran_id.") ";
                    $query          = Yii::$app->db->createCommand($sql)->execute();
                    if($query){
                        $response = \Yii::$app->getResponse();
                        $response->setStatusCode(200);
                    }
                    else{
                        throw new HttpException(422, json_encode('Server error! Please try after some time.'));
                    }
                }
                else{
                    throw new HttpException(422, json_encode("Atleast select one record!!"));
                }
            }
            else{
                throw new HttpException(422, json_encode("Please Select Status!!"));
            }
        }

        private function rejectcashbackdatasentmessage($mobile,$campaign_id){
            $message    = new Message();
            $msg_template = "Opps, looks like there we were unable to process your cash back request. This could be due to various reasons check your details and try again on http://staging6.bigcityexperiences.co.in or write to feedback@bigcity.in.";
            $message->sendSMS($campaign_id,$mobile,$msg_template,'Cashback Rejection Message');
        }

        private function approvedcashbackdatasentmessage($mobile,$campaign_id,$cash,$method_name){
            $message    = new Message();
            $msg_template = "Hi there, your Rs ".$cash." Cash Back has been successfully processed via ".$method_name." . Thank you, Team  BigCity";
            $message->sendSMS($campaign_id,$mobile,$msg_template,'Cashback Approved Message');
        }


        public function actionVouchercodes() {    
            $user         = User::findIdentity(\Yii::$app->user->getId());
            $vouchercodes = Voucher::find()->all();
            $responseData = [
                'vouchercodes'  => $vouchercodes,
                'user_role'  => $user->role
            ];
            return $responseData;
        }

        /* Get movies list */
        public function actionMovies() {
            $user = User::findIdentity(\Yii::$app->user->getId());
            $movies = Movie::find()->where(['status' => 1])->all();
            $responseData = [
                'movies' => $movies,
                'user_role' => $user->role
            ];
            return $responseData;;
        }

        /* Upload Movies */
        public function actionMoviesupload() {
            if (Yii::$app->request->post('upload')) {
                $file = Yii::$app->request->post('upload');

                $moviesData = array();
                $moviesBatchData = array();
                $matchedMovies = array();
            
                if (isset($file)) {
                    // Decode base64 data
                    list($type, $data) = explode(';', $file);
                    list(, $data) = explode(',', $data);
                    $file_data = base64_decode($data);

                    // Get file mime type
                    $finfo = finfo_open();
                    $file_mime_type = finfo_buffer($finfo, $file_data, FILEINFO_MIME_TYPE);

                    if (isset($file_data)) {
                        
                        $file_name = uniqid() . '.' . 'csv';
                        file_put_contents('uploads/' . $file_name, $file_data);

                        if (file_exists('uploads/' . $file_name)) {

                            $handle = fopen('uploads/' . $file_name, 'r');
                            $firstrow = 1;
                            $maxId = Movie::find()->max('id');
                            if (is_null($maxId)) {
                                $movieId = 1;
                            } else {
                                $movieId = $maxId + 1;
                            }

                            while (($fileop = fgetcsv($handle, 1000, ",")) !== false){
                                if($firstrow >= 1){
                                    $movie = $fileop[0];
                                    if (!empty($movie)) {
                                        $moviename = Movie::findOne(['name' => $movie]);

                                        if (isset($moviename) && !empty($moviename)) {
                                            array_push($matchedMovies, $movie);
                                        }
                                        else {
                                            $movies = array( 'id' => $movieId, 'name' => $fileop[0] );
                                            array_push($moviesBatchData, $movies);
                                            $movieId++;
                                        }
                                    }                                  
                                }
                                ++$firstrow;    
                            }

                            if (!empty($matchedMovies)) {
                                throw new HttpException(422, json_encode(count($matchedMovies).' duplicate Movies'));
                            } else {
                                $query =  Yii::$app->db->createCommand()->batchInsert('movies', ['id', 'name'], $moviesBatchData)->execute();
                                if (isset($query)) {
                                    $response = \Yii::$app->getResponse();
                                    $response->setStatusCode(200);
                                    $responseData = [
                                    'code'          => 100,
                                    'message'       => 'Data inserted successfully'
                                    ];
                                    return $responseData;
                                }
                            }                        
                        }
                        else {
                            throw new HttpException(422, json_encode('Server error! Please try after some time.'));
                        }
                    }
                    else {
                        throw new HttpException(422, json_encode('Error!!! Please check the file type'));
                    }
                }
                else{
                    throw new HttpException(422, json_encode("Please enter the mandatory details to proceed further"));
                }
            }
            else{
                throw new HttpException(422, json_encode('Could not recognize data'));
            } 
        }

        /* Search Movie */
        public function actionMoviesearch($movie) {                      
            if (isset($movie)) {                
                $user            = User::findIdentity(\Yii::$app->user->getId());
                $movies          = Movie::find()->where(['like', 'UPPER(name)', '%' . strtoupper($movie) . '%', false ])->all();                
                return $movies;
            }
        }

        public function actionMoviedelete($id) {
            if (isset($id)) {
                $update = Movie::updateAll(['status' => 2], ['id' => $id]);
                return $update;
            }
        }
    }   