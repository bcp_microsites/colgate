<?php
    namespace app\modules\v1\controllers;

    use app\filters\auth\HttpBearerAuth;
    use app\helpers\AppHelper;
    use app\helpers\MsgHelper;
    use app\models\Billsnap;
    use Yii;
    use yii\helpers\Json;
    use yii\data\ActiveDataProvider;
    use yii\filters\AccessControl;
    use yii\filters\auth\CompositeAuth;
    use yii\helpers\Url;
    use yii\rbac\Permission;
    use yii\rest\ActiveController;

    use yii\web\HttpException;
    use yii\web\NotFoundHttpException;
    use yii\web\ServerErrorHttpException;

    use app\models\Message;
    use app\models\Login;
    use app\models\RewardRequest;
    use app\models\RewardUniquecodes;
    use app\models\Campaign;
    use app\models\RewardCategory;
    // Microsite
    use app\models\Regsitration;
    use app\models\Voucher;
    use app\models\Area;
    use app\models\City;
    use app\models\State;
    use app\models\Booking;
    use app\models\Customer;
    use app\models\Theatre;
    use app\models\Restaurant;
    use app\models\Salon;
    use app\models\Movie;
    use app\models\Reward;
    use app\models\VoucherAuth;

    class UserController extends ActiveController
    {
        public $auth_key;
        public $modelClass = 'app\models\User';

        public function __construct($id, $module, $config = []) {
            parent::__construct($id, $module, $config);

        }

        public function actions() {
            return [];
        }

        public function behaviors() {
            $behaviors = parent::behaviors();
            $behaviors['authenticator'] = [
                'class' => CompositeAuth::className(),
                'authMethods' => [
                    HttpBearerAuth::className(),
                ],
            ]; 

            $behaviors['verbs'] = [
                'class' => \yii\filters\VerbFilter::className(),
                'actions' => [
                    'claimreward'  => ['post'],
                    'registration'=>['post'],
                    'getstatelist' => ['get'],
                    'getcitylist'  => ['post'],
                    'getcitylistbystate'  => ['post'],
                    'getarealistbycity' => ['post'],
                    'booking' => ['post'],
                    'validateotp'=>['post'],
                    'getrewardcategory'=>['post'],
                    'getstatelist' =>['get'],
                    'getsuccesspage'=>['post'],
                    'customerrankingdetails'=>['post'],
                    'validateuniquecode' =>['post'],
                    'termsconditions' =>['post'],                    
                    'getcampaign' =>['post'],
                    'gettheatres' => ['post'],
                    'getrestaurant' =>['post'],
                    'getsalon'=>['post'],
                    'getrewards'=>['get'],
                    'customerregistration'=>['post'],
                    'getmovielist' => ['get'],
                    'moviebooking' => ['post'],
                    'resendotp' => ['post'],
                    'selectreward' => ['post'],
                    'salonbooking' => ['post']
                ],
            ];

            // remove authentication filter
            $auth = $behaviors['authenticator'];
            unset($behaviors['authenticator']);

            // add CORS filter
            $behaviors['corsFilter'] = [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
                    'Access-Control-Request-Headers' => ['*'],
                ],
            ];

            // re-add authentication filter
            $behaviors['authenticator'] = $auth;
            // avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)
            $behaviors['authenticator']['except'] = ['options', 'claimreward','registration','getstatelist','getcitylist','getcitylistbystate','getarealistbycity','booking','validateotp','getrewardcategory','getstatelist','getsuccesspage','customerrankingdetails','validateuniquecode','termsconditions','getcampaign','gettheatres','getrestaurant','getsalon','getrewards','customerregistration','getmovielist', 'moviebooking', 'resendotp', 'selectreward', 'salonbooking'];
            return $behaviors;
        }

         /**
         * Generates "remember me" authentication key
         */
        public function generateAuthKey() {
            $this->auth_key = Yii::$app->security->generateRandomString();
        }

        public function auth() {
            return [
                'bearerAuth' => [
                    'class' => \yii\filters\auth\HttpBearerAuth::className(),
                ],
            ];
        }

        public function actionIndex(){
            return new ActiveDataProvider([
                'query' =>  User::find()->where([
                    '!=', 'status', -1
                ])->andWhere([
                    'role'  =>  User::ROLE_USER
                ])
            ]);
        }

        public function actionOptions($id = null) {
            return "ok";
        }

        public function getBearerAccessToken(){
            $bearer = null;
            $headers = apache_request_headers();
            if(isset($headers['Authorization'])){
                $matches = array();
                preg_match('/^Bearer\s+(.*?)$/', $headers['Authorization'], $matches);
                if(isset($matches[1])){
                    $bearer = $matches[1];
                }
            } elseif(isset($headers['authorization'])){
                $matches = array();
                preg_match('/^Bearer\s+(.*?)$/', $headers['authorization'], $matches);
                if(isset($matches[1])){
                  $bearer = $matches[1];
                }
            }
            return $bearer;
        }

        public function getUniqueAlphaNumCode() {
           $unique_code = md5(uniqid(rand(), true));
           return $unique_code;
        }

        public function getLastValueFromUrl($url) {
           $data = substr(strrchr(rtrim($url, '/'), '/'), 1);
           return $data;
        }

        public function returnVoucherDetails($user_reward,$reward_type) {
            $campaign_id = 1;
            $connection   = Yii::$app->db;   // assuming you have configured a "db" connection           
            $transaction  = $connection->beginTransaction();
            try {  
                    if(isset($user_reward['unique_code']) && !empty($user_reward['unique_code'])){                         
                        $unique_code = $user_reward['unique_code'];

                        $customer_query  = " UPDATE reward_requests SET unique_code = '".$unique_code."',updated_date = now() WHERE order_id = '".$user_reward->order_id."' AND user_id = '".$user_reward->user_id."' AND reward_id = '".$user_reward->reward_id."'";                 
                        $customer_update    = Yii::$app->db->createCommand($customer_query)->execute();                        
                        //$this->sendRewardcode($user_reward,$reward_uniquecode);
                        $campaigns   = Campaign::findOne($campaign_id);               

                        $arrParams = array(
                             array('name'=>'unique_code','value' => $unique_code), array('name'=>'number','value' =>$campaigns->contactno));
                        $mobile = $user_reward->mobile;
                        $email  = $user_reward->email ;
                        if($reward_type == 'B')
                        {    
                            $sql_templates = "SELECT * FROM reward_category rc LEFT JOIN offer_category oc ON rc.offer_id = oc.id where rc.reward_id ='".$user_reward->reward_id."'";
                            $result_template = Yii::$app->db->createCommand($sql_templates)->queryAll();
                            
                            $msg_template    = $result_template[0]['msg_api'];
                            $msg_content = $campaigns->parseCampginSmsTemplate($msg_template,$arrParams);
                            $email_template  = $result_template[0]['email_api'];
                            $email_content = $campaigns->parseCampginSmsTemplate($email_template,$arrParams);
                            $subject_template = $result_template[0]['subject_api'];

                            $message  = new Message();
                            $message->sendSMS($campaign_id,$mobile,$msg_content,$subject_template);

                            $message1 = new Message();
                            $message1->sendEmail($campaign_id,$email,$email_content,$subject_template);   
                        }    
                        $points_total_sql   = "SELECT points  FROM reward_points";
                        $points_totalresult = Yii::$app->db->createCommand($points_total_sql)->queryAll(); 
                        $points_total       = $points_totalresult[0]['points'];

                        $points_used   = "SELECT SUM(points) AS sum_points FROM reward_requests WHERE unique_code is not null";
                        $points_result = Yii::$app->db->createCommand($points_used)->queryAll(); 
                        $used_points   = $points_total - $points_result[0]['sum_points'];
                        $responseData  = [                        
                          'userid' => $user_reward->user_id,'unique_code'=>$user_reward->unique_code,
                          'orderid'=>$user_reward->order_id,'orderstatus'=> 'success','avlcappoints' => $used_points ];                                                                
                    }
                    else{
                        throw new HttpException(422, json_encode("Reward code not configured!"));
                    }  
                    $transaction->commit();                   

            } catch (Exception $e) {

                $transaction->rollBack();

                Yii::$app()->user->setFlash('error', "database error");

                $client =null;
            }

            return $responseData;
        }

        private function sendRewardcode($user_reward,$reward_uniquecode){
            $mobile = $user_reward->mobile;
            $email  = $user_reward->email;
            $campaign_id = 1;
            $unique_code = $reward_uniquecode['unique_code'];
            $exp_date = $reward_uniquecode['expiry_date'];         

            $sql     = "SELECT * FROM rewards where reward_code ='".$user_reward->reward_id."'";                 
            $reward  = Yii::$app->db->createCommand($sql)->queryAll();            
            $product_name = $reward[0]['prod_name'];
            $reward_type  = $reward[0]['reward_type'];       
            $reward_id    = $user_reward->reward_id;    

            if(isset($campaign_id) && isset($mobile)){
                $date        = date('Y-m-d H:i:s');
                $appHelper   = new AppHelper();
                $otp         = $appHelper->getUniqueRandomNum();
                $campaigns   = Campaign::findOne($campaign_id);               

                $arrParams = array(
                     array('name'=>'unique_code','value' => $unique_code),array('name'=>'number','value' =>$campaigns->contactno));

                $sql_templates = "SELECT * FROM reward_category rc LEFT JOIN offer_category oc ON rc.offer_id = oc.id where rc.reward_id ='".$reward_id."'";          
                $result_template  = Yii::$app->db->createCommand($sql_templates)->queryAll();

                $msg_template     = $result_template[0]['msg_api'];
                $msg_content      = $campaigns->parseCampginSmsTemplate($msg_template,$arrParams);
                $email_template   = $result_template[0]['email_api'];
                $email_content    = $campaigns->parseCampginSmsTemplate($email_template,$arrParams);
                $subject_template = $result_template[0]['subject_api'];

                $message  = new Message();
                $message->sendSMS($campaign_id,$mobile,$msg_content,$subject_template);

                $message1 = new Message();
                $message1->sendEmail($campaign_id,$email,$email_content,$subject_template);               
             }
            else {
                // Validation error
                throw new HttpException(422, json_encode("Permission denied."));
            }
        }

    // Microsite
     public function actionRegistration(){
        $registrationmodel = new Regsitration();
        $vchModel      = new Voucher();
        $campModel = new Campaign();
        $custModel = new Customer();
        $date   = date('Y-m-d H:i:s');
        
        if(Yii::$app->request->post()){
            $campaign_id = 1;
            $registrationmodel->mobile          = Yii::$app->request->post('mobile');
            $registrationmodel->voucher_code    = Yii::$app->request->post('voucher_code');
            $registrationmodel->created_date    = $date;
           
            if(!empty($registrationmodel->mobile) && !empty($registrationmodel->voucher_code)) {
                $today = date('Y-m-d');
                $valid_voucher = $vchModel->getValidVoucherStatus($registrationmodel->voucher_code, $today);
                if (empty($valid_voucher)) {
                    throw new HttpException(422, "The voucher code entered by you is invalid. Please check the code and try again. For T&C visit lifestyleedge.bigcityexperience.com");
                }

                $voucher_last_date = $vchModel->getVoucherLastDateStatus($today, $registrationmodel->voucher_code);
                if (empty($voucher_last_date)) {
                    throw new HttpException(422, "The last date to register for the offer is now over. For T&C visit: lifestyleedge.bigcityexperience.com ");
                }
                $monthYear = date('Y-m');
                $voucher_redeemed = $vchModel->getVoucherRedeemedStatus($registrationmodel->voucher_code, $monthYear);
                if (!$voucher_redeemed['status'] && $voucher_redeemed['error'] == 0) {
                    throw new HttpException(422, $voucher_redeemed['message']);
                }

                /* if new_user = 1 then registered user else new user */
                $new_user = 1;
                $customerRegistration = $custModel->getCustomerStatus($registrationmodel->mobile);
                if (empty($customerRegistration)) {
                    $new_user = 0;
                }

                if ($new_user) {

                    if ($valid_voucher['mobile'] != $registrationmodel->mobile) {
                        throw new HttpException(422, "You have exceeded the maximum number of tries for this month.");
                    }

                    if (!empty($valid_voucher['expiry_date']) && isset($valid_voucher['expiry_date'])) {
                        $voucher_last_date = $vchModel->getVoucherActivationStatus($registrationmodel->voucher_code);
                        if (empty($voucher_last_date)) {
                            throw new HttpException(422, "The last date to register for the offer is now over. For T&C visit: lifestyleedge.bigcityexperience.com ");
                        }
                    }

                    if ($voucher_redeemed['error'] == 1 && $voucher_redeemed['status']) {
                        $newDate = date('Y-m-01');
                        $otp = $this->generateOTP();

                        $qry = "INSERT INTO voucher_auth_mapping (voucher_code, auth_code, month_date, voucher_id) VALUES ('" . $registrationmodel->voucher_code . "', '" . $otp . "', '" . $newDate . "', " . $valid_voucher['id'] . ")";
                        $insertVoucherAuth = $vchModel->insertIntoVoucherAuth($qry);
                        if (!$insertVoucherAuth) {
                            throw new HttpException(422, 'Something went wrong. Please try later!');
                        }
                        $auth_id = Yii::$app->db->getLastInsertID();
                        $reward_id = 0;
                    } else {
                        $voucher_auth = $voucher_redeemed['data'];
                        $otp = $voucher_auth['auth_code'];
                        $auth_id = $voucher_auth['id'];
                        $reward_id = $voucher_auth['reward_id'];
                    }
                    if ($reward_id != 0) {
                        $getRewardType = Reward::findOne($reward_id);
                        $reward_type = $getRewardType->reward_type;
                    } else {
                        $reward_type = '';
                    }

                    $customerId = $customerRegistration[0]['id'];
                    $registrationmodel->otp = $otp;
                    $registrationmodel->customer_id = $customerId;
                    $registrationmodel->ipaddress = Yii::$app->request->userIP;
                    $registrationmodel->updated_date = $date;
                    $registrationmodel->reward_id = $reward_id;
                    if (!is_null($customerRegistration[0]['email'])) {
                        $registrationmodel->email = $customerRegistration[0]['email'];
                    }
                    if (!is_null($customerRegistration[0]['customer_name'])) {
                        $registrationmodel->customername = $customerRegistration[0]['customer_name'];
                    }

                    if (!is_null($customerRegistration[0]['email'])) {
                        $emailId = $customerRegistration[0]['email'];
                    } else {
                        $emailId = '';
                    }
                    if ($registrationmodel->validate() && $registrationmodel->save()) {
                        $registration_id = Yii::$app->db->getLastInsertID();
                        $sendOtp = $this->sendOTP($campaign_id, $registrationmodel->mobile, $emailId, $registrationmodel->voucher_code, $otp);
                        $response = \Yii::$app->getResponse();
                        $response->setStatusCode(200);
                        $responseData = [ 
                            'customer_registered'=> $new_user, 
                            'registration_id' => $registration_id,                                                                                    
                            'mobile' => $registrationmodel->mobile,
                            'voucher_code' =>  $registrationmodel->voucher_code,
                            'auth_id' => $auth_id,
                            'customer_id' => $customerId,
                            'otp' => $otp,
                            'reward_id' => $reward_id,
                            'reward_type' => $reward_type,
                            'voucher_id' => $valid_voucher['id']
                        ];
                        return $responseData;
                    } else {
                        throw new HttpException(422, json_encode($registrationmodel->errors));
                    }
                } else {
                    $custModel->mobile = $registrationmodel->mobile;
                    $custModel->voucher_code = $registrationmodel->voucher_code;
                    $custModel->createdon = $date;

                    if ($custModel->validate() && $custModel->save()) {
                        $customerId = Yii::$app->db->getLastInsertID();
                        $updateVoucher = Voucher::updateAll(['mobile' => $custModel->mobile], ['code' => $custModel->voucher_code]);

                        $otp = $this->generateOTP();
                        
                        $monthYear = date('Y-m-01');

                        $qry = "INSERT INTO voucher_auth_mapping (voucher_code, auth_code, month_date, voucher_id) VALUES ('" . $registrationmodel->voucher_code . "', '" . $otp . "', '" . $monthYear . "', " . $valid_voucher['id'] . ")";
                        $insertVoucherAuth = $vchModel->insertIntoVoucherAuth($qry);
                        if (!$insertVoucherAuth) {
                            throw new HttpException(422, 'Something went wrong. Please try later!');
                        }
                        $auth_id = Yii::$app->db->getLastInsertID();

                        $registrationmodel->otp = $otp;
                        $registrationmodel->customer_id = $customerId;
                        $registrationmodel->ipaddress = Yii::$app->request->userIP;
                        $registrationmodel->updated_date = $date;
                        $registrationmodel->reward_id = 0;

                        $reward_id = 0;
                        if ($registrationmodel->validate() && $registrationmodel->save()) {
                            $registration_id = Yii::$app->db->getLastInsertID();
                            $sendOtp = $this->sendOTP($campaign_id, $registrationmodel->mobile, '', $registrationmodel->voucher_code, $otp);
                            $response = \Yii::$app->getResponse();
                            $response->setStatusCode(200);
                            $responseData = [ 
                                'customer_registered'=> $new_user, 
                                'registration_id' => $registration_id,                                                                                    
                                'mobile' => $registrationmodel->mobile,
                                'voucher_code' =>  $registrationmodel->voucher_code,
                                'auth_id' => $auth_id,
                                'customer_id' => $customerId,
                                'otp' => $otp,
                                'reward_id' => $reward_id,
                                'voucher_id' => $valid_voucher['id']
                            ];
                            return $responseData;
                        } else {
                            throw new HttpException(422, json_encode($registrationmodel->errors));
                        }
                    } else {
                        throw new HttpException(422, json_encode($customermodel->errors));
                    }
                }

                //     $expiry = date('Y-m-d', strtotime($today . "+ 1 Year"));
                //     $update_voucher_expiry = $vchModel->updateVoucherExpiry($valid_voucher['id'], $expiry, $registrationmodel->mobile);
            } else{
                throw new HttpException(422,'Please enter the mandatory details to proceed further');
            }
        } else {
            throw new HttpException(422,"Permission denied.");
        }
    }

    public function actionCustomerregistration() {
        $campaign_id = 1;
        $date = date('Y-m-d H:i:s');
        if(Yii::$app->request->post()){             
            $customermodel = new Customer(); 
            $customermodel->customer_name   = Yii::$app->request->post('customername');
            $customermodel->mobile          = Yii::$app->request->post('mobile');
            $customermodel->voucher_code    = Yii::$app->request->post('voucher_code');
            $customermodel->email           = Yii::$app->request->post('email');
            $registration_id                = Yii::$app->request->post('registration_id');
            $auth_id                        = Yii::$app->request->post('auth_id');
            $customer_id                    = Yii::$app->request->post('customer_id');
            $reward_id                      = Yii::$app->request->post('reward_id');
            $voucher_id                     = Yii::$app->request->post('voucher_id');

            if (!empty($customermodel->customer_name) && isset($customermodel->customer_name) && !empty($customermodel->email) && isset($customermodel->email)) {

                $customerUpdate = Customer::updateAll(['customer_name' => $customermodel->customer_name, 'email' => $customermodel->email, 'updated_date' => $date], ['id' => $customer_id]);
                if (!$customerUpdate) {
                    throw new HttpException(422, 'Something went wrong. Please try later!');
                }

                $ipaddress = Yii::$app->request->userIP;                                
                $updateRegistration = Regsitration::updateAll(['customername' => $customermodel->customer_name, 'email' => $customermodel->email, 'ipaddress' => $ipaddress, 'updated_date' => $date], ['id' => $registration_id]);
                if (!$updateRegistration) {
                    throw new HttpException(422, 'Something went wrong. Please try later!');
                }

                $n = 0;
                if ($reward_id == 2 || $reward_id == 5) {
                    $n = 1;
                } else if ($reward_id == 3) {
                    $n = 5;
                }

                for ($i = 1; $i <= $n; $i++) {
                    $newMonthYear = date('Y-m-d', strtotime(date('Y-m-01') .'+' . $i . ' Month'));
                    $newAuthCode = $this->generateOTP();

                    $voucherModel = new VoucherAuth();
                    $voucherModel->voucher_code = $customermodel->voucher_code;
                    $voucherModel->auth_code = $newAuthCode;
                    $voucherModel->month_date = $newMonthYear;
                    $voucherModel->voucher_id = $voucher_id;
                    $voucherModel->reward_id = $reward_id;

                    if ($voucherModel->validate() && $voucherModel->save()) {
                        $newAuthId = Yii::$app->db->getLastInsertID();
                    } else {
                        throw new HttpException(422, json_encode($voucherModel->errors));
                    }
                }

                $updateVoucherAuth = VoucherAuth::updateAll(['status' => 1], ['id' => $auth_id]);
                if ($reward_id == 2 || $reward_id == 5) {
                    $sql_offer = "SELECT * FROM offercodes WHERE reward_id = " . $reward_id . " AND status != 1 limit 1";
                    $result_offer  = Yii::$app->db->createCommand($sql_offer)->queryAll();

                    $offerCodes = $result_offer[0]['code'];
                } else if ($reward_id == 3) {
                    $sql_offer = "SELECT * FROM offercodes WHERE reward_id = " . $reward_id . " AND status != 1 limit 2";
                    $result_offer  = Yii::$app->db->createCommand($sql_offer)->queryAll();

                    $offerCodes = $result_offer[0]['code'] . ',' . $result_offer[1]['code'];
                }   

                $bookModel = new Booking();
                $bookModel->voucher_code = $customermodel->voucher_code;
                $bookModel->mobile = $customermodel->mobile;
                $bookModel->registration_id = $registration_id;
                $bookModel->campaign = 1;
                $bookModel->reward = $reward_id;
                $bookModel->reward_type = 'I';
                $bookModel->created_date = date('Y-m-d H:i:s');
                $bookModel->updated_date = date('Y-m-d H:i:s');
                $bookModel->ipaddress = $ipaddress;
                $bookModel->offercode = $offerCodes;
                $bookModel->save();

                $offer_update = "UPDATE offercodes SET status = 1,senton = now() WHERE id = " . $result_offer[0]['id']; 
                Yii::$app->db->createCommand($offer_update)->execute();
                if ($reward_id == 3) {
                    $offer_update = "UPDATE offercodes SET status = 1,senton = now() WHERE id = " . $result_offer[1]['id']; 
                    Yii::$app->db->createCommand($offer_update)->execute();
                }

                $campaigns = Campaign::findOne($campaign_id);
               
                $arrParams = array(array('name' => 'offer1', 'value' => $result_offer[0]['code']));

                if ($reward_id == 3) {
                    array_push($arrParams, array('name' => 'offer2', 'value' => $result_offer[1]['code']));
                }

                $sql_templates    = "SELECT * FROM reward_messages where reward_id = " . $reward_id;          
                $result_template  = Yii::$app->db->createCommand($sql_templates)->queryOne();

                // $success_message  = $result_template['reg_validmsg'];
                $msg_template     = $result_template['messages_template'];
                $email_template   = $result_template['email_template'];
                $msg_content      = $campaigns->parseCampginSmsTemplate($msg_template,$arrParams);
                $subject_template = 'Lifestyle Edge Offercode';

                $message  = new Message();
                $message->sendSMS($campaign_id,$customermodel->mobile,$msg_content,$subject_template);  

                // $qry = "SELECT * FROM customers WHERE mobile = '" . $customermodel->mobile . "'";
                // $customer = Yii::$app->db->createCommand($qry)->queryOne();

                // $message1 = new Message();
                // array_push($arrParams, array( 'name' => 'customerName', 'value' => $customer['customer_name']));
                // $email_content = $email_template;
                // $email_content      = $campaigns->parseCampginSmsTemplate($email_template,$arrParams);
                // $message1->sendEmail($campaign_id, $customer['email'], $email_content, $subject_template);

                $response = \Yii::$app->getResponse();
                $response->setStatusCode(200);
                $responseData = [
                    'email' => $customermodel->email,
                    'mobile' => $customermodel->mobile,
                    'voucher_code' => $customermodel->voucher_code,
                    'registration_id' => $registration_id,
                    'auth_id' => $newAuthId,
                    'customer_name' => $customermodel->customer_name
                ];
                return $responseData;  
            } else {
                throw new HttpException(422, 'Please enter the mandatory details to proceed further');
            }
        } else {
            throw new HttpException(422, 'Permission denied!');
        }
    }

    private function generateOTP($auth_id = null){
        if (is_null($auth_id)) {
            // $appHelper = null;
            // $appHelper   = new AppHelper();
            // $otp = $appHelper->getUniqueRandomNum();
            $otp = $this->generateAuthCode();
        } else {
            $sql = "SELECT * FROM voucher_auth_mapping WHERE id = " . $auth_id;
            $result = Yii::$app->db->createCommand($sql)->queryOne();
            $otp = $result['auth_code'];
        }
        return $otp;
    }

    private function sendOTP($campaign_id, $mobile, $email, $voucher_code, $otp) {
        $campaigns    = Campaign::findOne($campaign_id);
        $arrParams    = array(
            array('name' => 'authCode', 'value' => $otp),
        );
        $msg_template     = $campaigns->regtext;
        $email_template   = $campaigns->reg_email_text;
        $msg_content      = $campaigns->parseCampginSmsTemplate($msg_template,$arrParams);
        $subject_template = 'OTP';
        $message  = new Message();
        $message->sendSMS($campaign_id,$mobile,$msg_content,$subject_template);

        $qry = "SELECT * FROM customers WHERE mobile = '" . $mobile . "'";
        $customer = Yii::$app->db->createCommand($qry)->queryOne();

        if ($email != '') {
            $subject_template = "Registration Email";
            $message1 = new Message();
            array_push($arrParams, array('name' => 'customerName', 'value' => $customer['customer_name']));
            $email_content      = $campaigns->parseCampginSmsTemplate($email_template,$arrParams);
            $message1->sendEmail($campaign_id,$email,$email_content,$subject_template);   
        }
        return true;
    }

    public function actionValidateotp() {
        $campaign_id = 1;
        $date = date('Y-m-d H:i:s');
        $custDataModel = new Regsitration();
        $success_message = '';

        if (Yii::$app->request->post('otp')) {
            $campaigns    = Campaign::findOne($campaign_id);        
            $otp = Yii::$app->request->post('otp');
            $registration_id = Yii::$app->request->post('registration_id');
            $voucher_code = Yii::$app->request->post('voucher_code');
            $registration = $custDataModel->validateOTPRegistration($otp, $registration_id, $voucher_code);

            if(isset($registration['registration_id']) && isset($registration['registration_id'])){
                if ($registration['reward'] != 0 ) {
                    $rewards = Reward::findOne($registration['reward']);
                    $reward_type = $rewards->reward_type;
                    if ($registration['reward'] == 1) {
                        $booking_type = 'movie';
                    } else if ($registration['reward'] == 4) {
                        $booking_type = 'salon';
                    } else {
                        $booking_type = 'instant';
                    }
                } else {
                    $booking_type = 'NA';
                }
                $response = \Yii::$app->getResponse();
                $response->setStatusCode(200);
                $responseData = ['otp' =>  $otp,'verified' => true, 'registration_id' => $registration_id, 'booking_type' => $booking_type ];
                return $responseData;
            } else {
                throw new HttpException(422, "You've entered an invalid OTP. Please re-enter the OTP or request for a new OTP again.");
            }
        } else {
            throw new HttpException(422, 'Please enter the mandatory details to proceed further');
        }
    }

    /*public function actionValidateotp(){
        $campaign_id = 1;
        $date = date('Y-m-d H:i:s');
        $custdatamodel = new Regsitration();
        $success_message ='';
        if(Yii::$app->request->post('otp')){   
            $campaigns    = Campaign::findOne($campaign_id);        
            $otp = Yii::$app->request->post('otp');
            $registration_id = Yii::$app->request->post('registration_id');
            $voucher_code = Yii::$app->request->post('voucher_code');
            $date            = date('Y-m-d H:i:s');
            $registration    = $custdatamodel->validateOTPRegistration($otp,$registration_id,$voucher_code);
            $voucher_auth_id = Yii::$app->request->post('voucher_auth_id');
            if(isset($registration['registration_id']) && isset($registration['registration_id'])){
                $voucher_code = $registration['voucher_code'];                             
                $mobile = $registration['mobile']; 
                $email = $registration['email'];                      
                $reward_type = $registration['reward_type'];
                $sql_offer     = "SELECT * FROM offercodes WHERE reward_id = ".$registration['reward_id']." AND status != 1 limit 1";          
                $result_offer  = Yii::$app->db->createCommand($sql_offer)->queryAll();   
               
                if(isset($result_offer) && !empty($result_offer)){

                    $uber_code = $result_offer[0]['code'];
                    Yii::$app->db->createCommand()->insert('booking',
                    [
                        'registration_id' => $registration['registration_id'],
                        'voucher_code' => $voucher_code,
                        'offercode' => $uber_code,
                        'created_date' => $date,
                        'campaign_id' => $campaign_id,
                        'reward_id' => $registration['reward_id'],
                        'reward_type' => $reward_type
                    ])->execute();
                    $booking_id = Yii::$app->db->getLastInsertID();
                }else{
                    throw new HttpException(422, json_encode("Invalid Offercode"));
                }  
                if($reward_type == "I"){                             
                    $offer_update = "UPDATE offercodes SET status = 1,senton = now() WHERE id = " . $result_offer[0]['id']; 
                    Yii::$app->db->createCommand($offer_update)->execute();
                    $campaigns = Campaign::findOne($campaign_id);
                   
                    $arrParams = array(array('name' => 'offer1', 'value' => $uber_code));

                    $sql_templates    = "SELECT * FROM reward_messages where reward_id = " . $registration['reward_id'];          
                    $result_template  = Yii::$app->db->createCommand($sql_templates)->queryAll();

                    $success_message  = $result_template[0]['reg_validmsg'];
                    $msg_template     = $result_template[0]['messages_template'];
                    $email_template   = $result_template[0]['email_template'];
                    $msg_content      = $campaigns->parseCampginSmsTemplate($msg_template,$arrParams);
                    $subject_template = 'HP Offercode';

                    $message  = new Message();
                    $message->sendSMS($campaign_id,$mobile,$msg_content,$subject_template);  

                    $qry = "SELECT * FROM customers WHERE mobile = '" . $mobile . "'";
                    $customer = Yii::$app->db->createCommand($qry)->queryOne();

                    $message1 = new Message();
                    array_push($arrParams, array( 'name' => 'customerName', 'value' => $customer['customer_name']));
                    $email_content      = $campaigns->parseCampginSmsTemplate($email_template,$arrParams);
                    $message1->sendEmail($campaign_id, $customer['email'], $email_content, $subject_template);
                    // print_r($a); exit();

                    $voucher_code    = $registration['voucher_code'];
                    $customer_update = "UPDATE vouchers SET status = 1,usedon = now() WHERE code = '".$voucher_code."'"; 
                    $update_query    = Yii::$app->db->createCommand($customer_update)->execute();    
                    $vchModel      = new Voucher();
                    $update_voucher_status = $vchModel->updateVouherAuthStatus($voucher_auth_id);
                }
                $response = \Yii::$app->getResponse();
                $response->setStatusCode(200);
                $responseData = ['otp' =>  $otp,'verified' => true, 'registration_id' => $registration_id, 'reward_type' => $reward_type, 'booking_id' => $booking_id, 'auth_id' => $voucher_auth_id];
                return $responseData;
            } else{
                throw new HttpException(422, "You've entered an invalid OTP. Please re-enter the OTP or request for a new OTP again.");
            }
        } else{
            // Validation error
            throw new HttpException(422,"Please enter the mandatory details to proceed further");
        }
    }*/

    public function actionGetrewardcategory(){
        $reward_category = RewardCategory::find()->all();
        return $reward_category;
    }    

    public function actionGetstatelist(){                           
        $states = State::find()->all();
        return $states;
    }

    public function actionGetcitylist(){
        $reward_id = Yii::$app->request->post('reward_id');
        $sql   = "SELECT * FROM cities";
        $cities    = Yii::$app->db->createCommand($sql)->queryAll();
        return $cities;
    }

    public function actionGetarealistbycity(){
        $city_id = Yii::$app->request->post('id');
        $reward_catid = Yii::$app->request->post('zone');
        $reward_category = RewardCategory::find()->where(['id' => $reward_catid])->one();
       
        $cityid  = !empty($city_id)?$city_id:0;

        $areas   = Area::find()->where(['city_id' => $cityid, 'zone' => $reward_category->zone])->all();    
        return $areas;
    }

    public function actionGetmovielist(){                           
        $movies = Movie::find()->all();
        return $movies;
    }

    public function actionGetcitylistbystate(){
        $paramsArray = Yii::$app->request->post('params');
        $stateArray  = Yii::$app->request->post('state');
        $state_id = $stateArray['id'];
        $stateid = !empty($state_id)?$state_id:0;
        $cities = City::find()->where(['state_id' => $stateid ])->all();
        return $cities;
    }

    public function actionMoviebooking() {
        $campaign_id = 1;
        $date = date('Y-m-d H:i:s');
        $ipaddress = Yii::$app->request->userIP;

        if(Yii::$app->request->post('booking')){ 
            $booking_data = Yii::$app->request->post('booking');

            if (
                !empty($booking_data['city']) && isset($booking_data['city']) && 
                !empty($booking_data['backup_city']) && isset($booking_data['backup_city']) && 
                !empty($booking_data['theatre']) && isset($booking_data['theatre']) && 
                !empty($booking_data['backup_theatre']) && isset($booking_data['backup_theatre']) && 
                !empty($booking_data['movie']) && isset($booking_data['movie']) && 
                !empty($booking_data['backup_movie']) && isset($booking_data['backup_movie']) && 
                !empty($booking_data['preferred_date']) && isset($booking_data['preferred_date']) && 
                !empty($booking_data['backup_date']) && isset($booking_data['backup_date']) && 
                !empty($booking_data['registration_id']) && isset($booking_data['registration_id']) && 
                !empty($booking_data['preferred_time']) && isset($booking_data['preferred_time']) && 
                !empty($booking_data['backup_time']) && isset($booking_data['backup_time'])
            ) {

                $sql_request = "SELECT * FROM registration where id = ".$booking_data['registration_id'];
                $result = Yii::$app->db->createCommand($sql_request)->queryOne();


                $newMonthYear = date('Y-m-d', strtotime(date('Y-m-01') .'+1 Month'));
                $newAuthCode = $this->generateOTP();

                $voucherModel = new VoucherAuth();
                $voucherModel->voucher_code = $result['voucher_code'];
                $voucherModel->auth_code = $newAuthCode;
                $voucherModel->month_date = $newMonthYear;
                $voucherModel->voucher_id = $booking_data['voucher_id'];
                $voucherModel->reward_id = $booking_data['reward_id'];

                if ($voucherModel->validate() && $voucherModel->save()) {
                    $newAuthId = Yii::$app->db->getLastInsertID();
                } else {
                    throw new HttpException(422, json_encode($voucherModel->errors));
                }

                $bookModel = new Booking();
                $bookModel->voucher_code = $result['voucher_code'];
                $bookModel->mobile = $booking_data['mobile'];
                $bookModel->city = $booking_data['city'];
                $bookModel->backup_city = $booking_data['backup_city'];
                $bookModel->preferred_date = $booking_data['preferred_date'];
                $bookModel->backup_date = $booking_data['backup_date'];
                $bookModel->preferred_time = $booking_data['preferred_time'];
                $bookModel->backup_time = $booking_data['backup_time'];
                $bookModel->theatre = $booking_data['theatre'];
                $bookModel->backup_theatre = $booking_data['backup_theatre'];
                $bookModel->movie = $booking_data['movie'];
                $bookModel->backup_movie = $booking_data['backup_movie'];
                $bookModel->reward_type = $booking_data['reward_type'];
                $bookModel->campaign = $campaign_id;
                $bookModel->registration_id = $booking_data['registration_id'];
                $bookModel->reward = $booking_data['reward_id'];
                $bookModel->created_date = $date;
                $bookModel->updated_date = $date;
                $bookModel->ipaddress = $ipaddress;
                $bookModel->offercode = 'xxx';

                if ($bookModel->validate() && $bookModel->save()) {
                    $bookingId = Yii::$app->db->getLastInsertID();

                    $sql_templates    = "SELECT * FROM reward_messages where reward_id = 1";          
                    $result_template  = Yii::$app->db->createCommand($sql_templates)->queryOne();
                    $msg_template     = $result_template['messages_template'];
                    $subject_template = 'Lifestyle Edge Movie Reward';

                    $message = new Message();
                    $message->sendSMS($campaign_id, $result['mobile'], $msg_template, $subject_template);

                    // $email_template   = $result_template['email_template'];
                    // $qry = "SELECT * FROM customers WHERE mobile = '" . $result['mobile'] . "'";
                    // $customer = Yii::$app->db->createCommand($qry)->queryOne();

                    // $campaigns = Campaign::findOne($campaign_id);
                    // $arrParams = array( array( 'name' => 'customerName', 'value' => $customer['customer_name']) );
                    // $email_content    = $campaigns->parseCampginSmsTemplate($email_template,$arrParams);

                    // $message1 = new Message();
                    // $message1->sendEmail($campaign_id, $customer['email'], $email_content, $subject_template);

                    $voucher_code    = $result['voucher_code'];
                    $customer_update = "UPDATE vouchers SET status = 1,usedon = now() WHERE code = '".$voucher_code."'"; 
                    $update_query    = Yii::$app->db->createCommand($customer_update)->execute(); 
                    $voucher_auth_id = $booking_data['auth_id']; 
                    $vchModel      = new Voucher();
                    $update_voucher_status = $vchModel->updateVouherAuthStatus($voucher_auth_id);

                    $response = \Yii::$app->getResponse();
                    $response->setStatusCode(200);
                    $responseData = [
                        'booking_id' => $bookingId,
                        'new_auth_id' => $newAuthId
                    ];
                    return $responseData;
                } else {
                    throw new HttpException(422, json_encode($bookModel->errors));
                }
            } else {
                throw new HttpException(422,'Please enter the mandatory details to proceed further');
            }
        } else {
            throw new HttpException(422,"Permission denied.");
        }
    }

    public function actionGetsuccesspage(){
        $campaign_id = 1;
        $paramsArray = Yii::$app->request->post('params');
        if(isset($paramsArray) && !empty($paramsArray)){
            $unique_code    = $paramsArray['u'];
            $reward_update = " UPDATE reward_requests SET status = 1,updated_date = now() WHERE unique_code = '".$unique_code."'";
            $update_query    = Yii::$app->db->createCommand($reward_update)->execute();  
                                
            $sql_request = "SELECT rr.*, r.reward_type FROM reward_requests rr LEFT JOIN rewards r ON r.reward_code = rr.reward_id where rr.unique_code ='".$unique_code."'";
            $result = Yii::$app->db->createCommand($sql_request)->queryAll();
          
            if(isset($result) && !empty($result)){                 
                $reward_id    = $result[0]['reward_id'];                
                $reward_type  = $result[0]['reward_type'];      
                $mobile = $result[0]['mobile'];
                $email  = $result[0]['email'];
            }   
            $campaigns   = Campaign::findOne($campaign_id);
            $sql_templates = "SELECT * FROM reward_category rc LEFT JOIN offer_category oc ON rc.offer_id = oc.id where rc.reward_id ='".$reward_id."'";
            $result_template = Yii::$app->db->createCommand($sql_templates)->queryAll();
            $arrParams = array(
                        array('name'=>'unique_code','value' => $unique_code), array('name'=>'number','value' =>$campaigns->contactno));
            $reward    = $result_template[0]['reward_name'];

            $msg_template     = $result_template[0]['msg_booking'];
            $msg_content      = $campaigns->parseCampginSmsTemplate($msg_template,$arrParams);
            $email_template   = $result_template[0]['email_booking'];
            $email_content    = $campaigns->parseCampginSmsTemplate($email_template,$arrParams);
            $subject_template = $result_template[0]['subject_booking'];

            $message        = new Message();
            $message->sendSMS($campaign_id,$mobile,$msg_content,$subject_template);

            $message1 = new Message();
            $message1->sendEmail($campaign_id,$email,$email_content,$subject_template); 
                         
            $sql_msg = "SELECT * FROM message_template where reward_id ='".$reward_id."' AND reward_type ='".$reward_type."' AND message_type = 2 AND level = 3";
            $msg_result = Yii::$app->db->createCommand($sql_msg)->queryAll();  
            $responseData = ['message_content' =>  $msg_content, 'terms_conditions' =>  $result_template[0]['terms_conditions'],'reward' => $reward];         
            return $responseData;
        }    
    } 

    public function actionValidateuniquecode()
    {
        $paramsArray = Yii::$app->request->post('params');
        if(isset($paramsArray) && !empty($paramsArray)){
            $otp    = $paramsArray['otp'];            
            $sql_request = "SELECT * from registration where otp ='".$otp."'";
            $result = Yii::$app->db->createCommand($sql_request)->queryAll();
            if(isset($result) && !empty($result)){
                $responseData = [  
                'otp' =>  $result[0]['otp'],                                
                ];
        }
        else
        {
            $responseData =[];
        }    
        return $responseData;
        }    
    }   

    public function actionTermsconditions()
    {
        $campaign_id = 1;
        $campaigns   = Campaign::findOne($campaign_id);

        $responseData = [  
            'terms_conditions' =>  $campaigns->txtterms,              
        ];
        return $responseData;        
    }

    public function actionGetcampaign()
    {
        $campaign_id = 1;
        $campaigns   = Campaign::findOne($campaign_id);
        $responseData = [  
        'fullImagePath' =>  $campaigns->sitebanner              
        ];
        return $responseData;
    } 

    public function actionGettheatres()
    {
        $city_id = Yii::$app->request->post('city_id');
        $city_id = !empty($city_id)?$city_id:0;
        $theatres = Theatre::find()->where(['city_id' => $city_id ])->all();
        return $theatres;
    }  

    public function actionGetrestaurant()
    {
        $city_id = Yii::$app->request->post('city_id');
        $city_id = !empty($city_id)?$city_id:0;
        $restaurants = Restaurant::find()->where(['city_id' => $city_id ])->all();
        return $restaurants;
    }

    public function actionGetsalon()
    {
        $city_id = Yii::$app->request->post('city_id');
        $city_id = !empty($city_id)?$city_id:0;
        $restaurants = Salon::find()->where(['city_id' => $city_id ])->all();
        return $restaurants;
    }

    public function actionGetrewards(){
        $rewards = Reward::find()->all();
        return $rewards;
    }

    public function actionResendotp() {
        $campaign_id = 1;
        if (Yii::$app->request->post()) {
            $mobile = Yii::$app->request->post('mobile');
            $email = Yii::$app->request->post('email');
            $voucher_code = Yii::$app->request->post('voucher_code');
            $registration_id = Yii::$app->request->post('registration_id');
            $auth_id = Yii::$app->request->post('auth_id');

            if (!empty($mobile) && isset($mobile) && !empty($email) && isset($email) && !empty($voucher_code) && isset($voucher_code) && !empty($registration_id) && isset($registration_id) && !empty($auth_id) && isset($auth_id)) {

                $otp  = $this->generateOTP($campaign_id, $mobile, $email, $voucher_code, $auth_id);                                       
                $update = Regsitration::updateAll(['updated_date' => $date, 'otp' => $otp], ['id' => $registration_id]);
                if ($update) {
                    $response = \Yii::$app->getResponse();
                    $response->setStatusCode(200);
                } else {
                    throw new HttpException(422, "Server error. Please try later!");
                }
            } else {
                throw new HttpException(422, 'Please enter the mandatory details to proceed further');
            }
        } else {
            throw new HttpException(422, "Permission denied!");
        }
    }

    public function actionSelectreward() {
        if (Yii::$app->request->post()) {
            $reward_id = Yii::$app->request->post('reward_id');
            $registration_id = Yii::$app->request->post('registration_id');
            $auth_id = Yii::$app->request->post('auth_id');
            $voucher_code = Yii::$app->request->post('voucher_code');

            if (!empty($reward_id) && isset($reward_id)) {
                $updateReward = VoucherAuth::updateAll(['reward_id' => $reward_id], ['id' => $auth_id]);
                if (!$updateReward) {
                    throw new HttpException(422, 'Something went wrong. Please try later!');
                }
                $updateVoucherReward = Voucher::updateAll(['reward_id' => $reward_id], ['code' => $voucher_code]);
                if (!$updateVoucherReward) {
                    throw new HttpException(422, 'Something went wrong. Please try later!');
                }
                $updateRegistrationReward = Regsitration::updateAll(['reward_id' => $reward_id], ['id' => $registration_id]);
                if (!$updateRegistrationReward) {
                    throw new HttpException(422, 'Something went wrong. Please try later!');
                }
                $rewards = Reward::findOne($reward_id);
                $reward_type = $rewards->reward_type;
                if ($reward_id == 1) {
                    $booking_type = 'movie';
                } else if ($reward_id == 4) {
                    $booking_type = 'salon';
                } else {
                    $booking_type = 'instant';
                }

                $response = \Yii::$app->getResponse();
                $response->setStatusCode(200);
                $responseData = [
                    'reward_type' => $reward_type,
                    'reward_id' => $reward_id,
                    'booking_type' => $booking_type
                ];
                return $responseData;
            } else {
                throw new HttpException(422, 'Please enter the mandatory details to proceed further');
            }
        } else {
            throw new HttpException(422, 'Permission denied!');
        }
    }

    private function generateAuthCode() {
        $randval = mt_rand(100000,999999);
        return $randval;
    }

    public function actionSalonbooking() {
        $campaign_id = 1;
        $date = date('Y-m-d H:i:s');
        $ipaddress = Yii::$app->request->userIP;

        if(Yii::$app->request->post('booking')){ 
            $booking_data = Yii::$app->request->post('booking');

            if (
                !empty($booking_data['city']) && isset($booking_data['city']) && 
                !empty($booking_data['backup_city']) && isset($booking_data['backup_city']) &&
                !empty($booking_data['salon']) && isset($booking_data['salon']) && 
                !empty($booking_data['backup_salon']) && isset($booking_data['backup_salon']) && 
                !empty($booking_data['preferred_date']) && isset($booking_data['preferred_date']) && 
                !empty($booking_data['backup_date']) && isset($booking_data['backup_date']) && 
                !empty($booking_data['registration_id']) && isset($booking_data['registration_id']) && 
                !empty($booking_data['preferred_time']) && isset($booking_data['preferred_time']) && 
                !empty($booking_data['backup_time']) && isset($booking_data['backup_time'])
            ) {

                $sql_request = "SELECT * FROM registration where id = ".$booking_data['registration_id'];
                $result = Yii::$app->db->createCommand($sql_request)->queryOne();

                $bookModel = new Booking();
                $bookModel->voucher_code = $result['voucher_code'];
                $bookModel->mobile = $booking_data['mobile'];
                $bookModel->city = $booking_data['city'];
                $bookModel->backup_city = $booking_data['backup_city'];
                $bookModel->preferred_date = $booking_data['preferred_date'];
                $bookModel->backup_date = $booking_data['backup_date'];
                $bookModel->preferred_time = $booking_data['preferred_time'];
                $bookModel->backup_time = $booking_data['backup_time'];
                $bookModel->salon = $booking_data['salon'];
                $bookModel->backup_salon = $booking_data['backup_salon'];
                $bookModel->reward_type = $booking_data['reward_type'];
                $bookModel->campaign = $campaign_id;
                $bookModel->registration_id = $booking_data['registration_id'];
                $bookModel->reward = $booking_data['reward_id'];
                $bookModel->created_date = $date;
                $bookModel->updated_date = $date;
                $bookModel->ipaddress = $ipaddress;
                $bookModel->offercode = 'xxx';

                if ($bookModel->validate() && $bookModel->save()) {
                    $bookingId = Yii::$app->db->getLastInsertID();

                    $sql_templates    = "SELECT * FROM reward_messages where reward_id = 1";          
                    $result_template  = Yii::$app->db->createCommand($sql_templates)->queryOne();
                    $msg_template     = $result_template['messages_template'];
                    $subject_template = 'Lifestyle Edge Salon Reward';

                    $message = new Message();
                    $message->sendSMS($campaign_id, $result['mobile'], $msg_template, $subject_template);

                    // $email_template   = $result_template['email_template'];
                    // $qry = "SELECT * FROM customers WHERE mobile = '" . $result['mobile'] . "'";
                    // $customer = Yii::$app->db->createCommand($qry)->queryOne();

                    // $campaigns = Campaign::findOne($campaign_id);
                    // $arrParams = array( array( 'name' => 'customerName', 'value' => $customer['customer_name']) );
                    // $email_content    = $campaigns->parseCampginSmsTemplate($email_template,$arrParams);

                    // $message1 = new Message();
                    // $message1->sendEmail($campaign_id, $customer['email'], $email_content, $subject_template);

                    $voucher_code    = $result['voucher_code'];
                    $customer_update = "UPDATE vouchers SET status = 1,usedon = now() WHERE code = '".$voucher_code."'"; 
                    $update_query    = Yii::$app->db->createCommand($customer_update)->execute(); 
                    $voucher_auth_id = $booking_data['auth_id']; 
                    $vchModel      = new Voucher();
                    $update_voucher_status = $vchModel->updateVouherAuthStatus($voucher_auth_id);

                    $response = \Yii::$app->getResponse();
                    $response->setStatusCode(200);
                    $responseData = [
                        'booking_id' => $bookingId
                    ];
                    return $responseData;
                } else {
                    throw new HttpException(422, json_encode($bookModel->errors));
                }
            } else {
                throw new HttpException(422,'Please enter the mandatory details to proceed further');
            }
        } else {
            throw new HttpException(422,"Permission denied.");
        }
    }
}