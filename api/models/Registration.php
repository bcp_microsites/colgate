<?php
namespace app\models;

use Yii;

/**
 * This is the model class for table "registration".
 *
 */

 class Registration extends \yii\db\ActiveRecord {

 	/**
     * @inheritdoc
     */

 	const STATUS_UNCONFIRM  = 0;
    const STATUS_CONFIRM    = 1;

    public static function tableName() {
        return 'registration';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['customername', 'email', 'mobile','campaign_id','voucher_code'], 'required'],
            [['created_date','updated_date'], 'safe'],
            [['customername', 'email', 'ipaddress'], 'string', 'max' => 50],
            [['mobile'], 'string', 'min' => 10, 'max' => 50],
            [['campaign_id'], 'integer'],
            ['otp_verified', 'default', 'value' => self::STATUS_UNCONFIRM],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'voucher_code' => 'Voucher Number',
            'customername' => 'Customer Name',
            'email' => 'Email',
            'mobile' => 'Mobile',
            'created_date' => 'Created On',
            'ipaddress' => 'IP Address',
            'otp_verified' => 'OTP Verified',
        ];
    }

    public function validateOtpRegister($otp) {
        $sql = "SELECT * FROM registration WHERE otp = '" . $otp . "' ORDER BY id DESC LIMIT 1";
    	// $sql = "SELECT * FROM registration WHERE id = " . $reg_id;
    	$data = Yii::$app->db->createCommand($sql)->queryOne();
    	if ($data['otp'] == $otp) {
            $reg_id = $data['id'];
    		$qry = "UPDATE registration SET otp_verified = 1 WHERE id = " . $reg_id;
    		$res = Yii::$app->db->createCommand($qry)->execute();
    		if ($res) {
    			return 1;
    		} else {
    			return 0;
    		}
    	} else {
    		return 2;
    	}
    }

    public function getRegistrationDetails($id) {
        $sql = "SELECT * FROM registration WHERE id = " . $id;
        $data = Yii::$app->db->createCommand($sql)->queryOne();
        return $data;
    }

    public function getRegistrationDetailsByOtp($otp) {
        $sql = "SELECT * FROM registration WHERE otp = '" . $otp . "' ORDER BY id DESC LIMIT 1";
        $data = Yii::$app->db->createCommand($sql)->queryOne();
        return $data;
    }
 }