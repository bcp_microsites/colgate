<?php
namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "wallet_types".
 *
 * @property integer $id
 * @property string $wallet_type_name
 * @property string $wallet_type_logo
 * @property integer $status
 * @property timestamp $created_date
 */
class WalletType extends \yii\db\ActiveRecord
{
	const STATUS_INACTIVE = 0;
	const STATUS_ACTIVE = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wallet_types';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['wallet_type_name', 'wallet_type_logo', 'status'], 'required'],
            [['status'], 'integer'],
            [['wallet_type_name','wallet_type_logo'], 'string'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' =>  TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => date('Y-m-d H:i:s')
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'wallet_type_name' => 'Wallet Type Name',
            'wallet_type_logo' => 'Wallet Type logo',
            'status' => 'Status',
        ];
    }


}
