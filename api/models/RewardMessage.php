<?php
namespace app\models;

use Yii;
use app\models\Reward;
use app\models\Campaign;

/**
 * This is the model class for table "reward_messages".
 *
 * @property integer $id
 * @property integer $campaign_id
 * @property integer $reward_id
 * @property string $messages_template
 * @property string $email_template
 * @property integer $status
 * @property timestamp $created_date
 * @property timestamp $updated_date
 */
class RewardMessage extends \yii\db\ActiveRecord
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reward_messages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['campaign_id','reward_id','messages_template'], 'required'],
            [['id','reward_id'], 'integer'],
            [['messages_template','email_template'], 'string'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'reward_id' => 'Reward ID',
            'messages_template' => 'Messages Template',
            'email_template' => 'Email Template',
            'status' => 'Status',
        ];
    }

    public function getReward()
    {
        return $this->hasOne(Reward::className(), ['id' => 'reward_id']);
    }

    public function getCampaign()
    {
        return $this->hasOne(Campaign::className(), ['id' => 'campaign_id']);
    }

    public function fields()
    {
        return [
            'id',
            'product' => function($model) {
                return $model->reward->prod_name;
            },
            'reward_id' => function($model) {
                return $model->reward_id;
            },
            'campaign_id' => function($model) {
                return $model->campaign_id;
            },
            'messages_template' => function($model) {
                return $model->messages_template;
            },
            'email_template' => function($model) {
                return $model->email_template;
            },
            'status' => function($model) {
                return $model->status;
            },
            'campaign_title' => function($model) {
                return $model->campaign->title;
            },
        ];
    }

    public function parseMsgTemplate($arrParams)
    {
        $msg = $this->messages_template;
        if (isset($msg))
        {
            foreach($arrParams as $param)
            {
                $msg = str_ireplace( '{{' . $param["name"] . '}}', $param["value"], $msg);
            }
            $msg = trim(preg_replace('/\s\s+/', ' ', $msg));
            return $msg;
        }
        return '';
    }

    public function parseMailTemplate($arrParams)
    {
        $msg = $this->email_template;
        if (isset($msg))
        {
            foreach($arrParams as $param)
            {
                $msg = str_ireplace( '{{' . $param["name"] . '}}', $param["value"], $msg);
            }
            $msg = trim(preg_replace('/\s\s+/', ' ', $msg));
            return $msg;
        }
        return '';
    }


    public function getRewardMessageByCampaignandReward($campaign_id,$reward_id)
    {
         $sql = "SELECT messages 
                 FROM reward_messages AS RewardMessage 
                 WHERE RewardMessage.status = 1
                       AND RewardMessage.reward_id = ".$reward_id."
                       AND RewardMessage.campaign_id = ".$campaign_id." ";
        $data= Yii::$app->db->createCommand($sql)->queryAll();
        if(isset($data[0]) && !empty($data[0]))
            return $data[0]['messages'];
        else
            return 0;
    }

}
