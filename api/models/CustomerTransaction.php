<?php
namespace app\models;

use Yii;
use yii\db\Expression;
use app\models\RewardMessage;

/**
 * This is the model class for table "booking".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property string $voucher_code
 * @property string $customername
 * @property string $email
 * @property string $mobile
 * @property integer $city_id
 * @property timestamp $created_date
 * @property string $ipaddress
 * @property string $address
 * @property string $otp
 * @property integer $status
 * @property string $lat
 * @property string $lng
 * @property integer $state_id
 * @property integer $cash
 * @property integer $campaign_id
 */
class CustomerTransaction extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    const STATUS_UNCONFIRM  = 0;
    const STATUS_CONFIRM    = 1;


    public static function tableName()
    {
        return 'customer_transactions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customername', 'email', 'mobile', 'customer_id','campaign_id','voucher_code'], 'required'],
            [['created_date','updated_date'], 'safe'],
            [['customer_id','city_id','campaign_id','state_id'], 'integer'],
            [['cash'], 'number'],
            [['lat','lng','address','voucher_code'], 'string'],
            [['customername', 'email', 'ipaddress'], 'string', 'max' => 50],
            [['mobile'], 'string', 'min' => 10, 'max' => 50],
            ['status', 'default', 'value' => self::STATUS_UNCONFIRM],
            ['status', 'in', 'range' => [self::STATUS_UNCONFIRM, self::STATUS_CONFIRM]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Customer ID',
            'voucher_code' => 'Voucher No',
            'customername' => 'Customer Name',
            'email' => 'Email',
            'mobile' => 'Mobile',
            'city_id'=>'City',
            'createdon' => 'Createdon',
            'ipaddress' => 'Ip Address',
        ];
    }

    public function getMailMsg($campaign_id,$reward_id,$customer_name,$offer_code)
    {
        $arrParams = array(
            array('name' => 'customerName', 'value' => $customer_name),
            array('name' => 'offercode', 'value' => $offer_code),
        );
        $template = RewardMessage::find()->where(['campaign_id' => $campaign_id,'reward_id' => $reward_id])->one();

        return $template->parseMailTemplate($arrParams);
    }

    public function getSMSMsg($campaign_id,$reward_id,$customer_name,$offer_code)
    {
        $arrParams = array(
            array('name' => 'customerName', 'value' => $customer_name),
            array('name' => 'offercode', 'value' => $offer_code),
        );
        $template = RewardMessage::find()->where(['campaign_id' => $campaign_id,'reward_id' => $reward_id])->one();
        return isset($template)?$template->parseMsgTemplate($arrParams):0;
    }

    public function getRegCountByMobile($campaign_id,$mobile){
        $sql = "SELECT count(*) AS reg_count FROM booking WHERE status=1 AND mobile = '".$mobile."' AND campaign_id = "
            .$campaign_id;
        $data= Yii::$app->db->createCommand($sql)->queryAll();
        return !empty($data[0]['reg_count'])?$data[0]['reg_count']:0;
    }

    public function getCustomerDataID($customer_id){
        $sql = "SELECT * FROM booking WHERE customer_id = ".$customer_id." ORDER BY id DESC ";
        $data= Yii::$app->db->createCommand($sql)->queryAll();
        if(isset($data[0]) && !empty($data[0]))
            return $data[0];
        else
            return '';
    }

    public function getVoucherBasedRewardSummary(){
        $sql = "SELECT Voucher.code AS voucher_code,
                       VchReward.prod_name AS voucher_product,
                       BookReward.prod_name AS customerget_product,
                       Booking.mobile,
                       Booking.offercode 
                FROM booking AS Booking
                LEFT JOIN vouchers AS Voucher ON (Voucher.code = Booking.voucher_code)
                LEFT JOIN rewards AS VchReward ON (VchReward.id = Voucher.reward_id )
                LEFT JOIN rewards AS BookReward ON (BookReward.id = Booking.reward_id)
                WHERE Booking.status =1 " ;

        $data= Yii::$app->db->createCommand($sql)->queryAll();
        return $data;
    }

    public function getRegisterData($startdate,$enddate){
        $sql = "SELECT Reg.id AS reg_id,
                      Reg.voucher_code,
                      Reg.customername,
                      Reg.mobile,
                      Reg.email,
                      Reg.created_date
                FROM registration AS Reg
                WHERE Reg.campaign_id = 1
                      AND Reg.id NOT IN (SELECT registration_id FROM booking)
                      AND Reg.created_date::date BETWEEN '" . $startdate . "' AND '" . $enddate . "'
                ORDER BY Reg.id";

        $data= Yii::$app->db->createCommand($sql)->queryAll();
        return $data;
    }

    public function getBookingData($startdate,$enddate){
        $sql = "SELECT Booking.id AS booking_id,
                       Booking.voucher_code,
                       Reg.customername,
                       Reg.mobile,
                       Reg.email,
                       City.cityname,
                       Theatre.theatre_name,
                       Booking.createdon,
                       Movie.name movie,
                       Booking.prefered_date,
                       Booking.prefered_time,
                       Booking.seats,
                       Booking.cost_per_ticket,
                       Booking.grand_total,
                       Booking.discount,
                       Booking.total
                FROM booking AS Booking
                JOIN registration AS Reg ON (Reg.id = Booking.registration_id)
                JOIN cities AS City ON (City.id = Booking.city_id)
                JOIN theatres AS Theatre ON (Theatre.id = Booking.theatre_id)
                JOIN movies as Movie ON (Movie.id = Booking.movie_id)
                WHERE Reg.campaign_id = 1
                      AND Booking.status  = 1
                      AND Booking.createdon::date BETWEEN '".$startdate."' AND '".$enddate."'
                      AND Booking.id IN (SELECT booking_id FROM transaction WHERE status = 'success')
                ORDER BY Booking.id";

        $data= Yii::$app->db->createCommand($sql)->queryAll();
        return $data;
    }

    public function getCashbackData($startdate,$enddate,$payment_method_id){
        $sql = "SELECT CustomerTran.id AS customer_tran_id,
                       CustomerTran.voucher_code,
                       CustomerTran.customername,
                       CustomerTran.mobile,
                       CustomerTran.email,
                       CustomerTran.created_date,
                       CustomerTran.cash,
                       CustomerTran.ipaddress,
                       PaymentMethod.method_name,
                       WalletType.wallet_type_name                  
                FROM customer_transactions AS CustomerTran
                JOIN payment_methods AS PaymentMethod ON (PaymentMethod.id = CustomerTran.payment_method_id)
                LEFT JOIN wallet_types AS WalletType ON (WalletType.id = CustomerTran.wallet_type_id)
                WHERE CustomerTran.campaign_id = 1
                      AND CustomerTran.status  = 1
                      AND CustomerTran.internal_status = 0
                      AND CustomerTran.created_date::date BETWEEN '".$startdate."' AND '".$enddate."'
                      AND CustomerTran.payment_method_id = ".$payment_method_id."
                ORDER BY CustomerTran.id";

        $data= Yii::$app->db->createCommand($sql)->queryAll();
        return $data;
    }

    public function getCashbackReport($startdate,$enddate,$int_status){
        $sql = "SELECT CustomerTran.id AS customer_tran_id,
                       CustomerTran.voucher_code,
                       CustomerTran.customername,
                       CustomerTran.mobile,
                       CustomerTran.email,
                       CustomerTran.created_date,
                       CustomerTran.cash,
                       CustomerTran.ipaddress,
                       PaymentMethod.method_name,
                       WalletType.wallet_type_name,
                       CASE WHEN CustomerTran.internal_status = 1 THEN 'Approved' ELSE 'Rejected' END AS intstat   
                FROM customer_transactions AS CustomerTran
                JOIN payment_methods AS PaymentMethod ON (PaymentMethod.id = CustomerTran.payment_method_id)
                LEFT JOIN wallet_types AS WalletType ON (WalletType.id = CustomerTran.wallet_type_id)
                WHERE CustomerTran.campaign_id = 1
                      AND CustomerTran.status  = 1
                      AND CustomerTran.internal_status != 0 ";
                      if(isset($int_status) && !empty($int_status)){
                          $sql.= " AND CustomerTran.internal_status = ".$int_status;
                       }

                $sql.= " AND CustomerTran.created_date::date BETWEEN '".$startdate."' AND '".$enddate."'
                      
                ORDER BY CustomerTran.id";

        $data= Yii::$app->db->createCommand($sql)->queryAll();
        return $data;
    }

    public function getCashbackdataById($customer_tran_id){
        $sql = "SELECT CustomerTran.id AS customer_tran_id,
                       CustomerTran.campaign_id,
                       CustomerTran.voucher_code,
                       CustomerTran.customername,
                       CustomerTran.mobile,
                       CustomerTran.email,
                       CustomerTran.created_date,
                       CustomerTran.cash,
                       PaymentMethod.method_name,
                       WalletType.wallet_type_name,
                       CustomerTran.payment_method_num,
                       CustomerTran.account_name,
                       CustomerTran.ifsc_code,
                       PaymentMethod.id AS pay_method_id,
                       WalletType.id AS wallet_type_id     
                FROM customer_transactions AS CustomerTran
                JOIN payment_methods AS PaymentMethod ON (PaymentMethod.id = CustomerTran.payment_method_id)
                LEFT JOIN wallet_types AS WalletType ON (WalletType.id = CustomerTran.wallet_type_id)
                WHERE CustomerTran.id in (".$customer_tran_id.")
                ORDER BY CustomerTran.id";

        $data= Yii::$app->db->createCommand($sql)->queryAll();
        return $data;
    }



}
