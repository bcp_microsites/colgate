<?php
namespace app\models;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "movies".
 *
 * @property integer $id
 * @property string $name
 */
class Movie extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'movies';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string'],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Movie Name',
        ];
    }


}
