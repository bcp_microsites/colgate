<?php
namespace app\models;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "cities".
 *
 * @property integer $id
 * @property string $cityname
 * @property integer $state_id
 */
class City extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cityname'], 'required'],
            [['state_id'], 'integer'],
            [['cityname'], 'string'],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cityname' => 'City Name',
            'state_id' => 'State Id',
        ];
    }


}
