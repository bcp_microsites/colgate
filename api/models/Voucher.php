<?php
namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "vouchers".
 *
 * @property integer $id
 * @property integer $campaign_id
 * @property number $cash
 * @property string $code
 * @property integer $status
 * @property timestamp $usedon
 * @property date $regtill
 * @property timestamp $booktill
 */

class Voucher extends \yii\db\ActiveRecord
{
	const STATUS_UNUSED = 0;
	const STATUS_USED = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vouchers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code'], 'required'],
            [['status'], 'integer'],
            [['code'], 'string', 'max'=>100],
            ['status', 'default', 'value' => self::STATUS_UNUSED],
            ['status', 'in', 'range' => [self::STATUS_UNUSED, self::STATUS_USED]],
            [['usedon'], 'safe'],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Voucher Code',
            'status' => 'Status'
        ];
    }


    public function getVoucherStatus($vcode){
        $sql = "SELECT status
                  FROM vouchers
                  WHERE code = '".$vcode."'";
        $data= Yii::$app->db->createCommand($sql)->queryOne();
        return $data;
    }

    public function updateVoucherStatus($vcode) {
        $sql = "UPDATE vouchers SET status = " . self::STATUS_USED . " WHERE code = '" . $vcode . "'";
        $update = Yii::$app->db->createCommand($sql)->execute();
        return $update;
    }

    public function getVoucherDetails($code) {
        $sql = "SELECT * FROM vouchers WHERE code = '" . $code . "'";
        $data = Yii::$app->db->createCommand($sql)->queryOne();
        return $data;
    }

    public function checkVoucherStatus($otp){
        $sql = "SELECT status
                  FROM vouchers
                  WHERE auth_code = '".$otp."'";
        $data= Yii::$app->db->createCommand($sql)->queryOne();
        return $data;
    }
}
