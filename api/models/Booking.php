<?php
namespace app\models;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "booking".
 *
 * @property integer $id
 * @property string $voucher_code
 * @property integer $city_id
 * @property integer $theatre_id
 * @property string $movie_id
 * @property date $prefered_date
 * @property string $prefered_time
 * @property integer $seats
 * @property string $cost_per_ticket
 * @property string $grand_total
 * @property string $discount
 * @property string $total
 * @property integer $status
 * @property timestamp $createdon
 * @property timestamp $updatedon
 * @property integer $payment_status
 * @property string transaction_details
 * 
 */

class Booking extends \yii\db\ActiveRecord {
	
	/**
     * @inheritdoc
     */

    const STATUS_UNCONFIRM  = 0;
    const STATUS_CONFIRM    = 1;


    public static function tableName() {
        return 'booking';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['theatre_id','city_id','prefered_date','prefered_time','seats','voucher_code','movie_id','cost_per_ticket','grand_total','discount','total','registration_id'],'required'],
            [['createdon','updatedon'], 'safe'],
            [['voucher_code'], 'string']
        ];
    }

    /**
     * @inheritdoc
    */
    public function attributeLabels() {
        return [

        ];
    }

    public function getCustomerDetails($regId) {
    	$sql = "SELECT * FROM registration WHERE id = " . $regId;
    	$data = Yii::$app->db->createCommand($sql)->queryOne();
    	return $data;
    }

    public function getBookingDetails($id) {
        $sql = "SELECT * FROM booking WHERE id = " . $id;
        $data = Yii::$app->db->createCommand($sql)->queryOne();
        return $data;
    }

}