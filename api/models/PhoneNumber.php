<?php
namespace app\models;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "flagged_numbers".
 *
 * @property integer $id
 * @property string $mobile
 */
class PhoneNumber extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'flagged_numbers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mobile'], 'required'],
            [['mobile'], 'unique','message' => 'Username must be unique.'],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mobile' => 'Mobile Numbers'
        ];
    }


}
