<?php
namespace app\models;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "transaction".
 *

 */

class Transaction extends \yii\db\ActiveRecord {
	
	/**
     * @inheritdoc
    */

    public static function tableName() {
        return 'transaction';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['txn_id','booking_id','payu_payment_id','amount','card_mode','status'],'required'],
            [['createdon'], 'safe'],
            [['card_mode','status','txn_id'], 'string']
        ];
    }

    /**
     * @inheritdoc
    */
    public function attributeLabels() {
        return [

        ];
    }
}