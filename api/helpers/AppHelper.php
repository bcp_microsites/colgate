<?php
namespace app\helpers;

use Yii;


class AppHelper
{
    public function getUniqueRandomNum(){
        function make_seed()
        {
            list($usec, $sec) = explode(' ', microtime());
            return $sec + $usec * 1000000;
        }
        srand(make_seed());
        $randval = mt_rand(100000,999999);
        return $randval;
    }

    public function getUniqueAlphaNumCode(){
        $unique_code = md5(uniqid(rand(), true));
        return $unique_code;
    }

    public function getLastValueFromUrl($url){
        $data = substr(strrchr(rtrim($url, '/'), '/'), 1);
        return $data;
    }

    public function getDecryptdata($string){
        $privatefile = \yii\helpers\Url::home(true) . 'uploads/files/rsa_1024_priv.pem';
        $fopen_private = fopen($privatefile,"r");
        $private_key = fread($fopen_private,8192);
        fclose($fopen_private);
        $pkey_private = openssl_pkey_get_private($private_key);
        openssl_private_decrypt(base64_decode($string), $decrypted, $pkey_private);
        return $decrypted;
    }

    public function getEncryptdata($string){
        $privatefile = \yii\helpers\Url::home(true) . 'uploads/files/rsa_1024_priv.pem';
        $fopen_private = fopen($privatefile,"r");
        $private_key = fread($fopen_private,8192);
        fclose($fopen_private);
        openssl_private_encrypt($string, $encrypted, $private_key);
        $encrypted = base64_encode($encrypted);
        return $encrypted;
    }

    public function getPublicEncryptdata($string){
        $privatefile = \yii\helpers\Url::home(true) . 'uploads/files/rsakey2_1024_pub.pem';
        $fopen_private = fopen($privatefile,"r");
        $private_key = fread($fopen_private,8192);
        fclose($fopen_private);
        openssl_public_encrypt($string, $encrypted, $private_key);
        $encrypted = base64_encode($encrypted);
        return $encrypted;
    }

    public function getPublicDecryptdata($string){
        $privatefile = \yii\helpers\Url::home(true) . 'uploads/files/rsa_1024_pub.pem';
        $fopen_private = fopen($privatefile,"r");
        $private_key = fread($fopen_private,8192);
        fclose($fopen_private);
        $pkey_private = openssl_pkey_get_public($private_key);
        openssl_public_decrypt(base64_decode($string), $decrypted, $pkey_private);
        return $decrypted;
    }

    public function payoutAuthenticate(){
        $url = 'https://payout-gamma.cashfree.com/payout/v1/authorize';

        $headers = array(
            'X-Client-Id: CF3078EYMHKW1XIV02UEE',
            'X-Client-Secret: dc6dd60367b7b997471a4e65cf463911c1fa69f6'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER,  $headers);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);

        // This should be the default Content-type for POST requests
        //curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: application/x-www-form-urlencoded"));

        $result = curl_exec($ch);

        if(curl_errno($ch) !== 0) {
            error_log('cURL error when connecting to ' . $url . ': ' . curl_error($ch));
        }

        curl_close($ch);
        return $result;

    }

    public function addBeneficiary($bearer_token,$beneId,$name,$email,$phone,$bankAccount,$ifsc,$vpa,$address1){
        $url = 'https://payout-gamma.cashfree.com/payout/v1/requestTransfer';
        $params = array(
            'beneId' => $beneId,
            'name' => $name,
            'email' => $email,
            'phone' => $phone,
            'bankAccount' => $bankAccount,
            'ifsc' => $ifsc,
            'vpa' => $vpa,
            'address1' => $address1
        );

        $post_params = json_encode($params);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: Bearer ' . $bearer_token
        ));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_params);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);

        $result = curl_exec($ch);

        if(curl_errno($ch) !== 0) {
            error_log('cURL error when connecting to ' . $url . ': ' . curl_error($ch));
        }

        curl_close($ch);

        return $result;

    }

    public function transferRequest($bearer_token,$beneId,$amount,$transferId,$transferMode,$remarks){
        $url = 'https://payout-gamma.cashfree.com/payout/v1/requestTransfer';
        $params = array(
            'beneId' => $beneId,
            'amount' => $amount,
            'transferId' => $transferId,
            'transferMode' => $transferMode,
            'remarks' => $remarks
        );

        $post_params = json_encode($params);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: Bearer ' . $bearer_token
        ));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_params);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);

        $result = curl_exec($ch);

        if(curl_errno($ch) !== 0) {
            error_log('cURL error when connecting to ' . $url . ': ' . curl_error($ch));
        }

        curl_close($ch);

        return $result;

    }



}