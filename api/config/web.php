<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'timeZone' => 'Asia/Calcutta',
    'components' => [
        'request' => [
            'cookieValidationKey' => 'K0I9yOJPLBqbaam4IWrqtelfxp1m1zEXB04f5H6D',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'authManager' => [
	        'class' => 'yii\rbac\DbManager',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => true,
            'rules' => [
                'ping'  =>  'site/ping',
                [
	                'class'         => 'yii\rest\UrlRule',
	                'controller'    => 'v1/user',
	                'pluralize'     => false,
	                'tokens' => [
		                '{id}'             => '<id:\d+>',
	                ],
	                'extraPatterns' => [
		                'OPTIONS {id}'      =>  'options',
		                'GET getsitesettings'          =>  'getsitesettings',
                        'OPTIONS getsitesettings'      =>  'options',
                        'POST registration'            =>  'registration',
                        'OPTIONS registration'         =>  'options',
                        'POST validateotp'             =>  'validateotp',
                        'OPTIONS validateotp'          =>  'options',
                        'POST resendotp'               =>  'resendotp',
                        'OPTIONS resendotp'            =>  'options',
                        'GET getpaymentmethods'        =>  'getpaymentmethods',
                        'OPTIONS getpaymentmethods'    =>  'options',
                        'GET getwallettypes'           =>  'getwallettypes',
                        'OPTIONS getwallettypes'       =>  'options',
                        'POST claimcashback'           =>  'claimcashback',
                        'OPTIONS claimcashback'        =>  'options',
                        'GET getstatelist'             =>  'getstatelist',
                        'OPTIONS getstatelist'         =>  'options',
                        'GET getcitylist'              =>  'getcitylist',
                        'OPTIONS getcitylist'          =>  'options',
                        'GET getcitylistbystate'       =>  'getcitylistbystate',
                        'OPTIONS getcitylistbystate'   =>  'options',
                        'GET getstatelistwithouttn'    =>  'getstatelistwithouttn',
                        'OPTIONS getstatelistwithouttn'=>  'options',
                        'POST detectme'                =>  'detectme',
                        'OPTIONS detectme'             =>  'options',
                        'POST gettheatres'             =>  'gettheatres',
                        'OPTIONS gettheatres'          =>  'options',
                        'POST booking'                 =>  'booking',
                        'OPTIONS booking'              =>  'options',
                        'POST paymentdetails'          =>  'paymentdetails',
                        'OPTIONS paymentdetails'       =>  'options',
                        'POST successpage'          =>  'successpage',
                        'OPTIONS successpage'       =>  'options',
                        'POST failurepage'          =>  'failurepage',
                        'OPTIONS failurepage'       =>  'options',
                        'POST payment'          =>  'payment',
                        'OPTIONS payment'       =>  'options',
                        'POST bookingsummary'          =>  'bookingsummary',
                        'OPTIONS bookingsummary'       =>  'options',
                        'GET getmovies'               =>   'getmovies',
                        'OPTIONS getmovies'            =>   'options',
	                ]
                ],
                [
                    'class'         => 'yii\rest\UrlRule',
                    'controller'    => 'v1/staff',
                    'pluralize'     => false,
                    'tokens' => [
                        '{id}'             => '<id:\d+>',
                    ],
                    'extraPatterns' => [
                        'OPTIONS {id}'              =>  'options',
                        'POST login'                =>  'login',
                        'OPTIONS login'             =>  'options',
                        'GET get-permissions'       =>  'get-permissions',
                        'OPTIONS get-permissions'   =>  'options',
                        'GET campaigns'             =>  'campaigns',
                        'OPTIONS campaigns'         =>  'options',
                        'GET campaign'              =>  'campaign',
                        'OPTIONS campaign'          =>  'options',
                        'POST createcampaign'             =>  'createcampaign',
                        'OPTIONS createcampaign'          =>  'options',
                        'PUT updatecampaigns'             =>  'updatecampaigns',
                        'OPTIONS updatecampaigns'         =>  'options',
                        'GET user'                        =>  'user',
                        'OPTIONS user'                    =>  'options',
                        'GET users'                       =>  'users',
                        'POST users'                      =>  'create-user',
                        'OPTIONS users'                   =>  'options',
                        'PUT updateUser'                  =>  'update-user',
                        'OPTIONS updateUser'              =>  'options',
                        'POST createuser'                 =>  'createuser',
                        'OPTIONS createuser'              =>  'options',
                        'GET rewardmessages'              =>  'rewardmessages',
                        'OPTIONS rewardmessages'          =>  'options',
                        'GET rewardmessage'               =>  'rewardmessage',
                        'OPTIONS rewardmessage'           =>  'options',
                        'GET rewards'                     =>  'rewards',
                        'OPTIONS rewards'                 =>  'options',
                        'POST createrewardmessage'                      =>  'createrewardmessage',
                        'OPTIONS createrewardmessage'                   =>  'options',
                        'PUT updaterewardmessages'                  =>  'updaterewardmessages',
                        'OPTIONS updaterewardmessages'              =>  'options',
                        'GET voucherrewardreport'                     =>  'voucherrewardreport',
                        'OPTIONS voucherrewardreport'       =>  'options',
                        'GET registerreport'           =>  'registerreport',
                        'OPTIONS registerreport'       =>  'options',
                        'GET bookingreport'           =>  'bookingreport',
                        'OPTIONS bookingreport'       =>  'options',
                        'GET getuniquecustdata'             =>  'getuniquecustdata',
                        'OPTIONS getuniquecustdata'         =>  'options',
                        'GET getflaggednum'             =>  'getflaggednum',
                        'OPTIONS getflaggednum'         =>  'options',
                        'POST addphonenumber'                      =>  'addphonenumber',
                        'OPTIONS addphonenumber'                   =>  'options',
                        'GET getflaggedseriesdata'           =>  'getflaggedseriesdata',
                        'OPTIONS getflaggedseriesdata'       =>  'options',
                        'GET getpaymentmethods'           =>  'getpaymentmethods',
                        'OPTIONS getpaymentmethods'       =>  'options',
                        'GET cashbackdata'           =>  'cashbackdata',
                        'OPTIONS cashbackdata'       =>  'options',
                        'POST cashbackstatusupdate'           =>  'cashbackstatusupdate',
                        'OPTIONS cashbackstatusupdate'         =>  'options',
                        'GET cashbaackreport'           =>  'cashbaackreport',
                        'OPTIONS cashbaackreport'       =>  'options',
                        'GET vouchercodes'              => 'vouchercodes',
                        'OPTIONS vouchercodes'          => 'options',
                        'GET movies'                    =>  'movies',
                        'OPTIONS movies'                => 'options',
                        'POST moviesupload'             => 'moviesupload',
                        'OPTIONS moviesupload'          => 'options',
                        'GET moviesearch'               => 'moviesearch',
                        'OPTIONS moviesearch'           => 'options', 
                        'GET moviedelete'               => 'moviedelete',
                        'OPTIONS moviedelete'           => 'options', 

                    ]
                ],
                [
                    'class'         => 'yii\rest\UrlRule',
                    'controller'    => 'v1/setting',
                    'pluralize'     => false,
                    'tokens'        => [
                        '{id}'             => '<id:\d+>',
                    ],
                    'extraPatterns' => [
                        'GET public'       =>  'public',
                        'OPTIONS public'   =>  'options',
                    ]
                ],
                [
	                'class'         => 'yii\rest\UrlRule',
	                'controller'    => 'v1/page',
	                'pluralize'     => false,
	                'tokens'        => [
	                ],
	                'extraPatterns' => [
		                'GET sse'       =>  'sse',
		                'OPTIONS sse'    =>  'sse',
	                ]
                ],
            ]
        ],
        'response' => [
            'class' => 'yii\web\Response',
            'on beforeSend' => function ($event) {

                $response = $event->sender;
                if($response->format == 'html') {
                    return $response;
                }

                $responseData = $response->data;

                if(is_string($responseData) && json_decode($responseData)) {
                    $responseData = json_decode($responseData, true);
                }


                if($response->statusCode >= 200 && $response->statusCode <= 299) {
                    $response->data = [
                        'success'   => true,
                        'status'    => $response->statusCode,
                        'data'      => $responseData,
                    ];
                } else {
                    $response->data = [
                        'success'   => false,
                        'status'    => $response->statusCode,
                        'data'      => $responseData,
                    ];

                }
                return $response;
            },
        ],
        'sse' => [
	        'class' => \odannyc\Yii2SSE\LibSSE::class
        ]

    ],
    'modules' => [
        'v1' => [
            'class' => 'app\modules\v1\Module',
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
