export class Movie{
    id:number;
    moviename: string;
    city_id:number;
    area_id:number;
    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}