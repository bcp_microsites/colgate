export class City{
    id:number;
    cityname: string;
 
    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}

export class Venue {
    city_id:number;
    venue_name:string;
    id:number;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}

export class Movie{
    id:number;
    name: string;
 
    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}