import { TestBed, inject } from '@angular/core/testing';

import { VouchercodeService } from './vouchercode.service';

describe('VouchercodeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VouchercodeService]
    });
  });

  it('should be created', inject([VouchercodeService], (service: VouchercodeService) => {
    expect(service).toBeTruthy();
  }));
});
