import { Time } from "ngx-bootstrap/timepicker/timepicker.models";

export class Booking{
    city:any;
    theatre: string;
    prefereddate:string;
    timing:string;
    movie:string;  
    seat:number; 
    cost:number;   
	registration_id: string;
	voucher_code: string;
    booking_id: any;
    formatted_time: string;
}

export class ChildrenData {
    childrenAge:number;
    childrenProf:string;
    childrenId:string;
}