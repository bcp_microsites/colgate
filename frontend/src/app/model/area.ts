export class Area{
    id:number;
    areaname: string;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}