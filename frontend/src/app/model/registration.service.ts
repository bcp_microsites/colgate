import {Injectable} from '@angular/core';
import {Http, Headers, Response} from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs/Rx';

import {GlobalService} from './global.service';
import {StaffService} from './staff.service';
import {Registration} from './registration';
import { ResendCode } from "./registration";
import {AuthHttp} from 'angular2-jwt';



@Injectable()
export class RegistrationService {

    constructor(private _globalService:GlobalService,
                private _staffService:StaffService,
                private _authHttp: AuthHttp){
    }

    // POST /v1/user
    public addRegistration(registration:Registration):Observable<any>{
        let headers = this.getHeaders();
        console.log(registration);
        return this._authHttp.post(
            this._globalService.apiHost+'/user/registration',
            JSON.stringify(registration),
            {
                headers: headers
            }
        )
            .map(response => response.json())
            .map((response) => {
                return response;
            })
            .catch(this.handleError);
    }

    
    // POST /v1/user
    public resendOfferCode(resendCode:ResendCode):Observable<any>{
        let headers = this.getHeaders();
        console.log(resendCode);
        return this._authHttp.post(
            this._globalService.apiHost+'/insms/resendoffercode',
            JSON.stringify(resendCode),
            {
                headers: headers
            }
        )
            .map(response => response.json())
            .map((response) => {
                return response;
            })
            .catch(this.handleError);
    }

    public getCampaignData():Observable<any>{
        let headers = this.getHeaders();
        return this._authHttp.get(
            this._globalService.apiHost+'/user/campaigns',
            {
                headers: headers
            }
        )
            .map(response => response.json())
            .map((response) => {
                return response;
            })
            .catch(this.handleError);
    }

 

    private getHeaders():Headers {
        return new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+this._staffService.getToken(),
        });
    }

    private handleError (error: Response | any) {

        let errorMessage:any = {};
        // Connection error
        if(error.status == 0) {
            errorMessage = {
                success: false,
                status: 0,
                data: "Sorry, there was a connection error occurred. Please try again.",
            };
        }
        else {
            errorMessage = error.json();
        }
        return Observable.throw(errorMessage);
    }
}
