import {Injectable} from '@angular/core';
import {Http, Headers, Response} from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs/Rx';

import {GlobalService} from './global.service';
import {StaffService} from './staff.service';
import {AuthHttp} from 'angular2-jwt';
import {City} from './city';
import {Movie} from './movie';
import {Booking} from './booking';
import { State } from './state';

@Injectable()
export class BookingService {

    constructor(private _globalService:GlobalService,
                private _staffService:StaffService,
                private _authHttp: AuthHttp){
    }

    // POST /v1/user
    
    public getRewardCategory(params:any):Observable<any>{
        let headers = this.getHeaders();
        return this._authHttp.post(
            this._globalService.apiHost+'/user/getrewardcategory',
            JSON.stringify(params),
            {
                headers: headers
            }
        )
        .map(response => response.json())
        .map((response) => {
            return response;
        })
        .catch(this.handleError);
    }

    public getCities(params:any):Observable<any>{
        console.log(params);
        let headers = this.getHeaders();
        return this._authHttp.post(
            this._globalService.apiHost+'/user/getcitylist',
            JSON.stringify(params),
            {
                headers: headers
            }
        )
        .map(response => response.json())
        .map((response) => {
            return response;
        })
        .catch(this.handleError);
    }

    public getVenues(city:number):Observable<any>
    {
        let headers = this.getHeaders();
        return this._authHttp.post(
            this._globalService.apiHost+'/user/gettheatres',
            JSON.stringify({
                'city_id':city
            }),
            {
                headers: headers
            }
        )
        .map(response => response.json())
        .map((response) => {
            return response;
        })
        .catch(this.handleError);
    }

    public getAreas(city:City):Observable<any>
    {
        let headers = this.getHeaders();
        return this._authHttp.post(
            this._globalService.apiHost+'/user/getarealistbycity',
            JSON.stringify(city),
            {
                headers: headers
            }
        )
        .map(response => response.json())
        .map((response) => {
            return response;
        })
        .catch(this.handleError);
    }

    public getMovies(movie:Movie):Observable<any>
    {        
        let headers = this.getHeaders();
        return this._authHttp.post(
            this._globalService.apiHost+'/user/getmovielistbyarea',
            JSON.stringify(movie),
            {
                headers: headers
            }
        )
        .map(response => response.json())
        .map((response) => {
            return response;
        })
        .catch(this.handleError);
    }

    private getHeaders():Headers {
        return new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+this._staffService.getToken(),
        });
    }

    private handleError (error: Response | any) {

        let errorMessage:any = {};
        // Connection error
        if(error.status == 0) {
            errorMessage = {
                success: false,
                status: 0,
                data: "Sorry, there was a connection error occurred. Please try again.",
            };
        }
        else {
            errorMessage = error.json();
        }
        return Observable.throw(errorMessage);
    }

    public  addBooking(booking:any) {
        console.log(booking);
        let headers = this.getHeaders();
        return this._authHttp.post(
            this._globalService.apiHost+'/user/booking',
            JSON.stringify(booking),
            {
                headers: headers
            }
        )
        .map(response => response.json())
        .map((response) => {
            return response;
        })
        .catch(this.handleError);
    }

    public getStates()
    {
        let headers = this.getHeaders();
        return this._authHttp.get(
            this._globalService.apiHost+'/user/getstatelist',
            {
                headers: headers
            }
        )
        .map(response => response.json())
        .map((response) => {
            return response;
        })
        .catch(this.handleError);
    }

    public getCityList()
    {
        let headers = this.getHeaders();
        return this._authHttp.get(
            this._globalService.apiHost+'/user/getcitylist',
            {
                headers: headers
            }
        )
        .map(response => response.json())
        .map((response) => {
            return response;
        })
        .catch(this.handleError);
    }


    public getcitylistbystate(state:State,params:any)
    {
        let headers = this.getHeaders();
        return this._authHttp.post(
            this._globalService.apiHost+'/user/getcitylistbystate',
            JSON.stringify({state,params}),
            {
                headers: headers
            }
        )
        .map(response => response.json())
        .map((response) => {
            return response;
        })
        .catch(this.handleError);
    }

    public getsuccesspage(params:any)
    {
        let headers = this.getHeaders();
        return this._authHttp.post(
            this._globalService.apiHost+'/user/getsuccesspage',
            JSON.stringify({params}),
            {
                headers: headers
            }
        )
        .map(response => response.json())
        .map((response) => {
            return response;
        })
        .catch(this.handleError);
    }

    public getCustomerdetails(id: number) {
        let headers = this.getHeaders();
        let param = { 'id' : id };
        return this._authHttp.post(
            this._globalService.apiHost+'/user/bookingsummary',
            JSON.stringify(param),
            {
                headers: headers
            }
        )
        .map(response => response.json())
        .map((response) => {
            return response;
        })
        .catch(this.handleError);
    }

    public getPaymentDetails(params: any) {
        let headers = this.getHeaders();
        return this._authHttp.post(
            this._globalService.apiHost+'/user/payment',
            JSON.stringify(params),
            {
                headers: headers
            }
        )
        .map(response => response.json())
        .map((response) => {
            return response;
        })
        .catch(this.handleError);
    }

    // Get Movies list
    public getMovieList(){
        let headers = this.getHeaders();
        return this._authHttp.get(
            this._globalService.apiHost+'/user/getmovies',
            {
                headers: headers
            }
        )
        .map(response => response.json())
        .map((response) => {
            return response;
        })
        .catch(this.handleError);
    }
}
