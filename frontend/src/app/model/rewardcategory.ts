export class RewardCategory{
    id:number;
    reward_id:string;
    reward_name:string;

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}