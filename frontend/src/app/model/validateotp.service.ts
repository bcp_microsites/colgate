import {Injectable} from '@angular/core';
import {Http, Headers, Response} from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs/Rx';

import {GlobalService} from './global.service';
import {StaffService} from './staff.service';
import {AuthHttp} from 'angular2-jwt';
import { Otp } from "../model/otp";
import { RewardSelection } from "../model/rewardselection";

@Injectable()
export class ValidateotpService {

    private otp:number;

    constructor(private _globalService:GlobalService,
                private _staffService:StaffService,
                private _authHttp: AuthHttp){
    }

    // POST /v1/user

   
    private getHeaders():Headers {
        return new Headers({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+this._staffService.getToken(),
        });
    }

    private handleError (error: Response | any) {

        let errorMessage:any = {};
        // Connection error
        if(error.status == 0) {
            errorMessage = {
                success: false,
                status: 0,
                data: "Sorry, there was a connection error occurred. Please try again.",
            };
        }
        else {
            errorMessage = error.json();
        }
        return Observable.throw(errorMessage);
    }

    public validateOTP(otp:Otp):Observable<any>{
        let headers = this.getHeaders();
        return this._authHttp.post(
            this._globalService.apiHost+'/user/validateotp',
            JSON.stringify(otp),
            {
                headers: headers
            }
        )
            .map(response => response.json())
            .map((response) => {
                return response;
            })
            .catch(this.handleError);
    }

    
    public sendUniqueCode(rewardselection:RewardSelection):Observable<any>{
        let headers = this.getHeaders();
        return this._authHttp.post(
            this._globalService.apiHost+'/insms/getoffercode',
            JSON.stringify(rewardselection),
            {
                headers: headers
            }
        )
            .map(response => response.json())
            .map((response) => {
                return response;
            })
            .catch(this.handleError);
    }
    
    public resendOTP(otp:Otp):Observable<any>{
        let headers = this.getHeaders();
        return this._authHttp.post(
            this._globalService.apiHost+'/user/resendotp',
            JSON.stringify(otp),
            {
                headers: headers
            }
        )
            .map(response => response.json())
            .map((response) => {
                return response;
            })
            .catch(this.handleError);
    }
}
