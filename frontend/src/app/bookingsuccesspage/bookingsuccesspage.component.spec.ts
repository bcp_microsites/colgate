import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookingsuccesspageComponent } from './bookingsuccesspage.component';

describe('BookingsuccesspageComponent', () => {
  let component: BookingsuccesspageComponent;
  let fixture: ComponentFixture<BookingsuccesspageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookingsuccesspageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookingsuccesspageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
