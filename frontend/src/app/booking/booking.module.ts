// import { NgModule } from '@angular/core';
// import {CommonModule} from '@angular/common';
// import { BookingComponent } from './booking.component';
// import { BookingRoutingModule } from './booking-routing.module';
// import { BsDatepickerModule } from 'ngx-bootstrap';
// import { FormsModule, ReactiveFormsModule } from "@angular/forms"; //this to use ngModule
// import { RecaptchaModule } from 'angular-google-recaptcha';
// import{SummaryComponent} from './summary.component'


// @NgModule({
//   imports: [
//     CommonModule,
//     BookingRoutingModule,
//     BsDatepickerModule.forRoot(),
//     FormsModule,
//     ReactiveFormsModule,
//     RecaptchaModule.forRoot({
//       siteKey: '6LdYHwwUAAAAAAS6-F9wqwt_I0g6MnDpudcfJXRB',
//     }),
//   ],
//   declarations: [BookingComponent,SummaryComponent ]
// })
// export class BookingModule { 
    
// }

import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';
import { FormBuilder, FormGroup, FormArray,Validators , ReactiveFormsModule,FormsModule } from '@angular/forms';
import { BookingComponent } from './booking.component';
import { BookingRoutingModule} from './booking-routing.module';
import { RecaptchaModule } from 'angular-google-recaptcha';

@NgModule({
  imports: [
    CommonModule,
    BookingRoutingModule,
    FormsModule, ReactiveFormsModule,
    RecaptchaModule.forRoot({
      siteKey: '6LdYHwwUAAAAAAS6-F9wqwt_I0g6MnDpudcfJXRB',
  }),
  ],
  declarations: [ BookingComponent ]
})
export class RegistrationModule { }

