import { Component, OnInit ,Input} from '@angular/core';
import {FormGroup, FormBuilder, FormArray, Validators} from "@angular/forms";
import { BookingService } from "../model/booking.service";
import { State } from "../model/state";
import { City, Venue } from "../model/city";
import { StaffService } from "../model/staff.service";
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { Area } from "../model/area";
import { Movie } from "../model/movie";
import { Booking, ChildrenData } from "../model/booking";
import { RewardCategory } from "../model/rewardcategory";
import * as moment from "moment";
//import * as moment from "moment";
import { FormControl } from '@angular/forms';
import { empty } from 'rxjs';
@Component({
	selector: 'app-booking',
	templateUrl: './booking.component.html',
	styleUrls: ['./booking.component.scss']
})
export class BookingComponent implements OnInit {
	private bookingForm: FormGroup;
	private _city: City[];
	private _venue: Venue[];
	private _city_function: any;
	private _errorMessage: string;
	private _tab1Submitted: boolean = false;
	private _formErrors: any;
	private _area: Area;
	private _selectedCity: City;

	private _booking: Booking = new Booking;
	private _children: ChildrenData[];
	private params: any;
	private _bookingFormSubmitted:boolean = false;
	private _rewardCategory: RewardCategory;
	private _state: State;
	private _selectedState: State;
	private numbers: any;
	public itemRows: any[] = [];
	private _form4:FormGroup;
	private bookingDate:string;
	childrens: any;
	submitted = false;
	maxMDate: Date;
	minLDate: Date;
	minDate: Date;
	maxDate: Date;
	value: number;
	errorMsg: string;
	private childrenError:boolean = false;
	private venueError = '';
	myRecaptcha = new FormControl(false);
	@Input() model: Booking = new Booking;
	
	// myFilter = (d: Date): boolean => {
	// 	const day = d.getDay();
	// 	// Prevent Saturday and Sunday from being selected.
	// 	return day !== 0 && day !== 6;
	// }

	constructor(private formBuilder: FormBuilder, private _bookingService: BookingService, private _activatedRoute: ActivatedRoute, private _staffService: StaffService, private _router: Router) {
		this.bookingForm = this.formBuilder.group({
			city: ['', Validators.required],
			theatre: ['', Validators.required],
			timing: ['', Validators.required],
			seat: ['', Validators.required],
			prefereddate: ['', Validators.required],
			movie: ['', Validators.required],
			cost:['',Validators.required]
		});
	}


	ngOnInit() {
		
	this.city()
this.model.city=0
		
	}

	city() {
		this._bookingService.getCityList()
			.subscribe(
				result => {
					if (result.success) {
						this._city = result.data;
						this._errorMessage = '';
					} else {
						this._tab1Submitted = false;
					}
				},
				error => {
					this._tab1Submitted = false;
					// Validation errors

					if (error.status == 422) {
						this._errorMessage = error.data.message;
						let errorFields = JSON.parse(error.data.message);
						this._setFormErrors(errorFields);
					}
					// Unauthorized Access
					else if (error.status == 401 || error.status == 403) {
						this._staffService.unauthorizedAccess(error);
					}
					// All other errors
					else {
						this._errorMessage = error.data.message;
					}
				}
			);
	}


	OnCityChange(city_id) {
		this._bookingService.getVenues(city_id)
			.subscribe(
				result => {
					if (result.success) {
						this._venue = result.data;
						console.log(result.data)
						this._errorMessage = '';
					} else {
						this._tab1Submitted = false;
					}
				},
				error => {
					this._tab1Submitted = false;
					// Validation errors

					if (error.status == 422) {
						this._errorMessage = error.data.message;
						let errorFields = JSON.parse(error.data.message);
						this._setFormErrors(errorFields);
					}
					// Unauthorized Access
					else if (error.status == 401 || error.status == 403) {
						this._staffService.unauthorizedAccess(error);
					}
					// All other errors
					else {
						this._errorMessage = error.data.message;
					}
				}
			);
	}


	private _setFormErrors(errorFields: any): void {
		for (let key in errorFields) {
			let errorField = errorFields[key];
			// skip loop if the property is from prototype
			if (!this._formErrors.hasOwnProperty(key)) continue;

			// let message = errorFields[error.field];
			this._formErrors[key].valid = false;
			this._formErrors[key].message = errorField;
		}
	}

	// convenience getter for easy access to form fields
	get f() {
		return this.bookingForm.controls;
	}

	onBookingSubmit() {
		
		this.submitted = true;
		this._errorMessage = '';
		// stop here if form is invalid
		console.log(this.bookingForm);
		if (this.bookingForm.invalid) {
			console.log("hi")
			return;
		}

this.model.prefereddate	=moment(this.model.prefereddate).format("YYYY-MM-DD");
	
this.model.registration_id = localStorage.getItem('registration_id');
this.model.voucher_code = localStorage.getItem('voucher_code');

		this._bookingService.addBooking(this.model)
		
		.subscribe(
			
			result => {
			
				if (result.data) {
					console.log(result.data.final_total)
					localStorage.setItem('final_total',result.data.final_total);
					localStorage.setItem('grand_total',result.data.grand_total);
					localStorage.setItem('total_seats',result.data.total_seats);
					localStorage.setItem('booking_id', result.data.booking_id);
					this._router.navigate(['/summary']);
					this._errorMessage = '';
				}
				else if(result.data.verified == false){
					this.childrenError = true;
					return;
				} 
			},
			error => {
				this._tab1Submitted = false;
				// Validation errors
				if (error.status == 422) {
					this._errorMessage = error.data.message; 
					let errorFields = JSON.parse(error.data.message);
					this._setFormErrors(errorFields);
					return;
				}
				// Unauthorized Access
				else if (error.status == 401 || error.status == 403) {
					//this._staffService.unauthorizedAccess(error);
				}
				// All other errors
				else {
					this._errorMessage = JSON.parse(error.data.message);
				}
			}
		);

	}



}
