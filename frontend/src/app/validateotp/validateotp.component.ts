import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { FormControl } from '@angular/forms';
import { ValidateotpService } from "../model/validateotp.service";
import { PlatformLocation } from '@angular/common';
import { Registration } from "../model/registration";
import { Otp } from "../model/otp";
import { ResendCode } from "../model/registration";
import { RegistrationService } from "../model/registration.service";
import { CustomValidators } from 'ng2-validation';
import { TabsetComponent, TabDirective } from 'ngx-bootstrap/tabs';
import { Campaign } from '../model/campaign';
@Component({
  selector: 'app-validateotp',
  templateUrl: './validateotp.component.html',
  styleUrls: ['./validateotp.component.scss']
})
export class ValidateotpComponent implements OnInit {
  private resend:boolean = false;
  private otpForm: FormGroup;
  private registerForm1: FormGroup;
  private resendotpForm: FormGroup;
  submitted = false;
  private otparray = [];
  private _errorMessage: string;
  private _errorMessage1: string;
  private _errorMessage2: string;
  private _tab1Submitted: boolean = false;
  private _formErrors: any;
  private rewardType: string;
  private registration_id: number;
  private _registration: Registration;
  private params: any;
  private _campaigns: Campaign;
  private errorMsg: any;
  submitted2 = false;
  childrens : any;
  private otpresponce: any;
  emailPattern = "[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}";
  submitted1: boolean = false;
  @Input() model: Otp = new Otp;
  @Input() model1: ResendCode = new ResendCode;

  constructor(private formBuilder: FormBuilder, private _router: Router, private _activatedRoute: ActivatedRoute, private _validateotpService: ValidateotpService, location: PlatformLocation, private _registrationDataService: RegistrationService) {
    this.otpForm = this.formBuilder.group({
      otp: ['', [Validators.required, Validators.minLength(4)]]
    });

    this.registerForm1 = this.formBuilder.group({
      voucher_code: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.pattern(this.emailPattern)]],
      mobile: ['', [Validators.required,
      Validators.minLength(10),
      Validators.maxLength(10),
      CustomValidators.number,]]
    });

    // location.onPopState(() => {
    // 	this._router.navigate(['/validateotp']);
    // 	});

  }

  ngOnInit() {

  }

  // convenience getter for easy access to form fields
  get f() { return this.otpForm.controls; }

  get f1() {
    return this.registerForm1.controls;
  }

  onOTPSubmit() {

    this.submitted = true;
    this.submitted1 = false;
    this.resend = false;
    // stop here if form is invalid
    if (this.otpForm.invalid) {
      return;
    }
 
    this._validateotpService.validateOTP(this.model)
      .subscribe(
        result => {
          if (result.success) {
            if (result.data.verified == true) {
              localStorage.setItem('regd_id',result.data.access_token);
              localStorage.setItem('voucher_code',result.data.voucher_code);
             
              this._router.navigate(['/booking']);
            }
            else if (result.data.verified == false) {
             this._errorMessage=result.data.message
            }
          
          } 
        },
        error => {
          this._tab1Submitted = false;
          // Validation errors

          if (error.status == 422) {
            this._errorMessage = error.data.message;
            let errorFields = JSON.parse(error.data.message);
            this._setFormErrors(errorFields);
          }
          // Unauthorized Access
          else if (error.status == 401 || error.status == 403) {
            //this._staffService.unauthorizedAccess(error);
          }
          // All other errors
          else {
            this._errorMessage = error.data.message;
          }
        }
      );
  }


	onSearchChange(searchValue : string ) {  
		this.resend = false;
	}

	



  resendOTP() {
    this.submitted = false;
    this.submitted1 = true;
    this.resend = true;

    // stop here if form is invalid

    
    this._validateotpService.resendOTP(this.model)
      .subscribe(
        result => {
          if (result.success) {
            this._errorMessage = '';
          } else {
            this._tab1Submitted = false;
          }
        },
        error => {
          this._tab1Submitted = false;
          // Validation errors
          this.submitted1 = false;
          if (error.status == 422) {
            this._errorMessage2 = error.data.message;
            let errorFields = JSON.parse(error.data.message);
            this._setFormErrors(errorFields);
          }
          // Unauthorized Access
          else if (error.status == 401 || error.status == 403) {
            //this._staffService.unauthorizedAccess(error);
          }
          // All other errors
          else {
            this._errorMessage2 = error.data.message;
          }
        }
      );

  }

  private _setFormErrors(errorFields: any): void {
    for (let key in errorFields) {
      let errorField = errorFields[key];
      // skip loop if the property is from prototype
      if (!this._formErrors.hasOwnProperty(key)) continue;

      // let message = errorFields[error.field];
      this._formErrors[key].valid = false;
      this._formErrors[key].message = errorField;
    }
  }

  onResumbit() {
    this.submitted2 = true;
    console.log(this.registerForm1);

    // stop here if form is invalid
    if (this.registerForm1.invalid) {
      return;
    }

    this._registrationDataService.resendOfferCode(this.registerForm1.value)
      .subscribe(
        result => {
          if (result.success) {
            localStorage.setItem('mobile', result.data.mobile);
            localStorage.setItem('customer_id', result.data.customer_id);
            this._router.navigate(['/booking']);
            this._errorMessage = '';
          } else {
            this._tab1Submitted = false;
          }
        },
        error => {
          this._tab1Submitted = false;
          // Validation errors

          if (error.status == 422) {
            this._errorMessage1 = error.data.message;
            let errorFields = (error.data.message);
            this._setFormErrors(errorFields);
          }
          // Unauthorized Access
          else if (error.status == 401 || error.status == 403) {
            //this._staffService.unauthorizedAccess(error);
          }
          // All other errors
          else {
            this._errorMessage1 = JSON.parse(error.data.message);
          }
        }
      );


  }

  public getCampaignData() {
    this._registrationDataService.getCampaignData()
      .subscribe(
        result => {
          if (result.data) {
            this._campaigns = result.data;
            this._errorMessage = '';
          } else {
            this._tab1Submitted = false;
          }
        },
        error => {
          this._tab1Submitted = false;
          // Validation errors

          if (error.status == 422) {
            this._errorMessage = error.data.message;

            if (this._errorMessage == "This voucher no. has been registered already. To receive the reward code for this voucher again, Click Here.") {
              this.errorMsg = true;

            }
            let errorFields = JSON.parse(error.data.message);

            this._setFormErrors(errorFields);
          }
          // Unauthorized Access
          else if (error.status == 401 || error.status == 403) {
            //this._staffService.unauthorizedAccess(error);
          }
          // All other errors
          else {
            this._errorMessage = JSON.parse(error.data.message);
          }
        }
      );
  }

}
