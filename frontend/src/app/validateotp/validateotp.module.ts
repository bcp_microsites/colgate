import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';
import { ValidateotpComponent } from './validateotp.component';
import { ValidateotpRoutingModule } from './validateotp-routing.module';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { FormsModule, ReactiveFormsModule } from "@angular/forms"; //this to use ngModule
import { RecaptchaModule } from 'angular-google-recaptcha';

@NgModule({
  imports: [
    CommonModule,
    ValidateotpRoutingModule,
    BsDatepickerModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    RecaptchaModule.forRoot({
      siteKey: '6Lfpro4UAAAAAEoAEaErUzK-3cfU0Ty55edTeXYf',
    }),
  ],
  declarations: [ ValidateotpComponent ]
})
export class ValidateotpModule { 
    
}
