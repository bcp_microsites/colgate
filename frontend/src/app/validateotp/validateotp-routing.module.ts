import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ValidateotpComponent } from './validateotp.component';

const routes: Routes = [
{
  path: '',
  component: ValidateotpComponent,
  data: {
    title: 'ValidateOTP'
  }
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ValidateotpRoutingModule {}
