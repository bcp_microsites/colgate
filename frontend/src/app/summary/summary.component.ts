import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import {Routes, Router} from '@angular/router';
import { Summary } from "../model/otp";
import { BookingService } from "../model/booking.service";

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent implements OnInit {
  private model: Summary = new Summary;
  public payuform: any = {};
  _errorMessage: any;
  constructor(private _router:Router, private _bookingService: BookingService) { }

  ngOnInit() {
    console.log(localStorage);
      this.customerDetails();
      this.model.final_total = localStorage.getItem('final_total');
      this.model.grand_total = localStorage.getItem('grand_total');
      this.model.total_seats = localStorage.getItem('total_seats');
      this.payuform.service_provider = 'payu_paisa';
      this.payuform.amount = localStorage.getItem('final_total');
      this.payuform.productinfo = 'Booking';
      this.payuform.udf1 = localStorage.getItem('booking_id');
  }

  customerDetails() {
    let regId = +localStorage.getItem('registration_id');
    this._bookingService.getCustomerdetails(regId)
    .subscribe(
      result => {
        if (result.data){
          this.payuform.firstname = result.data.firstname;
          this.payuform.email = result.data.email;
          this.payuform.phone = result.data.phone;
          this.paymentDetails();
        }
      },
      error => {
        // Validation errors
					if (error.status == 422) {
						this._errorMessage = error.data.message;
					
						let errorFields = JSON.parse(error.data.message);

						this._setFormErrors(errorFields);
					}
					// Unauthorized Access
					else if (error.status == 401 || error.status == 403) {
						//this._staffService.unauthorizedAccess(error);
					}
					// All other errors
					else {
						this._errorMessage = JSON.parse(error.data.message);
					}
      }
    )
  }

  paymentDetails() {
    const paymentPayload = {
      email: this.payuform.email,
      firstname: this.payuform.firstname,
      phone: this.payuform.phone,
      productinfo: this.payuform.productinfo,
      amount: this.payuform.amount,
      service_provider: this.payuform.service_provider,
      udf1: this.payuform.udf1
    }
    this._bookingService.getPaymentDetails(paymentPayload)
    .subscribe(
      result => {
        if (result.data) {
          this.payuform.txnid = result.data.txnid;
          this.payuform.surl = result.data.surl;
          this.payuform.furl = result.data.furl;
          this.payuform.key = result.data.key;
          this.payuform.hash = result.data.hash;
          this.payuform.salt = result.data.salt;
        }
      },
      error => {
        // Validation errors
					if (error.status == 422) {
						this._errorMessage = error.data.message;
					
						let errorFields = JSON.parse(error.data.message);

						this._setFormErrors(errorFields);
					}
					// Unauthorized Access
					else if (error.status == 401 || error.status == 403) {
						//this._staffService.unauthorizedAccess(error);
					}
					// All other errors
					else {
						this._errorMessage = JSON.parse(error.data.message);
					}
      }
    )
  }

  _setFormErrors(errorFields: any) {
    throw new Error("Method not implemented.");
  }
}
