import { Component, OnInit } from '@angular/core';
import {BookingService} from "../model/booking.service";
import {Router,NavigationExtras,ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-instantsuccesspage',
  templateUrl: './instantsuccesspage.component.html',
  styleUrls: ['./instantsuccesspage.component.scss']
})
export class InstantsuccesspageComponent implements OnInit {

  constructor(private _bookingService:BookingService,private _activatedRoute:ActivatedRoute) { }
  private _errorMessage:string;
  private _tab1Submitted:boolean = false;
  private _formErrors:any;
  private params:any;
  private success_msg:string;

  ngOnInit() {
    this._activatedRoute.queryParams.subscribe(params => {
      this.params = params;
		});
    this.getSuccessPage(this.params);
  }

  getSuccessPage(params) {
    this._bookingService.getsuccesspage(params)
		.subscribe(
			result => {
				if(result.success) {
					this.success_msg = result.data['message_content'];				
					this._errorMessage = '';					
				} else {
					this._tab1Submitted = false;				
				}
			},
			error => {
				this._tab1Submitted = false;
				// Validation errors
			
				if(error.status == 422) {
					this._errorMessage = error.data.message;
					let errorFields = JSON.parse(error.data.message);
					this._setFormErrors(errorFields);
				}
				// Unauthorized Access
				else if(error.status == 401 || error.status == 403) {
					//this._staffService.unauthorizedAccess(error);
				}
				// All other errors
				else {
					this._errorMessage = error.data.message;
				}
			}
		);
  }

  private _setFormErrors(errorFields:any):void{
    for (let key in errorFields) {
        let errorField = errorFields[key];
        // skip loop if the property is from prototype
        if (!this._formErrors.hasOwnProperty(key)) continue;

        // let message = errorFields[error.field];
        this._formErrors[key].valid = false;
        this._formErrors[key].message = errorField;
    }
}
}
