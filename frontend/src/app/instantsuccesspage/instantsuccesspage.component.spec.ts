import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstantsuccesspageComponent } from './instantsuccesspage.component';

describe('InstantsuccesspageComponent', () => {
  let component: InstantsuccesspageComponent;
  let fixture: ComponentFixture<InstantsuccesspageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstantsuccesspageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstantsuccesspageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
