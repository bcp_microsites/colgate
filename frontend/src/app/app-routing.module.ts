import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {RegistrationComponent} from './registration/registration.component';
import {BookingComponent} from './booking/booking.component';
import {ContactusComponent} from './contactus/contactus.component';
import {SuccesspageComponent} from './successpage/successpage.component';
import {BookingsuccesspageComponent} from './bookingsuccesspage/bookingsuccesspage.component';
import {TermsComponent} from './terms/terms.component';
import {InstantsuccesspageComponent} from './instantsuccesspage/instantsuccesspage.component';
import {ValidateotpComponent} from './validateotp/validateotp.component';
import { RewardselectionComponent } from "./rewardselection/rewardselection.component";
import { AuthGuard } from './model/auth.guard';
import { SummaryComponent } from './summary/summary.component';
import { FailurepageComponent } from './failurepage/failurepage.component';
import { RedeemComponent } from './redeem/redeem.component';

export const routes: Routes = [
    {
        path: '',
        redirectTo: 'registration',
        pathMatch: 'full',
    },
   
    {
        path: 'booking',
        redirectTo: 'booking',
        pathMatch: 'full',
    },
    { path: 'registration', component: RegistrationComponent },
    { path: 'booking', component: BookingComponent },
    {
        path: 'terms',
        component: TermsComponent,
        data: {
            title: 'Terms'
        },
    },
    {
        path: 'contactus',
        component: ContactusComponent,
        data: {
            title: 'Contact Us'
        },
    },
    {
        path: 'successpage',
        component: SuccesspageComponent,
        data: {
            title: 'Success'
        },
        
    },

    {
        path: 'summary',
        component: SummaryComponent,
        pathMatch: 'full',
    },
    {
        path: 'bookingsuccesspage',
        component: BookingsuccesspageComponent,
        data: {
            title: 'Success'
        },
    },
    {
        path: 'terms',
        component: TermsComponent,
        data: {
            title: 'terms'
        },
    },
    {
        path: 'instantsuccesspage',
        component: InstantsuccesspageComponent,
        data: {
            title: 'Success'
        },
    },
    {
        path: 'validateotp',
        component: ValidateotpComponent,
        data: {
            title: 'Success'
        },
    },
    {
        path: 'rewardselection',
        component: RewardselectionComponent,
        data: {
            title: 'Reward Selection'
        },
        
    },
    {
        path: 'failurepage',
        component: FailurepageComponent,
        data: {
            title: 'Failed'
        }
    },
    {
        path: 'redeem',
        component: RedeemComponent,
        data: {
            title: 'How to Redeem'
        },
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
