import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FailurepageComponent } from './failurepage.component';

describe('FailurepageComponent', () => {
  let component: FailurepageComponent;
  let fixture: ComponentFixture<FailurepageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FailurepageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FailurepageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
