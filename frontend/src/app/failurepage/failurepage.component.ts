import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-failurepage',
  templateUrl: './failurepage.component.html',
  styleUrls: ['./failurepage.component.scss']
})
export class FailurepageComponent implements OnInit {

  constructor(private _router:Router,private route: ActivatedRoute) { }

  ngOnInit() {
  }

  goHome(){
    this._router.navigate(['/registration']);
    // this._router.navigate(['/terms']);
  }

}
