import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from "@angular/router";
import { Observable }         from 'rxjs';
//import { map }                from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { PlatformLocation } from '@angular/common';
import { RegistrationService } from "../model/registration.service";
import { Campaign } from '../model/campaign';

@Component({
  selector: 'app-successpage',
  templateUrl: './successpage.component.html',
  styleUrls: ['./successpage.component.scss']
})
export class SuccesspageComponent implements OnInit {
  private _errorMessage: string;
  private _tab1Submitted: boolean = false;
  private _campaigns: Campaign;
  token: Observable<string>;
  myRecaptcha = new FormControl(false);
  
  constructor(private _router:Router,private route: ActivatedRoute,location: PlatformLocation,private _registrationDataService: RegistrationService) {
    location.onPopState(() => {
			this._router.navigate(['/registration']);
			});
   }

  ngOnInit() {
    // this.token = this.route
    // .fragment
    // .pipe(map(fragment => fragment || 'None'));
    //localStorage.clear();
    console.log(this.route.fragment);
    
  }

  goHome(){
    this._router.navigate(['/registration']);
    // this._router.navigate(['/terms']);
  }

  public getCampaignData() {
    this._registrationDataService.getCampaignData()
      .subscribe(
        result => {
          if (result.data) {
            this._campaigns = result.data;
            this._errorMessage = '';
          } else {
            this._tab1Submitted = false;
          }
        },
        error => {
          this._tab1Submitted = false;
          // Validation errors

          if (error.status == 422) {
            this._errorMessage = error.data.message;

            let errorFields = JSON.parse(error.data.message);
           
          }
          // Unauthorized Access
          else if (error.status == 401 || error.status == 403) {
            //this._staffService.unauthorizedAccess(error);
          }
          // All other errors
          else {
            this._errorMessage = JSON.parse(error.data.message);
          }
        }
      );
  }

}
