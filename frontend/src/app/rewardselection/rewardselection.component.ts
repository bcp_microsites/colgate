import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router, NavigationExtras, ActivatedRoute } from "@angular/router";
import { FormControl } from '@angular/forms';
import { ValidateotpService } from "../model/validateotp.service";
import { RewardSelection } from "../model/rewardselection";
import { ResendCode } from "../model/registration";
import { CustomValidators } from 'ng2-validation';
import { RegistrationService } from "../model/registration.service";
import { TabsetComponent, TabDirective } from 'ngx-bootstrap/tabs';



@Component({
  selector: 'app-rewardselection',
  templateUrl: './rewardselection.component.html',
  styleUrls: ['./rewardselection.component.scss']
})
export class RewardselectionComponent implements OnInit {

  private rewardForm: FormGroup;
  private resendotpForm: FormGroup;
  private registerForm1: FormGroup;
  private _errorMessage1: string;
  submitted1 = false;
  submitted = false;
  private _errorMessage: string;
  private _tab1Submitted: boolean = false;
  private _formErrors: any;
  private rewardType: string;
  @Input()model: RewardSelection = new RewardSelection;
  @Input() model1: ResendCode  = new ResendCode;
  emailPattern = "[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}";

  constructor(private formBuilder: FormBuilder, private _router: Router,  private _registrationDataService: RegistrationService,private _activatedRoute: ActivatedRoute, private _validateotpService: ValidateotpService) {
    this.rewardForm = this.formBuilder.group({
      rewardType: ['', [Validators.required]]
    });

    this.registerForm1 = this.formBuilder.group({
			voucher_code: ['', [Validators.required]],
			email: ['', [Validators.required, Validators.pattern(this.emailPattern)]],
			mobile: ['', [Validators.required,
			Validators.minLength(10),
			Validators.maxLength(10),
			CustomValidators.number,]]
		});

  }

  ngOnInit() {

  }

  // convenience getter for easy access to form fields
  get f() { return this.rewardForm.controls; }

  get f1() { return this.registerForm1.controls; }

  onSubmit() {
    
    this.submitted = true;

    // stop here if form is invalid
    if (this.rewardForm.invalid) {
      return;
    }
    this.model.unique_code = localStorage.getItem('unique_code');
    
    this._validateotpService.sendUniqueCode(this.model)
    .subscribe(
      result => {
        if (result.success) {
          localStorage.clear();
          this._router.navigate(['/successpage']);
          this._errorMessage = '';
        } else {
          this._tab1Submitted = false;
        }
      },
      error => {
        this._tab1Submitted = false;
        // Validation errors

        if (error.status == 422) {
          this._errorMessage = error.data.message;
          let errorFields = JSON.parse(error.data.message);
          this._setFormErrors(errorFields);
        }
        // Unauthorized Access
        else if (error.status == 401 || error.status == 403) {
          //this._staffService.unauthorizedAccess(error);
        }
        // All other errors
        else {
          this._errorMessage = error.data.message;
        }
      }
    );
  }

  private _setFormErrors(errorFields: any): void {
    for (let key in errorFields) {
      let errorField = errorFields[key];
      // skip loop if the property is from prototype
      if (!this._formErrors.hasOwnProperty(key)) continue;

      // let message = errorFields[error.field];
      this._formErrors[key].valid = false;
      this._formErrors[key].message = errorField;
    }
  }

  onResumbit(){
		this.submitted1 = true;

		// stop here if form is invalid
		if (this.registerForm1.invalid) {
			return;
		}

	
		
		this._registrationDataService.resendOfferCode(this.registerForm1.value)
			.subscribe(
				result => {
					if (result.success) {
						localStorage.setItem('mobile', result.data.mobile);
						localStorage.setItem('customer_id', result.data.customer_id);
						this._router.navigate(['/validateotp']);
						this._errorMessage = '';
					} else {
						this._tab1Submitted = false;
					}
				},
				error => {
					this._tab1Submitted = false;
					// Validation errors

					if (error.status == 422) {
						this._errorMessage1 = error.data.message;
						let errorFields = JSON.parse(error.data.message);
						this._setFormErrors(errorFields);
					}
					// Unauthorized Access
					else if (error.status == 401 || error.status == 403) {
						//this._staffService.unauthorizedAccess(error);
					}
					// All other errors
					else {
						this._errorMessage1 = JSON.parse(error.data.message);
					}
				}
			);


	}

}
