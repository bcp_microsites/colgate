import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RewardselectionComponent } from './rewardselection.component';

describe('RewardselectionComponent', () => {
  let component: RewardselectionComponent;
  let fixture: ComponentFixture<RewardselectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RewardselectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RewardselectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
