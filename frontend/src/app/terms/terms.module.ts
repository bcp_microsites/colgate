import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import { TermsComponent } from './terms.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { FormsModule, ReactiveFormsModule } from "@angular/forms"; //this to use ngModule
import { RecaptchaModule } from 'angular-google-recaptcha';
import { TermsRoutingModule } from './terms-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    BsDatepickerModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    TermsRoutingModule
  ],
  declarations: [ TermsComponent ]
})
export class TermsModule { 
    
}
