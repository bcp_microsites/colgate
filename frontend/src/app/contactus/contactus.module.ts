import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import { ContactusComponent } from './contactus.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { FormsModule, ReactiveFormsModule } from "@angular/forms"; //this to use ngModule
import { RecaptchaModule } from 'angular-google-recaptcha';
import { ContactusRoutingModule } from './contactus-routing.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    BsDatepickerModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    ContactusRoutingModule
  ],
  declarations: [ ContactusComponent ]
})
export class ContactusModule { 
    
}
