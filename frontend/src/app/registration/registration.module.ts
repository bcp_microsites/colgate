import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';
import { FormBuilder, FormGroup, FormArray,Validators , ReactiveFormsModule,FormsModule } from '@angular/forms';
import { RegistrationComponent } from './registration.component';
import { RegistrationRoutingModule } from './registration-routing.module';
import { RecaptchaModule } from 'angular-google-recaptcha';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';


@NgModule({
  imports: [
    CommonModule,
    RegistrationRoutingModule,
    FormsModule, ReactiveFormsModule,
    RecaptchaModule.forRoot({
      siteKey: '6LdYHwwUAAAAAAS6-F9wqwt_I0g6MnDpudcfJXRB',
  }),
  TimepickerModule.forRoot()
  ],
  declarations: [ RegistrationComponent ]
})
export class RegistrationModule { }
