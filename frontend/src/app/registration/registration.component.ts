import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators, ReactiveFormsModule } from '@angular/forms';
import { isNumber } from 'util';
import { CustomValidators } from 'ng2-validation';
import { RegistrationService } from "../model/registration.service";
import { Registration } from "../model/registration";
import { ResendCode } from "../model/registration";
import { Otp, Summary } from "../model/otp";
import { Router, NavigationExtras } from "@angular/router";
import { FormControl } from '@angular/forms';
import { PlatformLocation } from '@angular/common';
import { TabsetComponent, TabDirective } from 'ngx-bootstrap/tabs';
import { Campaign } from '../model/campaign';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { BookingService } from "../model/booking.service";
import { StaffService } from "../model/staff.service";
import { City, Venue, Movie } from "../model/city";
import { ValidateotpService } from '../model/validateotp.service';
import { Booking, ChildrenData } from '../model/booking';
import * as moment from "moment";


@Component({
	selector: 'app-registration',
	templateUrl: './registration.component.html',
	styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
	
	@ViewChild('first') first: TabDirective;
	@ViewChild('second') second: TabDirective;
	@ViewChild('tabset') tabset: TabsetComponent;
	private registerForm: FormGroup;
	private otpForm: FormGroup;
	private bookingForm: FormGroup;
	private _voucherErrorMsg:string;
	private _errorMessage1: string;
	private _errorMessage2: string;
	private _tab1Submitted: boolean = false;
	private _formErrors: any;
	private _city: City[];
	private _venue: Venue[];
	private _movie: Movie[];
	private _campaigns : Campaign; 
	private uploadMsg: string;
	private _successMessage: string;
	submitted = false;
	submitted1 = false;
	submitted2 = false;
	example: string;
	errorMsg: string;
	myRecaptcha = new FormControl(false);
	emailPattern = "[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}";
	amountPattern = "[0-9]+([.][0-9]+)?";
	minDate: Date;
	maxDate: Date;
	@Input() model: Registration = new Registration;
	@Input() model1: Otp = new Otp;
	@Input() model2: Booking = new Booking;
	private _children: ChildrenData[];
	resend: boolean;
	private childrenError:boolean = false;
	private model3: Summary = new Summary;
	public payuform: any = {};
	

	otpVerified = 0;
	mytime: Date = new Date();


	constructor(private formBuilder: FormBuilder, private _validateotpService: ValidateotpService, private _registrationDataService: RegistrationService, private _router: Router, location: PlatformLocation,private _bookingService: BookingService,private _staffService: StaffService) {
		this.registerForm = this.formBuilder.group({
			voucher_code: ['', [Validators.required]],
			customer_name: ['', [Validators.required]],

			email: ['', [Validators.required, Validators.pattern(this.emailPattern)]],
			mobile: ['', [Validators.required,
			Validators.minLength(10),
			Validators.maxLength(10),
			CustomValidators.number,]],
			agreeTerms: ['', Validators.requiredTrue],
		});


		this.otpForm = this.formBuilder.group({
			otp: ['', [Validators.required, Validators.minLength(4)]]
		});

		this.bookingForm = this.formBuilder.group({
			city: ['', Validators.required],
			theatre: ['', Validators.required],
			timing: ['', Validators.required],
			seat: ['', Validators.required],
			prefereddate: ['', Validators.required],
			movie: ['', Validators.required],
			cost:['', [Validators.required, Validators.pattern(this.amountPattern)]],
			// timing: 
		});
		
		// location.onPopState(() => {
		// 	console.log('pressed back in add!!!!!');
		// 	this._router.navigate(['/registration']);
		// 	});
		this.minDate = new Date();
		this.maxDate = new Date();
		this.minDate.setDate(this.minDate.getDate() + 2);
		this.maxDate.setDate(this.maxDate.getDate() + 7);
	}

	ngOnInit() {
		this._successMessage = '';
		this._errorMessage1 = this._errorMessage2 = '';
		this.city();
		this.movie();
		// this.model2.city = 0
	}

	city() {
		this._bookingService.getCityList()
		.subscribe(
			result => {
				if (result.success) {
					this._city = result.data;
					this._errorMessage2 = '';
				} else {
					this._tab1Submitted = false;
				}
			},
			error => {
				this._tab1Submitted = false;
				// Validation errors

				if (error.status == 422) {
					this._errorMessage2 = error.data.message;
					let errorFields = JSON.parse(error.data.message);
					this._setFormErrors(errorFields);
				}
				// Unauthorized Access
				else if (error.status == 401 || error.status == 403) {
					this._staffService.unauthorizedAccess(error);
				}
				// All other errors
				else {
					this._errorMessage2 = error.data.message;
				}
			}
		);
	}

	movie() {
		this._bookingService.getMovieList()
		.subscribe(
			result => {
				if (result.success) {
					this._movie = result.data;
					this._errorMessage2 = '';
				} else {
					this._tab1Submitted = false;
				}
			},
			error => {
				this._tab1Submitted = false;
				// Validation errors

				if (error.status == 422) {
					this._errorMessage2 = error.data.message;
					let errorFields = JSON.parse(error.data.message);
					this._setFormErrors(errorFields);
				}
				// Unauthorized Access
				else if (error.status == 401 || error.status == 403) {
					this._staffService.unauthorizedAccess(error);
				}
				// All other errors
				else {
					this._errorMessage2 = error.data.message;
				}
			}
		);
	}

	OnCityChange(city_id) {
		this._bookingService.getVenues(city_id)
		.subscribe(
			result => {
				if (result.success) {
					this._venue = result.data;
					console.log(result.data)
					this._errorMessage2 = '';
				} else {
					this._tab1Submitted = false;
				}
			},
			error => {
				this._tab1Submitted = false;
				// Validation errors

				if (error.status == 422) {
					this._errorMessage2 = error.data.message;
					let errorFields = JSON.parse(error.data.message);
					this._setFormErrors(errorFields);
				}
				// Unauthorized Access
				else if (error.status == 401 || error.status == 403) {
					this._staffService.unauthorizedAccess(error);
				}
				// All other errors
				else {
					this._errorMessage2 = error.data.message;
				}
			}
		);
	}

	onSearchChange(searchValue : string ) {  
		this._voucherErrorMsg = '';
	}

	// convenience getter for easy access to form fields
	get f() {
		return this.registerForm.controls;
	}

	get f1() {
		return this.otpForm.controls;
	}

	get f2() {
		return this.bookingForm.controls;
	}

	onSubmit() {

		this.submitted = true;
		this.submitted1 = false;
		this.submitted2 = false;
		this.errorMsg = '';
		this._errorMessage1 = '';
		// stop here if form is invalid
		if (this.registerForm.invalid) {
			return;
		}
		console.log(this.model);
		this._registrationDataService.addRegistration(this.model)
			.subscribe(
				result => {
					if (result.data ) {
					
						localStorage.setItem('mobile', result.data.mobile);
						localStorage.setItem('customer_id', result.data.customer_id);
						localStorage.setItem('otp',result.data.otp);
						localStorage.setItem('registration_id',result.data.registration_id);
						localStorage.setItem('email',result.data.email);
						localStorage.setItem('voucher_code', result.data.voucher_code);
						localStorage.setItem('campaign_id',result.data.email);
						//this._router.navigate(['/validateotp']);
						this.tabset.tabs[1].active = true;
						this.tabset.tabs[0].active = false;
						this.registerForm.reset();
						this._errorMessage1 = '';
						// this._successMessage = 'You are successfully registered. Please validate the OTP in the Booking Tab';
						this.submitted = false;
					} 
					
				},
				error => {				
					// Validation errors
					if (error.status == 422) {
						this._errorMessage1 = error.data.message;
					
						let errorFields = JSON.parse(error.data.message);

						this._setFormErrors(errorFields);
					}
					// Unauthorized Access
					else if (error.status == 401 || error.status == 403) {
						//this._staffService.unauthorizedAccess(error);
					}
					// All other errors
					else {
						this._errorMessage1 = JSON.parse(error.data.message);
					}
				}
			);
	}

	public getCampaignData() {
		this._registrationDataService.getCampaignData()
			.subscribe(
				result => {
					if (result.data) {
						this._campaigns = result.data;
						this._errorMessage1 = '';
					} else {
						this._tab1Submitted = false;
					}
				},
				error => {
					this._tab1Submitted = false;
					// Validation errors

					if (error.status == 422) {
						this._errorMessage1 = error.data.message;
					
						let errorFields = JSON.parse(error.data.message);

						this._setFormErrors(errorFields);
					}
					// Unauthorized Access
					else if (error.status == 401 || error.status == 403) {
						//this._staffService.unauthorizedAccess(error);
					}
					// All other errors
					else {
						this._errorMessage1 = JSON.parse(error.data.message);
					}
				}
			);
	}

	private _setFormErrors(errorFields: any): void {
		for (let key in errorFields) {
			let errorField = errorFields[key];
			// skip loop if the property is from prototype
			if (!this._formErrors.hasOwnProperty(key)) continue;

			// let message = errorFields[error.field];
			this._formErrors[key].valid = false;
			this._formErrors[key].message = errorField;
		}
	}


	onOTPSubmit() {

		this.submitted = false;
		this.submitted1 = true;
		this.submitted2 = false;
		this.resend = false;
		// stop here if form is invalid
		if (this.otpForm.invalid) {
		  return;
		}
	 
		this._validateotpService.validateOTP(this.model1)
		  .subscribe(
			result => {
			  if (result.success) {
				if (result.data.verified == true) {
				  	localStorage.setItem('regd_id',result.data.access_token);
				  	localStorage.setItem('voucher_code',result.data.voucher_code);
				  	localStorage.setItem('mobile', result.data.mobile);
					localStorage.setItem('customer_id', result.data.customer_id);
					localStorage.setItem('registration_id',result.data.registration_id);
					localStorage.setItem('email',result.data.email);
					localStorage.setItem('campaign_id',result.data.campaign_id);
				//   this._router.navigate(['/booking']);
					this.otpVerified = 1;
					this._errorMessage2 = '';
					this.otpForm.reset();
				}
				else if (result.data.verified == false) {
				 	this._errorMessage2 = result.data.message
				}
			  
			  } 
			},
			error => {
			  this._tab1Submitted = false;
			  // Validation errors
	
			  if (error.status == 422) {
				this._errorMessage2 = error.data.message;
				// let errorFields = JSON.parse(error.data.message);
				// this._setFormErrors(errorFields);
			  }
			  // Unauthorized Access
			  else if (error.status == 401 || error.status == 403) {
				//this._staffService.unauthorizedAccess(error);
			  }
			  // All other errors
			  else {
				this._errorMessage2 = error.data.message;
			  }
			}
		  );
	  }

	  resendOTP() {
		this.submitted = false;
		this.submitted1 = true;
		this.submitted2 = false;
		this.resend = true;
	
		// stop here if form is invalid
		
		this._validateotpService.resendOTP(this.model1)
		  .subscribe(
			result => {
			  if (result.success) {
				this._errorMessage2 = '';
			  } else {
				this._tab1Submitted = false;
			  }
			},
			error => {
			  this._tab1Submitted = false;
			  // Validation errors
			  this.submitted1 = false;
			  if (error.status == 422) {
				this._errorMessage2 = error.data.message;
				// let errorFields = JSON.parse(error.data.message);
				// this._setFormErrors(errorFields);
			  }
			  // Unauthorized Access
			  else if (error.status == 401 || error.status == 403) {
				//this._staffService.unauthorizedAccess(error);
			  }
			  // All other errors
			  else {
				this._errorMessage2 = error.data.message;
			  }
			}
		  );
	
	  }

	  onBookingSubmit() {
		
		this.submitted = false;
		this.submitted1 = false;
		this.submitted2 = true;
		this._errorMessage2 = '';
		// stop here if form is invalid
		console.log(this.bookingForm);
		if (this.bookingForm.invalid) {
			console.log("hi")
			return;
		}
		
		if (this.payuform.udf1 != undefined) {
			this.model2.booking_id = this.payuform.udf1;
		}

		this.model2.prefereddate = moment(this.model2.prefereddate).format("YYYY-MM-DD");
		this.model2.registration_id = localStorage.getItem('registration_id');
		this.model2.voucher_code = localStorage.getItem('voucher_code');
		let newDate = this.model2.timing.toString();
		let newDate1 = newDate.split(' ');
		this.model2.formatted_time = newDate1[4];

		this._bookingService.addBooking(this.model2)
		
		.subscribe(
			
			result => {
			
				if (result.data) {
					console.log(result.data.final_total)
					localStorage.setItem('final_total',result.data.final_total);
					localStorage.setItem('grand_total',result.data.grand_total);
					localStorage.setItem('total_seats',result.data.total_seats);
					localStorage.setItem('booking_id', result.data.booking_id);
					// this._router.navigate(['/summary']);
					this._errorMessage2 = '';
					this.customerDetails();
					this.model3.final_total = localStorage.getItem('final_total');
					this.model3.grand_total = localStorage.getItem('grand_total');
					this.model3.total_seats = localStorage.getItem('total_seats');
					this.payuform.service_provider = 'payu_paisa';
					this.payuform.amount = localStorage.getItem('final_total');
					this.payuform.productinfo = 'Booking';
					this.payuform.udf1 = localStorage.getItem('booking_id');
					// this.payuform.enforce_paymethod = 'creditcard|debitcard|netbanking';
					this.otpVerified = 2;
				}
				else if(result.data.verified == false){
					this.childrenError = true;
					return;
				} 
			},
			error => {
				this._tab1Submitted = false;
				// Validation errors
				if (error.status == 422) {
					this._errorMessage2 = error.data.message; 
					// let errorFields = JSON.parse(error.data.message);
					// this._setFormErrors(errorFields);
					return;
				}
				// Unauthorized Access
				else if (error.status == 401 || error.status == 403) {
					//this._staffService.unauthorizedAccess(error);
				}
				// All other errors
				else {
					this._errorMessage2 = JSON.parse(error.data.message);
				}
			}
		);

	}

	customerDetails() {
		let regId = +localStorage.getItem('registration_id');
		this._bookingService.getCustomerdetails(regId)
		.subscribe(
		  result => {
			if (result.data){
			  this.payuform.firstname = result.data.firstname;
			  this.payuform.email = result.data.email;
			  this.payuform.phone = result.data.phone;
			  this.paymentDetails();
			}
		  },
		  error => {
			// Validation errors
			if (error.status == 422) {
				this._errorMessage2 = error.data.message;
			
				// let errorFields = JSON.parse(error.data.message);

				// this._setFormErrors(errorFields);
			}
			// Unauthorized Access
			else if (error.status == 401 || error.status == 403) {
				//this._staffService.unauthorizedAccess(error);
			}
			// All other errors
			else {
				this._errorMessage2 = JSON.parse(error.data.message);
			}
		  }
		)
	  }
	
	paymentDetails() {
		const paymentPayload = {
		  email: this.payuform.email,
		  firstname: this.payuform.firstname,
		  phone: this.payuform.phone,
		  productinfo: this.payuform.productinfo,
		  amount: this.payuform.amount,
		  service_provider: this.payuform.service_provider,
		  udf1: this.payuform.udf1
		}
		this._bookingService.getPaymentDetails(paymentPayload)
		.subscribe(
		  result => {
			if (result.data) {
			  this.payuform.txnid = result.data.txnid;
			  this.payuform.surl = result.data.surl;
			  this.payuform.furl = result.data.furl;
			  this.payuform.key = result.data.key;
			  this.payuform.hash = result.data.hash;
			  this.payuform.salt = result.data.salt;
			  this._errorMessage2 = '';
			}
		  },
		  error => {
			// Validation errors
			if (error.status == 422) {
				this._errorMessage2 = error.data.message;
			
				// let errorFields = JSON.parse(error.data.message);

				// this._setFormErrors(errorFields);
			}
			// Unauthorized Access
			else if (error.status == 401 || error.status == 403) {
				//this._staffService.unauthorizedAccess(error);
			}
			// All other errors
			else {
				this._errorMessage2 = JSON.parse(error.data.message);
			}
		  }
		)
	}

	goBack() {
		this.otpVerified = 1;
	}
}