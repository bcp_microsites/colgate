import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {LocationStrategy, HashLocationStrategy} from '@angular/common';
import {HttpModule} from '@angular/http';

import {AppComponent} from './app.component';
import {BreadcrumbsComponent} from './shared/breadcrumb.component';

// Routing Module
import {AppRoutingModule} from './app-routing.module';

// Layouts
import {FullLayoutComponent} from './layouts/full-layout.component';
import {SimpleLayoutComponent} from './layouts/simple-layout.component';


// Shared
import {AuthGuard} from './model/auth.guard';
import {SharedModule} from './shared/shared.module';

// Model & Services
import {GlobalService} from './model/global.service';
import {StaffService} from './model/staff.service';
import { RegistrationService } from "./model/registration.service";
import { SuccesspageComponent } from './successpage/successpage.component';
import { BookingComponent } from './booking/booking.component';
import { BookingService} from "./model/booking.service";
import { BookingsuccesspageComponent } from './bookingsuccesspage/bookingsuccesspage.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { TermsComponent } from './terms/terms.component';
import { ContactusComponent } from './contactus/contactus.component'; //this to use ngModule
import { RegistrationComponent } from './registration/registration.component';
import { RecaptchaModule} from 'angular-google-recaptcha';
import { InstantsuccesspageComponent } from './instantsuccesspage/instantsuccesspage.component';
import { ValidateotpComponent } from './validateotp/validateotp.component';
import { ValidateotpService } from "./model/validateotp.service";
import { RewardselectionComponent } from './rewardselection/rewardselection.component';
import { TabsModule } from 'ngx-bootstrap';
import { ImageCropperModule } from 'ngx-image-cropper';
import { Ng2ImgMaxModule } from 'ng2-img-max';
import { SummaryComponent } from './summary/summary.component';
import { FailurepageComponent } from './failurepage/failurepage.component';
import { RedeemComponent } from './redeem/redeem.component';



@NgModule({
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpModule,
        SharedModule,
        ImageCropperModule,
        Ng2ImgMaxModule,
        BsDatepickerModule.forRoot(),
        TabsModule.forRoot(),
        FormsModule,
        ReactiveFormsModule,
        RecaptchaModule.forRoot({
         // siteKey: '6Lfpro4UAAAAAEoAEaErUzK-3cfU0Ty55edTeXYf',
        siteKey: '6LdYHwwUAAAAAAS6-F9wqwt_I0g6MnDpudcfJXRB',
         
          
      }),
    ],
    declarations: [
        AppComponent,
        FullLayoutComponent,
        SimpleLayoutComponent,
        BreadcrumbsComponent,
        SuccesspageComponent,
        BookingComponent,
        BookingsuccesspageComponent,
        ContactusComponent, 
        RegistrationComponent,
        TermsComponent,
        InstantsuccesspageComponent,
        ValidateotpComponent,
        RewardselectionComponent,
        SummaryComponent,
        FailurepageComponent,
        RedeemComponent    
    ],
    providers: [
        {
            provide: LocationStrategy,
            useClass: HashLocationStrategy
        },
        AuthGuard,
        StaffService,
        GlobalService,
        RegistrationService,
        BookingService,
        ValidateotpService,
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
